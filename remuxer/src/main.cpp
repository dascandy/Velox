#include "velox/MuxStream.hpp"
#include <fstream>
#include <filesystem>
#include <iostream>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <map>
#include <unordered_set>
#include "velox/DiscType.hpp"
#include "velox/Image.hpp"
#include <filesystem>
#include <manto/mapping.hpp>

std::string sanitize(std::string name) {
  for (auto& c : name) {
    std::unordered_set<char> bad = {
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
      '/', '\\', '*', '"', '<', '>', ':', '|', '?'
    };
    if (bad.contains(c)) c = '_';
  }
  return name;
}

void StoreTrack(Velox::Graph& g, std::shared_ptr<Velox::MuxInStream>& in) {
  int fd;
  std::filesystem::path filename;
  bool isVideo = not in->videostreams.empty();
  std::string ext = isVideo ? ".mkv" : ".mka";
  std::filesystem::path root = getenv("HOME");
  if (in->chapters.size() == 1) {
    Velox::ChapterMarking& chap = in->chapters[0];
    std::string artist = chap.GetTag("ARTIST"), 
                title = chap.GetTag("TITLE");
    if (artist.empty() || title.empty()) {
      filename = sanitize(in->title) + ext;
    } else {
      filename = sanitize(artist) + "/" + sanitize(artist + " - " + title) + ext;
    }
  } else {
    filename = sanitize(in->title) + ext;
  }
  filename = root / (isVideo ? "Movies" : "Music") / filename;
  std::filesystem::create_directories(filename.parent_path());
  fd = open(filename.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0644);
  std::shared_ptr<Velox::MuxOutStream> mkv = Velox::OpenMatroskaOut(g, [&, fd](size_t off, std::span<const uint8_t> sp) {
    lseek64(fd, off, SEEK_SET);
    size_t size = sp.size();
    while (size) {
      int written = write(fd, sp.data() + sp.size() - size, size);
      if (written > 0) size -= written;
      else if (written < 0) {
        fprintf(stderr, "Error while writing %s at offset %zu with size %zu: ", filename.c_str(), off, sp.size());
        perror(0);
        throw std::runtime_error("IO error during write");
      }
    }
  });
  for (auto& vs : in->videostreams) {
    mkv->AddStream(vs);
  }
  for (auto& vs : in->audiostreams) {
    if (vs->codec == Velox::AudioCodec::Opus) {
      mkv->AddStream(vs);
    } else {
      if (vs->codec != Velox::AudioCodec::LPCM) {
//        add decode step
      }
      mkv->AddStream(OpusEncode(g, vs, 96000));
    }
  }
  for (auto& vs : in->subtitlestreams) {
    mkv->AddStream(vs);
  }
  for (auto& t : in->tags) {
    mkv->AddTag(50, t);
  }
  if (in->chapters.size() == 1) {
    for (auto& t : in->chapters[0].tags) {
      mkv->AddTag(30, t);
    }
  }
  mkv->Run();
  mkv.reset();
  close(fd);
}

std::vector<std::shared_ptr<Velox::MuxInStream>> OpenDisc(Velox::Graph& g, const char* device, Velox::DiscType type) {
  switch(type) {
    case Velox::DiscType::Cd:
      return Velox::OpenCdAudio(g, device, true);
    case Velox::DiscType::Bluray:
      return Velox::OpenBluray(g, device);
    case Velox::DiscType::Dvd:
      return Velox::OpenDvd(g, device);
    default:
      return {};
  }
}

std::string to_string(Velox::DiscType type) {
  switch(type) {
  case Velox::DiscType::Cd:
    return "CD";
  case Velox::DiscType::Dvd:
    return "DVD";
  case Velox::DiscType::Bluray:
    return "Bluray";
  case Velox::DiscType::Unknown:
  default:
    return "Unknown";
  case Velox::DiscType::TrayOpen:
    return "Tray open";
  case Velox::DiscType::NoDisc:
    return "No disc";
  case Velox::DiscType::Busy:
    return "Busy";
  case Velox::DiscType::Error:
    return "Error";
  case Velox::DiscType::HdDvd:
    return "HD-DVD";
  }
}

#define CLEAR_LINE "\x1b[2K"
int main(int , const char** argv) {
  if (std::filesystem::is_regular_file(argv[1])) {
    mapping map(argv[1]);

    Velox::FileFormat type = Velox::GuessFormat(map.data());
    std::shared_ptr<Velox::MuxInStream> in;
    Velox::Graph g;
    switch(type) {
      case Velox::FileFormat::Unknown:
        // No idea
        break;
      case Velox::FileFormat::Bmp:
      case Velox::FileFormat::Jpeg:
      case Velox::FileFormat::Tga:
        // Static image
        break;
      case Velox::FileFormat::Gif89a:
      case Velox::FileFormat::Png:
        // Possibly animated image
        break;
      case Velox::FileFormat::Wav:
        // Sound only
        in = OpenWavIn(g, std::move(map));
        break;
      case Velox::FileFormat::Mkv:
        in = OpenMatroskaIn(g, std::move(map));
        break;
      case Velox::FileFormat::AVI:
      case Velox::FileFormat::Ogg:
      case Velox::FileFormat::Quicktime:
        // AVS streams
        break;
    }
    if (not in) {
      printf("Cannot open file %s\n", argv[1]);
      return -1;
    }
    StoreTrack(g, in);
    printf("File done.\n");
  } else if (std::filesystem::is_block_file(argv[1])) {
    while (true) {
      Velox::DiscType type = Velox::GetDiscType(argv[1]);
      switch(type) {
      case Velox::DiscType::Cd:
      case Velox::DiscType::Dvd:
      case Velox::DiscType::Bluray:
      {
        printf("device %s found disc of type %s, ripping\n", argv[1], to_string(type).c_str());
        try {
          Velox::Graph g;
          std::vector<std::shared_ptr<Velox::MuxInStream>> in = OpenDisc(g, argv[1], type);
          printf("Found disc title %s\n", in[0]->title.c_str());
          size_t current = 1;
          for (auto& in_p : in) {
            printf("Storing track %zu/%zu: %s\n", current, in.size(), in_p->chapters[0].name.c_str());
            StoreTrack(g, in_p);
            current++;
          }
          printf("Disc done, ejecting\n");
        } catch (...) {
          printf("Error while storing disc, ejecting\n");
        }
        Velox::EjectDevice(argv[1]);
      }
        break;
      default:
        printf(CLEAR_LINE "device %s %s\r", argv[1], to_string(type).c_str());
        fflush(stdout);
        break;
      }
      sleep(1);
    }
  }
}


