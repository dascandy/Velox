#include "velox/MuxStream.hpp"
#include <fcntl.h>
#include <sys/uio.h>

int main(int argc, const char** argv) {
  std::vector<std::string> args(argv, argv+argc);

  std::vector<std::shared_ptr<Velox::MuxInStream>> instreams;
  Velox::Graph g;
  for (size_t n = 1; n < args.size() - 1; n++) {
    instreams.push_back(OpenMatroskaIn(g, mapping(args[n])));
  }
  int outfd = open(args.back().c_str(), O_WRONLY, 0644);
  auto out = OpenMatroskaOut(g, [outfd](size_t offset, std::span<const uint8_t> data) {
    struct iovec iov{(void*)data.data(), data.size()};
    pwritev(outfd, &iov, 1, offset);
  });
}


