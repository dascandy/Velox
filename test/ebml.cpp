#include <catch2/catch_all.hpp>
#include "EbmlWriter.hpp"

namespace Velox {

TEST_CASE("ebml writer") {
  EbmlWriter w;
  w.add(mkv_ids::EbmlHeader, 0);
  std::vector<uint8_t> v = w;
  CHECK(v == std::vector<uint8_t>{ 0x1a, 0x45, 0xdf, 0xa3, 0x81, 0x00 });
}

TEST_CASE("ebml head") {
  EbmlWriter w;

  EbmlWriter header;
  header.add(mkv_ids::EbmlVersion, 1);
  header.add(mkv_ids::EbmlReadVersion, 1);
  header.add(mkv_ids::EbmlMaxIdLength, 4);
  header.add(mkv_ids::EbmlMaxSizeLength, 8);
  header.add(mkv_ids::DocType, "matroska");
  header.add(mkv_ids::DocTypeVersion, 4);
  header.add(mkv_ids::DocTypeReadVersion, 2);
  w.add(mkv_ids::EbmlHeader, header);

  std::vector<uint8_t> v = w;
  CHECK(v == std::vector<uint8_t>{ 
    0x1a, 0x45, 0xdf, 0xa3, 0xa3, 
      0x42, 0x86, 0x81, 0x01,
      0x42, 0xf7, 0x81, 0x01,
      0x42, 0xf2, 0x81, 0x04,
      0x42, 0xf3, 0x81, 0x08,
      0x42, 0x82, 0x88, 'm', 'a', 't', 'r', 'o', 's', 'k', 'a',
      0x42, 0x87, 0x81, 0x04,
      0x42, 0x85, 0x81, 0x02,
  });
}
}

