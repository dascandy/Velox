#include <catch2/catch_all.hpp>
#include "velox/Color.hpp"

namespace Velox {

TEST_CASE("color conversion from 16 bit") {
  CHECK(Color::from555(0) == Color{});
  CHECK(Color::from555(0x7FFF) == Color{1, 1, 1});
  CHECK(Color::from555(0xFFFF) == Color{1, 1, 1});
  CHECK(Color::from555(0x001F) == Color{0, 0, 1});
  CHECK(Color::from555(0x03E0) == Color{0, 1, 0});
  CHECK(Color::from555(0x7C00) == Color{1, 0, 0});

  CHECK(Color::from565(0) == Color{});
  CHECK(Color::from565(0xFFFF) == Color{1, 1, 1});
  CHECK(Color::from565(0x001F) == Color{0, 0, 1});
  CHECK(Color::from565(0x07E0) == Color{0, 1, 0});
  CHECK(Color::from565(0xF800) == Color{1, 0, 0});

  CHECK(Color::from888(0) == Color{});
  CHECK(Color::from888(0xFFFFFF) == Color{1, 1, 1});
  CHECK(Color::from888(0x0000FF) == Color{0, 0, 1});
  CHECK(Color::from888(0x00FF00) == Color{0, 1, 0});
  CHECK(Color::from888(0xFF0000) == Color{1, 0, 0});

  CHECK(Color::from8888(0) != Color{});
  CHECK(Color::from8888(0) == Color{0, 0, 0, 0});
  CHECK(Color::from8888(0xFFFFFFFF) == Color{1, 1, 1});
  CHECK(Color::from8888(0xFF0000FF) == Color{0, 0, 1});
  CHECK(Color::from8888(0xFF00FF00) == Color{0, 1, 0});
  CHECK(Color::from8888(0xFFFF0000) == Color{1, 0, 0});
  CHECK(Color::from8888(0xFF000000) == Color{0, 0, 0});
}

}

