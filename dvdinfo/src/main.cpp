#include "velox/DvdInfo.hpp"
#include <filesystem>

int main(int , const char** argv) {
  if (std::filesystem::is_block_file(argv[1])) {
    auto disc = Velox::DiscData(argv[1]);
    printf("{\n");
    printf("  \"dvdid\": \"%s\",\n", disc.dvdid.c_str());
    printf("  \"disctitle\": \"%s\",\n", disc.disctitle.c_str());
    printf("  \"disctitle2\": \"%s\",\n", disc.disctitle2.c_str());
    printf("  \"serial\": \"%s\",\n", disc.serial.c_str());
    printf("  \"titles\": [\n");
    bool first_title = true;
    for (auto& title : disc.titles) {
      if (not first_title) {
        printf(",\n  {\n");
      } else {
        first_title = false;
        printf("  {\n");
      }
      printf("    \"id\": \"%u\",\n", title.id);
      printf("    \"usage\": \"%s\",\n", title.usage.c_str());
      printf("    \"aspect_ratio\": \"%s\",\n", title.aspect_ratio.c_str());
      printf("    \"resolution\": \"%s\",\n", title.resolution.c_str());
      printf("    \"duration\": \"%f\",\n", title.duration);
      printf("    \"tracks\": [\n");
      bool first_track = true;
      for (auto& track : title.tracks) {
        if (not first_track) {
          printf(",\n    {\n");
        } else {
          first_track = false;
          printf("    {\n");
        }
        printf("      \"type\": \"%s\",\n", track.type.c_str());
        printf("      \"id\": \"%u\",\n", track.id);
        printf("      \"format\": \"%s\",\n", track.format.c_str());
        printf("      \"language\": \"%s\",\n", track.language.c_str());
        if (track.type == "audio") {
          printf("      \"channels\": \"%d\",\n", track.channels);
        }
        printf("      \"is_hard_of_hearing\": \"%s\",\n", track.is_hard_of_hearing ? "yes" : "no");
        printf("      \"is_commentary\": \"%s\"\n", track.is_commentary ? "yes" : "no");
        printf("    }");
      }
      printf("\n    ],\n");

      printf("    \"parts\": [\n");
      bool first_part = true;
      for (auto& part : title.parts) {
        if (not first_part) {
          printf(",\n    {\n");
        } else {
          first_part = false;
          printf("    {\n");
        }
        printf("      \"start\": \"%f\",\n", part.start);
        printf("      \"duration\": \"%f\"\n", part.duration);
        printf("    }");
      }
      printf("\n    ]\n");
      printf("  }");
    }
    printf("\n  ]\n");
    printf("}\n");
  }
}


