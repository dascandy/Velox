#pragma once

#include <cstdint>
#include <vector>
#include <span>
#include <bini/reader.hpp>
#include "MuxStream.hpp"
#include <origo/channel.hpp>

namespace Velox {

struct Mpeg2PES {
  Origo::channel_writer<Packet> chan;
  std::vector<uint8_t> accum;
  Mpeg2PES(Origo::channel_writer<Packet> channel)
  : chan(channel)
  {}
  void Add(std::span<const uint8_t> buffer) {
    accum.insert(accum.end(), buffer.begin(), buffer.end());
    while (accum.size() >= 7) {
      size_t start = 0;
      while (accum.size() > start + 3 &&
             (accum[start] != 0 ||
              accum[start+1] != 0 ||
              accum[start+2] != 1)) start++;
      if (start == accum.size() - 3) return;
      Bini::reader r(accum);
      r.skip(start);
      uint32_t header = r.read32be();
      (void)header;
      size_t size = r.read16be();
      if (size == 0) {
        void* end = memmem(accum.data() + start + 4, accum.size() - start - 4, accum.data() + start, 4);
        if (not end) return;
        size = (uint8_t*)end - accum.data() - start - 6;
      }

      if (start + size + 6 > accum.size()) return;
      SendPacket(start, size + 6);
      memmove(accum.data(), accum.data() + start + size + 6, accum.size() - start - size - 6);
      accum.resize(accum.size() - start - size - 6);
    }
  }
  void SendPacket(size_t start, size_t size) {
    Packet p;
    Bini::reader buf(std::span<const uint8_t>(accum).subspan(start, size));
    buf.skip(6);
    uint16_t pes_flags = buf.read16be();
    uint8_t pes_hdrlen = buf.read8();
    Bini::reader peshdr(buf.get(pes_hdrlen));
    if (pes_flags & 0x80) {
      uint64_t pts = peshdr.readSizedbe(5);
      pts = ((pts & 0xE00000000ULL) >> 3) | ((pts & 0xFFFE0000) >> 2) | ((pts & 0xFFFE) >> 1);
      p.timestamp = (100000 * pts) / 9;
    } else {
      p.timestamp = 0;
    }
    auto ds = buf.get(buf.sizeleft());
    if (buf.fail()) {
      printf("Buffer fail on channel\n");
    } else {
      p.data = std::vector<uint8_t>(ds.begin(), ds.begin() + ds.size());
      chan.add(std::move(p));
    }
  }
  ~Mpeg2PES() {
    size_t start = 0;
    while (accum.size() > start + 3 &&
           (accum[start] != 0 ||
            accum[start+1] != 0 ||
            accum[start+2] != 1)) start++;
    if (start == accum.size() - 3) return;
    
    if (not accum.empty()) 
      SendPacket(start, accum.size() - start);
  }
};

}

