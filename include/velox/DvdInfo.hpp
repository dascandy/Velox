#pragma once

#include <string>
#include <cstdint>
#include <vector>

namespace Velox {

std::string DvdId(std::string device);

struct Track {
  std::string type;
  int8_t id;
  std::string language;
  std::string format;
  uint8_t channels = 0;
  bool is_hard_of_hearing = false;
  bool is_commentary = false;
};
struct Part {
  double start = 0.0;
  double duration = 0.0;
};
struct Title {
  int8_t id;
  std::string usage;
  std::string imdb_id;
  std::string aspect_ratio;
  std::string resolution;
  std::vector<Track> tracks;
  std::vector<Part> parts;
  double duration = 0.0;
};
struct DvdDisc {
  std::string dvdid;
  std::string disctitle;
  std::string disctitle2;
  std::string serial;
  std::vector<Title> titles;
};

DvdDisc DiscData(std::string device);

}


