#pragma once

#include <utility>
#include <string>
#include <vector>
#include <string_view>

struct CddbTrack {
  std::string artist;
  std::string title;
  uint64_t duration;
};

// Title, chapter or song titles
struct CddbLookupResult {
  std::string mbid;
  std::string artist;
  std::string title;
  std::string releaseDate;
  std::vector<CddbTrack> tracks;
};

CddbLookupResult CddbLookup(std::string_view discid);


