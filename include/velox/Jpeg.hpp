#pragma once

#include "Image.hpp"
#include <map>
#include <memory>
#include <span>
#include <cstdint>
#include <vector>

namespace Velox {

Image LoadJpeg(std::span<const uint8_t> data);
std::vector<uint8_t> SaveJpeg(const Image&);

}


