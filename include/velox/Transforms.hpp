#pragma once

#include "Image.hpp"

namespace Velox {

Image ConvertTo(const Image& source, PixelFormat to);
Image FlipVertical(const Image& img);
Image FlipHorizontal(const Image& img);

}

