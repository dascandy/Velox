#pragma once

#include "Image.hpp"
#include <map>
#include <memory>
#include <span>
#include <cstdint>
#include <vector>

namespace Velox {

Image LoadPng(std::span<const uint8_t> data);
std::unique_ptr<VideoStream> LoadPngVideo(std::span<const uint8_t> data);
std::vector<uint8_t> SavePng(const Image&);
std::vector<uint8_t> SavePngVideo(VideoStream&);

}


