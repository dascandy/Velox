#pragma once

#include "Image.hpp"
#include <map>
#include <memory>
#include <span>
#include <cstdint>
#include <vector>

namespace Velox {

Image LoadGif(std::span<const uint8_t> data);
std::unique_ptr<VideoStream> LoadGifVideo(std::span<const uint8_t> data);

struct GifStream : VideoStream {
  struct ImageDataReference {
    std::span<const uint8_t> imageData;
    enum class ClearStyle {
      LeaveAsIs,
      ClearToBackground,
    } clearStyle = ClearStyle::LeaveAsIs;
    ImageDataReference* overlaidOn = nullptr;
  };
  GifStream(std::span<const uint8_t> data);
  const Image& get() override;
  bool seek(size_t frameno) override;
  void Decode(ImageDataReference*);
  std::span<const uint8_t> data;
  std::span<const uint8_t> globalColorTable;
  std::map<uint32_t, ImageDataReference> offsets;
  Image currentImage;
  ImageDataReference* currentReference;
  size_t loopCount;
};

}

