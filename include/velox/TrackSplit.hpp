#pragma once

#include <velox/MuxStream.hpp>
#include "channel.hpp"

namespace Velox {

struct TrackSplit {
  TrackSplit(std::vector<ChapterMarking>& chaps, channel<Packet>& inC);
  TrackSplit(const TrackSplit&) = delete;
  TrackSplit& operator=(const TrackSplit&) = delete;
  std::vector<channel<Packet>> out;
  ~TrackSplit();
private:
  std::thread thr;
};

}


