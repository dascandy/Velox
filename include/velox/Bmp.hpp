#pragma once

#include "Image.hpp"
#include <map>
#include <memory>
#include <span>
#include <cstdint>
#include <vector>

namespace Velox {

Image LoadBmp(std::span<const uint8_t> data);
std::vector<uint8_t> SaveBmp(const Image&);

}


