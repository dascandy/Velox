#pragma once

#include <string>

namespace Velox {

enum class DiscType {
  Unknown,
  TrayOpen,
  NoDisc,
  Busy,
  Error,
  Cd,
  Dvd,
  Bluray,
  HdDvd,
};

void EjectDevice(const std::string& device);
DiscType GetDiscType(const std::string& device);

}

