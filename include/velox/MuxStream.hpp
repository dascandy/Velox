#pragma once

#include <string>
#include <span>
#include <cstddef>
#include <cstdint>
#include <vector>
#include <utility>
#include <variant>
#include <thread>
#include <functional>
#include "origo/channel.hpp"
#include "manto/mapping.hpp"

namespace Velox {

enum class VideoCodec {
  Unknown = 0,
  Mpeg1 = 1,
  Mpeg2 = 2,
  VC1 = 3,
  H264 = 4,
  H265 = 5,
  VP8 = 8,
  VP9 = 9,
  Theora = 11,
  AV1 = 12,
};

enum class AudioCodec {
  Unknown = 0,
  DTS = 1,
  AC3 = 2,
  TRUHD = 3,
  MPEG1 = 4,
  MPEG2 = 5,
  LPCM = 6,
  AC3PLUS = 7, // or secondary
  DTSHD = 8, // or master, or secondary
  Opus = 9,
};

// Channel position is in 15 degree increments from the front center
enum class AudioChannelPosition {
  Center = 0,
  FrontRight = 2,
  SideRight = 7,
  RearRight = 10,
  RearCenter = 12,
  RearLeft = 14,
  SideLeft = 17,
  FrontLeft = 22,

  Overhead = 32,
  LFE = 64,
};

inline int getChannelAngle(AudioChannelPosition pos) {
  return (int(pos) & 0x1F) * 15;
}

enum class SubtitleCodec {
  Unknown = 0,
  Vobsub = 1,
  PG = 2,
  IG = 3,
  BlurayText = 4,
};

enum TextEncoding {
  Unknown = 0,
  NotApplicable = 1,
  Utf8 = 2,
  Utf16be = 3,
  ShiftJis = 4,
  EucKr = 5,
  Gb18030 = 6,
  CnGb = 7,
  Big5 = 8,
};

struct Packet {
  std::vector<uint8_t> data;
  uint64_t timestamp = 0;
  bool isKeyframe = false;
};

struct GraphNode {
  virtual ~GraphNode() {} 
};

struct Graph {
  std::vector<std::shared_ptr<GraphNode>> nodes;
  void Add(std::shared_ptr<GraphNode> n) {
    nodes.push_back(std::move(n));
  }
};

struct StreamDescription {
  Origo::channel<Packet> packets;
  std::shared_ptr<GraphNode> parentNode = nullptr;
};

struct VideoStreamDescription : StreamDescription {
  VideoStreamDescription(VideoCodec codec, float framerate, std::pair<uint32_t, uint32_t> imagesize, std::pair<uint32_t, uint32_t> aspectratio) 
  : codec(codec)
  , framerate(framerate)
  , imagesize(imagesize)
  , aspectratio(aspectratio)
  {}
  VideoCodec codec;
  float framerate;
  std::pair<uint32_t, uint32_t> imagesize;
  std::pair<uint32_t, uint32_t> aspectratio;
  std::string description;
  std::vector<uint8_t> codecPrivate;
  bool interlaced = false;
};

struct AudioStreamDescription : StreamDescription {
  AudioStreamDescription(AudioCodec codec, uint32_t samplingFrequency, std::vector<AudioChannelPosition> channels) 
  : codec(codec)
  , samplingFrequency(samplingFrequency)
  , channels(std::move(channels))
  {}
  AudioCodec codec;
  uint32_t samplingFrequency;
  std::vector<AudioChannelPosition> channels;
  std::string language = "und";
  std::string description;
  uint8_t bitdepth = 0; // unknown or irrelevant by default
  bool commentaryTrack = false;
};

struct SubtitleStreamDescription : StreamDescription {
  SubtitleStreamDescription(SubtitleCodec format, std::string language)
  : format(format)
  , language(std::move(language))
  {}
  SubtitleCodec format;
  std::string language;
  std::string description;
  TextEncoding encoding = TextEncoding::NotApplicable;
  bool commentaryTrack = false;
  bool hardOfHearing = false;
};

struct Tag {
  std::string tagname;
  std::string value;
  uint64_t startTime = 0;
  uint64_t endTime = 0;
};

struct ChapterMarking {
  uint64_t timestamp;
  std::string name;
  uint64_t duration = 0;
  bool connectedToNext = true;
  std::vector<Tag> tags = {};
  std::string GetTag(const std::string& key) {
    for (auto& t : tags) {
      if (t.tagname == key) return t.value;
    }
    return "";
  }
};

struct MuxOutStream : GraphNode {
public:
  virtual ~MuxOutStream() = default;
  virtual void AddTag(int level, Tag tag) = 0;
  virtual void AddChapterMarking(ChapterMarking marking) = 0;
  virtual void AddStream(std::shared_ptr<VideoStreamDescription>) = 0;
  virtual void AddStream(std::shared_ptr<AudioStreamDescription>) = 0;
  virtual void AddStream(std::shared_ptr<SubtitleStreamDescription>) = 0;
  virtual void SetDuration(uint64_t) = 0;
  virtual void Run() = 0;
};

struct MuxInStream : GraphNode {
public:
  virtual ~MuxInStream() = default;
  std::vector<std::shared_ptr<VideoStreamDescription>> videostreams;
  std::vector<std::shared_ptr<AudioStreamDescription>> audiostreams;
  std::vector<std::shared_ptr<SubtitleStreamDescription>> subtitlestreams;
  std::vector<ChapterMarking> chapters;
  std::vector<Tag> tags;
  std::string title;
  uint64_t duration = 0;
};

std::vector<std::shared_ptr<MuxInStream>> OpenDvd(Graph& graph, std::string device);
std::vector<std::shared_ptr<MuxInStream>> OpenBluray(Graph& graph, std::string device);
std::vector<std::shared_ptr<MuxInStream>> OpenCdAudio(Graph& g, std::string device, bool forceMerge = false);
std::shared_ptr<MuxInStream> OpenMatroskaIn(Graph& graph, mapping map);
std::shared_ptr<MuxInStream> OpenWavIn(Graph& graph, mapping map);

std::shared_ptr<MuxOutStream> OpenMatroskaOut(Graph& graph, std::function<void(size_t, std::span<const uint8_t>)> write);

std::shared_ptr<AudioStreamDescription> OpusEncode(Graph& graph, std::shared_ptr<AudioStreamDescription> in, uint32_t bitrate);

}


