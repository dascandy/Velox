#pragma once

#include <cstdint>
#include <vector>
#include <span>
#include <bini/reader.hpp>

struct Mpeg2TS {
  std::function<void(uint16_t, std::span<const uint8_t>)> Send;
  Mpeg2TS(std::function<void(uint16_t, std::span<const uint8_t>)> OnSend)
  : Send(OnSend)
  {}
  std::vector<uint8_t> accum;
  uint16_t accum_pid = 32767;
  void Add(std::span<const uint8_t> buffer) {
    if (buffer.size() % 192) {
      throw std::runtime_error("Please pass in a whole number of packets at a time");
    }
    Bini::reader buf(buffer);
    for (size_t n = 0; n < buffer.size() / 192; n++) {
      Bini::reader r(buf.get(192));
      uint32_t timestamp = r.read32be();
      uint32_t ts_hdr = r.read32be();
      bool pusi = ts_hdr & 0x400000;
      bool prio = ts_hdr & 0x200000;
      uint16_t pid = (ts_hdr >> 8) & 0x1FFF;
      bool adaptation_field = ts_hdr & 0x20;
      bool payload = ts_hdr & 0x10;
      uint8_t adaptationSize = 0;
      if (adaptation_field) {
        adaptationSize = r.read8();
      }
      std::span<const uint8_t> adaptation = r.get(adaptationSize);
      std::span<const uint8_t> data = r.get(r.sizeleft());
      if ((pusi || accum_pid != pid) && not accum.empty()) {
        Send(accum_pid, accum);
        accum.clear();
      }
      accum_pid = pid;
      (void)timestamp;
      (void)prio;
      (void)payload;
      if (not adaptation.empty()) {
        uint8_t aflag = adaptation[0];
        (void)aflag;
      }
      if (not data.empty()) {
        accum.insert(accum.end(), data.begin(), data.end());
      }
    }
  }
  ~Mpeg2TS() {
    Send(accum_pid, accum);
  }
};


