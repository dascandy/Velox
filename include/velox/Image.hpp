#pragma once

#include <vector>
#include <cstddef>
#include <cstdint>
#include <span>
#include <ostream>
#include <cmath>
#include <memory>

namespace Velox {

enum class PixelFormat {
  Bitmask,
  Grayscale8,
  Grayscale16,
  RGB8,
  BGR8,
  RGBA8,
  BGRA8,
  RGB10A2,
  RGB16,
  RGBA16,
  RGB32F,
  RGBA32F,
};

enum class ColorSpace {
  Grayscale,
  RGB,
  YCbCr,
};

struct Color {
  float red = 0.0, green = 0.0, blue = 0.0, alpha = 1.0;
  auto operator<=>(const Color&) const = default;
  static Color from555(uint16_t);
  static Color from565(uint16_t);
  static Color from888(uint32_t);
  static Color from8888(uint32_t);
  static Color fromColorspace(ColorSpace space, float a, float b, float c);
  std::tuple<float, float, float> to_colorspace(ColorSpace space);
  friend std::ostream& operator<<(std::ostream& os, const Color& c) {
    os << "{ " << c.red << ", " << c.green << ", " << c.blue << ", " << c.alpha << "}";
    return os;
  }
  static Color Blend(Color a, Color b, float t) {
    if (t == 0) return a;
    if (t == 1) return b;
    return { a.red * (1-t) + b.red * t,
             a.green * (1-t) + b.green * t,
             a.blue * (1-t) + b.blue * t,
             a.alpha * (1-b.alpha) + b.alpha };
  }
  Color GammaCorrect(float gamma) {
    return {powf(red, 1.0f / gamma),
            powf(green, 1.0f / gamma),
            powf(blue, 1.0f / gamma),
            alpha };
  }
  Color GammaInvCorrect(float gamma) {
    return {powf(red, gamma),
            powf(green, gamma),
            powf(blue, gamma),
            alpha };
  }
};

// For example, the above r5g6b5 format would have 16 bpp, 5 bpc (lowest of any channel), and 3 channels.
ColorSpace PixelFormatToColorSpace(PixelFormat format);
bool PixelFormatHasTransparency(PixelFormat format);
uint8_t PixelFormatToBPP(PixelFormat format);
uint8_t PixelFormatToBPC(PixelFormat format);
uint8_t PixelFormatToChannels(PixelFormat format);

enum class Framerate {
  FSingleFrame = 0,
  F15 = 15,
  F24 = 24,
  F25 = 25,
  F30 = 30,
  F50 = 50,
  F60 = 60,
  F100 = 100,
  F300 = 300,
};

enum class FileFormat {
  Unknown,
  Bmp,
  Gif89a,
  Jpeg, 
  Png,
  Tga,
  Wav,
  Mkv,
  AVI,
  Ogg,
  Quicktime,
};

FileFormat GuessFormat(std::span<const uint8_t> data);

struct Image {
  Image() {}
  Image(size_t width, size_t height, PixelFormat format);
  void LoadFrom(ColorSpace cs, Image& plane1, Image& plane2, Image& plane3);
  Image ConvertTo(PixelFormat format) const;
  void setPixel(uint32_t x, uint32_t y, Color color);
  Color getPixel(uint32_t x, uint32_t y) const;
  std::span<const uint8_t> GetLine(size_t line) const;
  void Clear(Color color = {0, 0, 0, 0});
  std::vector<uint8_t> buffer;
  uint32_t width, height;
  PixelFormat format;
  float gamma = 2.2;
};

struct VideoStream {
  uint32_t width, height;
  PixelFormat format;
  Framerate rate;
  uint64_t framecount;
  virtual ~VideoStream() = default;
  virtual const Image& get() = 0;
  // returns true if the seek resulted in a different image to display
  virtual bool seek(size_t frameno) = 0;
};

struct ImageVideoStream : VideoStream {
  Image image;
  ImageVideoStream(Image image)
  : image(image)
  {
    width = image.width;
    height = image.height;
    format = image.format;
    rate = Framerate::FSingleFrame;
    framecount = 1;
  }
  ~ImageVideoStream() = default;
  const Image& get() override { return image; }
  bool seek(size_t) override { return false; }
};

// This is just going to load a video & get the first frame.
Image LoadStillImage(std::span<const uint8_t> data, FileFormat format = FileFormat::Unknown);
std::unique_ptr<VideoStream> LoadVideoStream(std::span<const uint8_t> data, FileFormat format = FileFormat::Unknown);
std::vector<uint8_t> SaveStillImage(const Image&, FileFormat format);
std::vector<uint8_t> SaveVideoStream(const VideoStream&, FileFormat format);

struct ImageView {
  Image* image;
  
};

}


