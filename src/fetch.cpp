#include "fetch.hpp"
#include "nlohmann/json.hpp"
#include "manto/process.hpp"

std::vector<uint8_t> fetch(std::string url, bool pretend_to_be_chrome) {
  std::string useragent = (pretend_to_be_chrome ? "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36" : "velox-0.0.1 (dascandy@gmail.com)");
  std::vector<uint8_t> str;
  //printf("curl -H \"User-Agent: %s\" \"%s\"\n", useragent.c_str(), url.c_str());
  Run(Process("curl", "-H", "User-Agent: " + useragent, url).out(str));
  //printf("output size %zu\n", str.size());
  return str;
}

nlohmann::json fetch_json(std::string url, bool pretend_to_be_chrome) {
  std::vector<uint8_t> str = fetch(url, pretend_to_be_chrome);
  std::string_view sv((const char*)str.data(), (const char*)str.data() + str.size());
  //printf("%s\n", std::string(sv).c_str());
  return nlohmann::json::parse(sv);
}

