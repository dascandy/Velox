#pragma once

#include <vector>
#include <string>
#include <cstdint>
#include "nlohmann/json.hpp"

std::vector<uint8_t> fetch(std::string url, bool pretend_to_be_chrome = false);
nlohmann::json fetch_json(std::string url, bool pretend_to_be_chrome = false);


