#pragma once

#include "Image.hpp"
#include <map>

struct GifStream : VideoStream {
  struct ImageDataReference {
    std::span<const uint8_t> imageData;
    enum class ClearStyle {
      LeaveAsIs,
      ClearToBackground,
    } clearStyle = ClearStyle::LeaveAsIs;
    ImageDataReference* overlaidOn = nullptr;
  };
  GifStream(std::span<const uint8_t> data);
  const Image& get() override;
  bool seek(size_t frameno) override;
  VideoStream* clone() override;
  void Decode(ImageDataReference*);
  std::span<const uint8_t> data;
  std::span<const uint8_t> globalColorTable;
  std::map<uint32_t, ImageDataReference> offsets;
  Image currentImage;
  ImageDataReference* currentReference;
  size_t loopCount;
};


