#include "velox/Color.hpp"

namespace Velox {

Color Color::from555(uint16_t color) {
  return { ((color & 0x7C00) >> 10) / 31.0f, 
           ((color & 0x3E0) >> 5) / 31.0f, 
           (color & 0x1F) / 31.0f };
}

Color Color::from565(uint16_t color) {
  return { ((color & 0xF800) >> 11) / 31.0f, 
           ((color & 0x7E0) >> 5) / 63.0f, 
           (color & 0x1F) / 31.0f };
}

Color Color::from888(uint32_t color) {
  return { ((color & 0xFF0000) >> 16) / 255.0f, 
           ((color & 0xFF00) >> 8) / 255.0f, 
           (color & 0xFF) / 255.0f };
}

Color Color::from8888(uint32_t color) {
  return { ((color & 0xFF0000) >> 16) / 255.0f, 
           ((color & 0xFF00) >> 8) / 255.0f, 
           (color & 0xFF) / 255.0f,
           ((color & 0xFF000000) >> 24) / 255.0f};
}

Color Color::fromColorspace(ColorSpace space, float a, float b, float c) {
  printf("y %f cb %f cr %f\n", a, b, c);
  switch(space) {
    case ColorSpace::Grayscale: return {a, a, a};
    case ColorSpace::RGB: return {a, b, c};
    case ColorSpace::YCbCr: {
      Color co;
      co.red = a + 1.402 * (c - 0.501960784314);
      co.green = a - 0.34414 * (b - 0.501960784314) - 0.71414 * (c - 0.501960784314);
      co.blue = a + 1.772 * (b - 0.501960784314);
      return co;
    }
  }
}

std::tuple<float, float, float> Color::to_colorspace(ColorSpace space) {
  switch(space) {
    case ColorSpace::Grayscale:
    {
      float y = 0.299 * red + 0.587 * green + 0.114 * blue;
      return {y, y, y};
    }
    case ColorSpace::RGB:
    {
      return {red, green, blue};
    }
    case ColorSpace::YCbCr:
    {
      float y = 0.299 * red + 0.587 * green + 0.114 * blue;
      float cb = -0.1687 * red - 0.3313 * green + 0.5 * blue + 0.5;
      float cr = 0.5 * red - 0.4187 * green - 0.0813 * blue + 0.5;
      return {y, cb, cr};
    }
  }
}

}

