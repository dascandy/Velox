#pragma once

#include <span>
#include <tuple>
#include "bini/reader.hpp"

namespace Velox {

// channels, bit depth, samples
std::tuple<std::vector<AudioChannelPosition>, int, int> lpcm_bd_packet_to_channels(std::span<const uint8_t> firstPacket) {
  Bini::reader r(firstPacket);
  std::vector<AudioChannelPosition> channels;
  int bitdepth, samples;
  uint32_t val = r.read32be();

  switch((val >> 12) & 0xf) {
    case 1: 
      channels = { AudioChannelPosition::Center }; 
      break;
    case 3: 
      channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }; 
      break;
    case 4: 
      channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight };
      break;
    case 5: 
      channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter };
      break;
    case 6: 
      channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::Center, AudioChannelPosition::RearCenter };
      break;
    case 7: 
      channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight };
      break;
    case 8: 
      channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight, AudioChannelPosition::Center };
      break;
    case 9: 
      channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight, AudioChannelPosition::Center, AudioChannelPosition::LFE };
      break;
    case 10: 
      channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearRight, AudioChannelPosition::Center };
      break;
    case 11: 
      channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearRight, AudioChannelPosition::Center, AudioChannelPosition::LFE };
      break;
    default: break;
  }

  switch ((val >> 6) & 0x3) {
  case 1: bitdepth = 16; break;
  case 2: // 20 bit stored as 24
  case 3: bitdepth = 24; break;
  }

  switch ((val >> 8) & 0xf) {
  case 1: samples = 48000; break;
  case 4: samples = 96000; break;
  case 5: samples = 192000; break;
  }

  return { channels, bitdepth, samples };
}

}


