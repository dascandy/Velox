#include "Scsi.hpp"
#include "velox/DiscType.hpp"
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <map>

namespace Velox {

static std::map<uint8_t, DiscType> known_types = {
  { 0x08, DiscType::Cd },
  { 0x09, DiscType::Cd },
  { 0x0a, DiscType::Cd },
  { 0x10, DiscType::Dvd },
  { 0x11, DiscType::Dvd },
  { 0x12, DiscType::Dvd },
  { 0x13, DiscType::Dvd },
  { 0x14, DiscType::Dvd },
  { 0x15, DiscType::Dvd },
  { 0x16, DiscType::Dvd },
  { 0x17, DiscType::Dvd },
  { 0x18, DiscType::Dvd },
  { 0x1a, DiscType::Dvd },
  { 0x1b, DiscType::Dvd },
  { 0x20, DiscType::Cd },
  { 0x21, DiscType::Cd },
  { 0x22, DiscType::Cd },
  { 0x2a, DiscType::Dvd },
  { 0x2b, DiscType::Dvd },
  { 0x40, DiscType::Bluray },
  { 0x41, DiscType::Bluray },
  { 0x42, DiscType::Bluray },
  { 0x43, DiscType::Bluray },
  { 0x50, DiscType::HdDvd },
  { 0x51, DiscType::HdDvd },
  { 0x52, DiscType::HdDvd },
  { 0x53, DiscType::HdDvd },
  { 0x54, DiscType::HdDvd },
  { 0x55, DiscType::HdDvd },
};

void EjectDevice(const std::string& device) {
  int fd = open(device.c_str(), O_RDWR | O_NONBLOCK);
  std::array<uint8_t, 6> cmd_eject = { 0x1E, 0, 0, 0, 0, 0 };
  std::array<uint8_t, 18> sense;
  sg_io_hdr_t command = create_command(cmd_eject, false, false, {}, sense);
  // Unlock drive
  ioctl(fd, SG_IO, &command);
  cmd_eject[0] = 0x1B;
  // Eject
  cmd_eject[4] = 1;
  ioctl(fd, SG_IO, &command);
  cmd_eject[4] = 2;
  ioctl(fd, SG_IO, &command);
  close(fd);
}

DiscType TestUnit(int fd) {
  std::array<uint8_t, 6> test_unit_cmd = { 0, 0, 0, 0, 0, 0 };
  std::array<uint8_t, 18> sense;
  sg_io_hdr_t command = create_command(test_unit_cmd, false, false, {}, sense);
  ioctl(fd, SG_IO, &command);
  if (command.masked_status == 0) {
    return DiscType::Unknown;
  } else {
    if (sense[12] == 0x3a) {
      if (sense[13] == 1) {
        return DiscType::NoDisc;
      } else if (sense[13] == 2) {
        return DiscType::TrayOpen;
      }
    } else if (sense[12] == 0x04 && sense[13] == 0x01) {
      return DiscType::Busy;
    }
    return DiscType::Error;
  }
}

DiscType GetConfiguration(int fd) {
  std::array<uint8_t, 10> get_config_cmd = { 0x46, 0, 0, 0, 0, 0, 0, 8, 0, 0 };
  std::array<uint8_t, 2048> buffer;
  std::array<uint8_t, 18> sense;
  sg_io_hdr_t command = create_command(get_config_cmd, true, false, buffer, sense);
  ioctl(fd, SG_IO, &command);
  if (command.masked_status != 0) {
    return DiscType::Error;
  }
  if (auto it = known_types.find(buffer[7]); it != known_types.end()) {
    return it->second;
  }
  return DiscType::Unknown;
}

DiscType GetDiscType(const std::string& device) {
  int fd = open(device.c_str(), O_RDWR | O_NONBLOCK);
  DiscType type = TestUnit(fd);
  if (type != DiscType::Unknown) {
    close(fd);
    return type;
  }
  type = GetConfiguration(fd);
  close(fd);
  return type;
}

}

