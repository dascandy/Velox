#include "velox/MuxStream.hpp"
#include "opus/opus_multistream.h"
#include "bini/reader.hpp"
#include <soxr.h>

namespace Velox {

struct OpusConversionNode : GraphNode {
  std::shared_ptr<AudioStreamDescription> in;
  std::shared_ptr<AudioStreamDescription> out;
  std::thread thr;

  soxr* resampler = nullptr;
  std::vector<uint8_t> samplebuffer;
  OpusMSEncoder* state;
  size_t currentFrame = 0;
  uint32_t bitrate = 0;
  
  OpusConversionNode(std::shared_ptr<AudioStreamDescription> in, uint32_t bitrate) 
  : in(in)
  , out(std::make_shared<AudioStreamDescription>(AudioCodec::Opus, 48000, in->channels))
  , bitrate(bitrate)
  {
    if (in->samplingFrequency != out->samplingFrequency) {
      soxr_io_spec_t iospec = soxr_io_spec(SOXR_INT16_I, SOXR_INT16_I);
      soxr_error_t err;
      resampler = soxr_create(in->samplingFrequency, out->samplingFrequency, in->channels.size(), &err, &iospec, nullptr, nullptr);
    }
    int error;
    int streams, coupled_streams;
    unsigned char mapping[256];

    state = opus_multistream_surround_encoder_create(out->samplingFrequency, out->channels.size(), 1, &streams, &coupled_streams, mapping, OPUS_APPLICATION_AUDIO, &error);
    opus_multistream_encoder_ctl(state, OPUS_SET_BITRATE(bitrate));
    thr = std::thread([this]{ Run(); });
  }
  void Run();
  void EncodeBuffer(Origo::channel_writer<Packet>& out_chan);
  ~OpusConversionNode() {
    thr.join();
  }
};

std::shared_ptr<AudioStreamDescription> OpusEncode(Graph& graph, std::shared_ptr<AudioStreamDescription> in, uint32_t bitrate) {
  std::shared_ptr<OpusConversionNode> node = std::make_unique<OpusConversionNode>(in, bitrate);
  graph.Add(node);
  return node->out;
}

void OpusConversionNode::EncodeBuffer(Origo::channel_writer<Packet>& out_chan) {
  const size_t frameSize = 2*(out->channels.size() * out->samplingFrequency / 50);
  std::vector<uint8_t> outbuffer;
  outbuffer.resize(bitrate / 50); // we hard-limit on 3x bitrate. That's bitrate / 8 (bytes), / 20 (frames per sec), times 3... 
  int bytes = opus_multistream_encode((OpusMSEncoder*)state, (const int16_t*)samplebuffer.data(), frameSize/4, outbuffer.data(), outbuffer.size());
  outbuffer.resize(bytes);
  out_chan.add(Packet{std::move(outbuffer), currentFrame * 20000000, false});
  currentFrame++;
}

void OpusConversionNode::Run() {
  const size_t frameSize = 2*(out->channels.size() * out->samplingFrequency / 50);
  Origo::channel_reader<Packet> reader(in->packets);
  Origo::channel_writer<Packet> writer(out->packets);
  while (true) {
    auto opt_p = reader.get();
    if (not opt_p) break;
    auto p = *opt_p;
    size_t currentPosition = samplebuffer.size();
    if (resampler) {
      samplebuffer.resize(currentPosition + 2*4096 * out->channels.size());
      size_t odone = 0;
      soxr_process(resampler, p.data.data(), p.data.size() / 4, NULL, samplebuffer.data() + currentPosition, 4096, &odone);
      samplebuffer.resize(currentPosition + odone * out->channels.size() * 2);
    } else {
      // TODO: this is ugly memcpying over endian-dependent data
      samplebuffer.resize(samplebuffer.size() + p.data.size());
      memcpy(samplebuffer.data() + currentPosition, p.data.data(), p.data.size());
    }
    while (samplebuffer.size() >= frameSize) {
      EncodeBuffer(writer);
      memcpy(samplebuffer.data(), samplebuffer.data() + frameSize, samplebuffer.size() - frameSize);
      samplebuffer.resize(samplebuffer.size() - frameSize);
    }
  }

  if (resampler) {
    size_t odone = 0;
    do {
      size_t currentPosition = samplebuffer.size();
      samplebuffer.resize(currentPosition + 2*4096 * out->channels.size());
      soxr_process(resampler, nullptr, 0, nullptr, samplebuffer.data() + currentPosition, 4096, &odone);
      samplebuffer.resize(currentPosition + odone * out->channels.size() * 2);
    } while (odone > 0);
  }

  if (samplebuffer.size() > 0) {
    while (samplebuffer.size() < frameSize) {
      samplebuffer.push_back(0);
    }
    EncodeBuffer(writer);
  }
}

}


