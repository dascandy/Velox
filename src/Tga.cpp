#include "velox/Tga.hpp"
#include <cstring>
#include "velox/Transforms.hpp"
#include "bini/reader.hpp"
#include "bini/writer.hpp"
#include "velox/Color.hpp"
#include <cassert>
#include <optional>

namespace Velox {

Color readcolor(Bini::reader& r, uint8_t colorsize) {
  switch(colorsize) {
  case 15:
    return Color::from555(r.read16le());
  case 16: 
    return Color::from565(r.read16le());
  case 24: 
    return Color::from888(r.read24le());
  case 32: 
    return Color::from8888(r.read32le());
  default:
    assert(false);
    return {};
  }
}

Image LoadTga(std::span<const uint8_t> data) {
  Bini::reader r(data);
  uint8_t idLength = r.read8();
  uint8_t colorMapType = r.read8();
  uint8_t imagetype = r.read8();
  [[maybe_unused]] uint16_t firstEntry = r.read16le();
  uint16_t colorMapLength = r.read16le();
  uint8_t colorMapEntrySize = r.read8();
  [[maybe_unused]] uint16_t screen_x = r.read16le();
  [[maybe_unused]] uint16_t screen_y = r.read16le();
  uint16_t w = r.read16le();
  uint16_t h = r.read16le();
  uint8_t pixeldepth = r.read8();
  uint8_t imagedesc = r.read8();
  bool flipH = (imagedesc & 0x10) == 0x10;
  bool flipV = (imagedesc & 0x20) == 0;
  uint8_t alphaSize = imagedesc & 0xF;
  r.skip(idLength);
  std::vector<Color> colorMap;
  if (colorMapType == 1) {
    for (size_t n = 0; n < colorMapLength; n++) {
      colorMap.push_back(readcolor(r, colorMapEntrySize));
    }
  }
  Image img;
  if (pixeldepth == 1) {
    img = Image(w, h, PixelFormat::Bitmask);
  } else if (alphaSize == 0) {
    img = Image(w, h, PixelFormat::RGB8);
  } else {
    img = Image(w, h, PixelFormat::RGBA8);
  }

  uint16_t x = 0, y = 0;
  if (pixeldepth == 1) {
    std::abort();
  } else {
    if (imagetype & 8) {
      size_t n = 0;
      while (n < w*h) {
        uint8_t val = r.read8();
        if (val & 0x80) {
          Color color = readcolor(r, pixeldepth);
          for (size_t k = 0; k < (val & 0x7F) + 1; k++) {
            img.setPixel(x, y, color);
            x++;
            if (x == w) { 
              x = 0; 
              y++; 
            }
          }
        } else {
          for (size_t k = 0; k < (val & 0x7F) + 1; k++) {
            Color color = readcolor(r, pixeldepth);
            img.setPixel(x, y, color);
            x++;
            if (x == w) { 
              x = 0; 
              y++; 
            }
          }
        }
      }
    } else {
      for (size_t n = 0; n < w*h; n++) {
        Color color = readcolor(r, pixeldepth);
        img.setPixel(x, y, color);
        x++;
        if (x == w) { 
          x = 0; 
          y++; 
        }
      }
    }
  }
  if (flipV) {
    img = FlipVertical(img);
  }
  if (flipH) {
    img = FlipHorizontal(img);
  }
  return img;
}

static PixelFormat getTgaPixelFormat(PixelFormat format) {
  switch(format) {
  case PixelFormat::Grayscale8: 
  case PixelFormat::Grayscale16: 
  case PixelFormat::RGB8: 
  case PixelFormat::RGB16: 
  case PixelFormat::RGB32F: 
  case PixelFormat::BGR8: 
    return PixelFormat::BGR8;
  case PixelFormat::RGBA8: 
  case PixelFormat::RGB10A2: 
  case PixelFormat::RGBA16: 
  case PixelFormat::RGBA32F: 
  case PixelFormat::BGRA8: 
    return PixelFormat::BGRA8;
  case PixelFormat::Bitmask:
    return PixelFormat::Bitmask;
  }
}

std::vector<uint8_t> SaveTga(const Image& image) {
  PixelFormat targetFormat = getTgaPixelFormat(image.format);

  Bini::writer w;
  w.add8(0);
  w.add8(0);
  w.add8(targetFormat == PixelFormat::Bitmask ? 3 : 2);
  w.add16le(0);
  w.add16le(0);
  w.add8(0);
  w.add16le(0);
  w.add16le(0);
  w.add16le(image.width);
  w.add16le(image.height);
  w.add8(PixelFormatToBPP(targetFormat));
  w.add8(targetFormat == PixelFormat::BGRA8 ? 0x8 : 0x0);
  const Image* converted = &image;
  std::optional<Image> temporary_copy;
  if (targetFormat != image.format) {
    temporary_copy = image.ConvertTo(targetFormat);
    converted = &*temporary_copy;
  }
  for (size_t n = image.height; n --> 0;) {
    w.add(converted->GetLine(n));
  }
  return std::move(w);
}

}

