#pragma once

#include <set>
#include <string>

struct MovieInfo {
  std::set<std::string> genres;
  std::set<std::string> actors;
  std::string id;
  std::string title;
  int year;
  std::string posterUrl;
  std::string plot;
  std::string imdb_rating;
};

MovieInfo LookupMovieInfo(std::string imdbId);

