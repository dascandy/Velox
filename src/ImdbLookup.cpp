#include <raccoon/raccoon.hpp>
#include <string>
#include "ImdbLookup.hpp"
#include "fetch.hpp"

MovieInfo ParseMovieInfoPage(std::string id) {
  auto data = fetch("https://www.imdb.com/title/" + id + "/");
  std::string buffer((const char*)data.data(), (const char*)data.data() + data.size());
  MovieInfo info;
  auto tree = Raccoon::ParseDom(buffer, {});
  info.id = id;
  info.year = std::stol(std::string(tree->FindFirstElementRecursiveByAttribute("href", "/title/" + info.id + "/releaseinfo?ref_=tt_ov_rdat").text()));
  info.title = std::string(tree->FindFirstElementRecursiveByAttribute("data-testid", "hero__primary-text").text());
  info.posterUrl = tree->FindFirstElementRecursiveByAttribute("data-testid", "hero-media__poster").element(0)->element(0)->attributes["src"];
  for (auto& c : tree->FindFirstElementRecursiveByAttribute("data-testid", "genres").element(1)->children) {
    info.genres.insert(std::string(std::get<0>(c)->element(0)->text()));
  }
  info.plot = tree->FindFirstElementRecursiveByAttribute("data-testid", "plot").element(0)->text();
  info.imdb_rating = tree->FindFirstElementRecursiveByAttribute("data-testid", "hero-rating-bar__aggregate-rating__score").element(0)->text();
  for (auto& el : tree->FindElementsRecursive([&](const Raccoon::DomEntry& e) {
    auto it = e.attributes.find("href");
    if (it == e.attributes.end()) return false;
    return (it->second.starts_with("/name/nm") && it->second.ends_with("/?ref_=tt_ov_st"));
  })) {
    info.actors.emplace(el->text());
  }
  return info;
}


