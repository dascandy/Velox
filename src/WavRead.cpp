#include "velox/MuxStream.hpp"
#include <bini/reader.hpp>
#include <thread>
#include <map>
#include <manto/mapping.hpp>

namespace Velox {

struct WavMuxInStream : public MuxInStream {
public:
  int fd;
  std::thread thr; 
  uint32_t totalsamples;
  uint32_t samplerate;
  uint16_t channels;
  uint16_t bytespersample;
  mapping mapped;

  enum WavConstants {
    RIFF = 0x52494646,
    WAVE = 0x57415645,
    fmt = 0x666d7420,
    data = 0x64617461,
  };
  
  WavMuxInStream(mapping map)
  : mapped(std::move(map))
  {
    Bini::reader r(mapped.data());
    uint32_t riff_tag = r.read32be();
    uint32_t filesize = r.read32le();
    uint32_t wave_tag = r.read32be();
    uint32_t fmt_tag = r.read32be();
    uint32_t fmt_len = r.read32le();
    uint16_t format = r.read16le();
    channels = r.read16le();
    samplerate = r.read32le();
    uint32_t bytespersecond = r.read32le();
    bytespersample = r.read16le();
    uint16_t bitspersample = r.read16le();
    uint32_t data_tag = r.read32be();
    uint32_t datasize = r.read32le();
    if (riff_tag != RIFF ||
        wave_tag != WAVE ||
        fmt_tag != fmt ||
        data_tag != data ||
        fmt_len != 16 ||
        format != 1 ||
        filesize != mapped.data().size() - 8 ||
        datasize != filesize - 36 ||
        bitspersample != 16) {
      throw std::runtime_error("invalid file");
    }
    printf("%u\n", bytespersample);
    (void)bytespersecond;

    totalsamples = datasize / bytespersample;

    std::map<int, std::vector<AudioChannelPosition>> channels_to_mapping = {
      { 1, std::vector<AudioChannelPosition>{ AudioChannelPosition::Center }},
      { 2, std::vector<AudioChannelPosition>{ AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }}
    };
    if (not channels_to_mapping.contains(channels)) {
      throw std::runtime_error("Don't know how to map a " + std::to_string(channels) + " channel WAV file");
    }

    audiostreams.push_back(std::make_shared<AudioStreamDescription>(AudioCodec::LPCM, samplerate, channels_to_mapping[channels]));
    audiostreams.back()->bitdepth = bitspersample;
    thr = std::thread([this]{ Run(); });
  }
  ~WavMuxInStream() override {
    thr.join();
  }
  void Run() {
    Origo::channel_writer<Packet> wr(audiostreams.back()->packets);
    for (size_t n = 0; n < totalsamples; n += 512) {
      Packet p;
      p.timestamp = 1000000000ULL * n / samplerate;
      size_t thisPacket = (n + 512 >= totalsamples ? (totalsamples - n) : 512) * bytespersample;
      p.data.resize(thisPacket);
      memcpy(p.data.data(), mapped.data().data() + 44 + n * bytespersample, thisPacket);
      wr.add(std::move(p));
    }
  }
};

std::shared_ptr<MuxInStream> OpenWavIn(Graph& graph, mapping map) {
  auto p = std::make_shared<WavMuxInStream>(std::move(map));
  if (p)
    graph.Add(p);
  return p;
}

}


