#include "velox/Image.hpp"
#include <cassert>
#include <memory.h>

namespace Velox {

uint8_t PixelFormatToBPP(PixelFormat targetFormat) {
  switch(targetFormat) {
  case PixelFormat::Bitmask:
    return 1;
  case PixelFormat::Grayscale8:
    return 8;
  case PixelFormat::Grayscale16:
    return 16;
  case PixelFormat::RGB8:
  case PixelFormat::BGR8:
    return 24;
  case PixelFormat::RGBA8:
  case PixelFormat::BGRA8:
  case PixelFormat::RGB10A2:
    return 32;
  case PixelFormat::RGB16:
    return 48;
  case PixelFormat::RGBA16:
    return 64;
  case PixelFormat::RGB32F:
    return 96;
  case PixelFormat::RGBA32F:
    return 128;
  }
}

uint8_t PixelFormatToBPC(PixelFormat format) {
  switch(format) {
  case PixelFormat::Bitmask:
    return 1;
  case PixelFormat::RGB8:
  case PixelFormat::BGR8:
  case PixelFormat::RGBA8:
  case PixelFormat::BGRA8:
  case PixelFormat::Grayscale8:
    return 8;
  case PixelFormat::RGB10A2:
    return 10;
  case PixelFormat::RGB16:
  case PixelFormat::RGBA16:
  case PixelFormat::Grayscale16:
    return 16;
  case PixelFormat::RGB32F:
  case PixelFormat::RGBA32F:
    return 32;
  }
}

uint8_t PixelFormatToChannels(PixelFormat format) {
  switch(format) {
  case PixelFormat::Bitmask:
  case PixelFormat::Grayscale8:
  case PixelFormat::Grayscale16:
    return 1;
  case PixelFormat::RGB8:
  case PixelFormat::BGR8:
  case PixelFormat::RGB16:
  case PixelFormat::RGB32F:
    return 3;
  case PixelFormat::RGBA8:
  case PixelFormat::BGRA8:
  case PixelFormat::RGB10A2:
  case PixelFormat::RGBA16:
  case PixelFormat::RGBA32F:
    return 4;
  }
}

Image::Image(size_t width, size_t height, PixelFormat format) 
: width(width)
, height(height)
, format(format)
{
  buffer.resize((width*height*PixelFormatToBPP(format) + 7) / 8);
}

void Image::LoadFrom(ColorSpace cs, Image& im1, Image& im2, Image& im3) {
  for (size_t y = 0; y < height; y++) {
    for (size_t x = 0; x < width; x++) {
      setPixel(x, y, Color::fromColorspace(cs, im1.getPixel(x, y).red, im2.getPixel(x, y).red, im3.getPixel(x, y).red));
    }
  }
}

template <typename T>
inline T roundAndClamp(float value, T maximum) {
  float rv = std::round(value * maximum);
  if (rv < 0) return 0;
  if (rv > maximum) return maximum;
  return (T)rv;
}

void Image::Clear(Color color) {
  for (size_t y = 0; y < height; y++) {
    for (size_t x = 0; x < width; x++) {
      setPixel(x, y, color);
    }
  }
}

void Image::setPixel(uint32_t x, uint32_t y, Color color) {
  switch(format) {
    case PixelFormat::Bitmask:
    {
      size_t bitoffset = y*width+x;
      float lum = 0.299 * color.red + 0.587 * color.green + 0.114 * color.blue;
      if (lum > 0.5) {
        buffer[bitoffset / 8] |= (1 << (bitoffset % 8));
      } else {
        buffer[bitoffset / 8] &=  ~(1 << (bitoffset % 8));
      }
      break;
    }
    case PixelFormat::Grayscale8:
    {
      float lum = (0.299 * color.red + 0.587 * color.green + 0.114 * color.blue);
      buffer[y*width+x] = roundAndClamp<uint8_t>(lum, 255);
    }
      break;
    case PixelFormat::Grayscale16:
    {
      float lum = (0.299 * color.red + 0.587 * color.green + 0.114 * color.blue);
      uint16_t val = roundAndClamp<uint16_t>(lum, 65535);
      buffer[y*width+x] = val & 0xFF;
      buffer[y*width+x] = val >> 8;
    }
      break;
    case PixelFormat::RGB8:
      buffer[3*(y*width+x)] = roundAndClamp(color.red, 255);
      buffer[3*(y*width+x)+1] = roundAndClamp(color.green, 255);
      buffer[3*(y*width+x)+2] = roundAndClamp(color.blue, 255);
      break;
    case PixelFormat::BGR8:
      buffer[3*(y*width+x)] = roundAndClamp(color.blue, 255);
      buffer[3*(y*width+x)+1] = roundAndClamp(color.green, 255);
      buffer[3*(y*width+x)+2] = roundAndClamp(color.red, 255);
      break;
    case PixelFormat::RGBA8:
      buffer[4*(y*width+x)] = roundAndClamp(color.red, 255);
      buffer[4*(y*width+x)+1] = roundAndClamp(color.green, 255);
      buffer[4*(y*width+x)+2] = roundAndClamp(color.blue, 255);
      buffer[4*(y*width+x)+3] = roundAndClamp(color.alpha, 255);
      break;
    case PixelFormat::BGRA8:
      buffer[4*(y*width+x)] = roundAndClamp(color.blue, 255);
      buffer[4*(y*width+x)+1] = roundAndClamp(color.green, 255);
      buffer[4*(y*width+x)+2] = roundAndClamp(color.red, 255);
      buffer[4*(y*width+x)+3] = roundAndClamp(color.alpha, 255);
      break;
    case PixelFormat::RGB10A2:
    {
      uint32_t value = (roundAndClamp<uint32_t>(color.red, 1023) << 22) |
                       (roundAndClamp<uint32_t>(color.green, 1023) << 12) |
                       (roundAndClamp<uint32_t>(color.blue, 1023) << 2) |
                       (roundAndClamp<uint32_t>(color.alpha, 3) << 0);
  
      buffer[(y*width+x)*4] = value & 0xFF;
      buffer[(y*width+x)*4+1] = (value >> 8) & 0xFF;
      buffer[(y*width+x)*4+2] = (value >> 16) & 0xFF;
      buffer[(y*width+x)*4+3] = (value >> 24) & 0xFF;
      break;
    }
    case PixelFormat::RGB16:
    {
      uint16_t r = roundAndClamp<uint16_t>(color.red, 65535);
      uint16_t g = roundAndClamp<uint16_t>(color.green, 65535);
      uint16_t b = roundAndClamp<uint16_t>(color.blue, 65535);
      buffer[(y*width+x)*6+0] = r & 0xFF;
      buffer[(y*width+x)*6+1] = (r >> 8) & 0xFF;
      buffer[(y*width+x)*6+2] = g & 0xFF;
      buffer[(y*width+x)*6+3] = (g >> 8) & 0xFF;
      buffer[(y*width+x)*6+4] = b & 0xFF;
      buffer[(y*width+x)*6+5] = (b >> 8) & 0xFF;
      break;
    }
    case PixelFormat::RGBA16:
    {
      uint16_t r = roundAndClamp<uint16_t>(color.red, 65535);
      uint16_t g = roundAndClamp<uint16_t>(color.green, 65535);
      uint16_t b = roundAndClamp<uint16_t>(color.blue, 65535);
      uint16_t a = roundAndClamp<uint16_t>(color.alpha, 65535);
      buffer[(y*width+x)*8+0] = r & 0xFF;
      buffer[(y*width+x)*8+1] = (r >> 8) & 0xFF;
      buffer[(y*width+x)*8+2] = g & 0xFF;
      buffer[(y*width+x)*8+3] = (g >> 8) & 0xFF;
      buffer[(y*width+x)*8+4] = b & 0xFF;
      buffer[(y*width+x)*8+5] = (b >> 8) & 0xFF;
      buffer[(y*width+x)*8+6] = a & 0xFF;
      buffer[(y*width+x)*8+7] = (a >> 8) & 0xFF;
      break;
    }
    case PixelFormat::RGB32F:
    {
      float rgb[3] = {color.red, color.green, color.blue};
      memcpy(buffer.data() + 12 * (y*width + x), rgb, 12);
      break;
    }
    case PixelFormat::RGBA32F:
    {
      float rgb[4] = {color.red, color.green, color.blue, color.alpha};
      memcpy(buffer.data() + 16 * (y*width + x), rgb, 16);
      break;
    }
  }
}

Color Image::getPixel(uint32_t x, uint32_t y) const {
  assert(y<height);
  assert(x<width);
  switch(format) {
    case PixelFormat::Bitmask:
    {
      size_t bitoffset = y*width+x;
      return (buffer[bitoffset / 8] >> (bitoffset % 8)) ? Color{ 1.0f, 1.0f, 1.0f, 1.0f } : Color { 0.0f, 0.0f, 0.0f, 1.0f };
    }
    case PixelFormat::Grayscale8:
      return { 
        buffer[y*width+x] / 255.0f, 
        buffer[y*width+x] / 255.0f, 
        buffer[y*width+x] / 255.0f, 
        1.0f 
      };
    case PixelFormat::Grayscale16:
      return { 
        (buffer[2*(y*width+x) + 1] * 256 + buffer[2*(y*width+x)]) / 65535.0f, 
        (buffer[2*(y*width+x) + 1] * 256 + buffer[2*(y*width+x)]) / 65535.0f, 
        (buffer[2*(y*width+x) + 1] * 256 + buffer[2*(y*width+x)]) / 65535.0f, 
        1.0f
      };
    case PixelFormat::RGB8:
      return { 
        buffer[3*(y*width+x)] / 255.0f, 
        buffer[3*(y*width+x)+1] / 255.0f, 
        buffer[3*(y*width+x)+2] / 255.0f, 
        1.0f
      };
    case PixelFormat::BGR8:
      return { 
        buffer[3*(y*width+x)+2] / 255.0f, 
        buffer[3*(y*width+x)+1] / 255.0f, 
        buffer[3*(y*width+x)] / 255.0f, 
        1.0f
      };
    case PixelFormat::RGBA8:
      return { 
        buffer[4*(y*width+x)] / 255.0f, 
        buffer[4*(y*width+x)+1] / 255.0f, 
        buffer[4*(y*width+x)+2] / 255.0f, 
        buffer[4*(y*width+x)+3] / 255.0f
      };
    case PixelFormat::BGRA8:
      return { 
        buffer[4*(y*width+x)+2] / 255.0f, 
        buffer[4*(y*width+x)+1] / 255.0f, 
        buffer[4*(y*width+x)] / 255.0f, 
        buffer[4*(y*width+x)+3] / 255.0f 
      };
    case PixelFormat::RGB10A2:
    {
      uint32_t value = buffer[(y*width+x)*4] + 
                       buffer[(y*width+x)*4 + 1] * 0x100 +
                       buffer[(y*width+x)*4 + 2] * 0x10000 +
                       buffer[(y*width+x)*4 + 3] * 0x1000000;
      return { ((value >> 22) & 0x3FF) / 1023.0f, ((value >> 12) & 0x3FF) / 1023.0f, ((value >> 2) & 0x3F) / 1023.0f, ((value) & 0x3) / 3.0f };
    }
    case PixelFormat::RGB16:
      return { 
        (buffer[6*(y*width+x) + 1] * 256 + buffer[6*(y*width+x) + 0]) / 65535.0f, 
        (buffer[6*(y*width+x) + 3] * 256 + buffer[6*(y*width+x) + 2]) / 65535.0f, 
        (buffer[6*(y*width+x) + 5] * 256 + buffer[6*(y*width+x) + 4]) / 65535.0f, 
        1.0f
      };
    case PixelFormat::RGBA16:
      return { 
        (buffer[8*(y*width+x) + 1] * 256 + buffer[8*(y*width+x) + 0]) / 65535.0f, 
        (buffer[8*(y*width+x) + 3] * 256 + buffer[8*(y*width+x) + 2]) / 65535.0f, 
        (buffer[8*(y*width+x) + 5] * 256 + buffer[8*(y*width+x) + 4]) / 65535.0f, 
        (buffer[8*(y*width+x) + 7] * 256 + buffer[8*(y*width+x) + 6]) / 65535.0f, 
      };
    case PixelFormat::RGB32F:
    {
      float rgb[3];
      memcpy(rgb, buffer.data() + 12 * (y*width + x), 12);
      return {rgb[0], rgb[1], rgb[2], 1.0f};
    }
    case PixelFormat::RGBA32F:
    {
      float rgb[4];
      memcpy(rgb, buffer.data() + 16 * (y*width + x), 16);
      return {rgb[0], rgb[1], rgb[2], 1.0f};
    }
  }
}

std::span<const uint8_t> Image::GetLine(size_t line) const {
  size_t lineStride = PixelFormatToBPP(format) / 8 * width;
  std::span<const uint8_t> s = buffer;
  return s.subspan(lineStride * line, lineStride);
}

Image Image::ConvertTo(PixelFormat to) const {
  Image target;
  target.width = width;
  target.height = height;
  target.format = to;
  target.buffer.resize(PixelFormatToBPP(to) * width * height / 8);
  for (size_t y = 0; y < height; y++) {
    for (size_t x = 0; x < width; x++) {
      target.setPixel(x, y, getPixel(x, y));
    }
  }
  return target;
}


}


