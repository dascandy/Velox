#include "velox/MuxStream.hpp"
#include "EbmlWriter.hpp"
#include <map>
#include <cassert>

namespace Velox {

std::string VideoCodecToMkv(VideoCodec c) {
  switch(c) {
  case VideoCodec::AV1: return "V_AV1";
  case VideoCodec::VC1: return "V_MS/VFW/FOURCC";
  case VideoCodec::Mpeg1: return "V_MPEG1";
  case VideoCodec::Mpeg2: return "V_MPEG2";
  case VideoCodec::H264: return "V_MPEG4/ISO/AVC";
  case VideoCodec::H265: return "V_MPEGH/ISO/HEVC";
  case VideoCodec::VP8: return "V_VP8";
  case VideoCodec::VP9: return "V_VP9";
  case VideoCodec::Theora: return "V_THEORA";
  default: throw std::runtime_error("Unsupported codec");
  }
}

std::string AudioCodecToMkv(AudioCodec c) {
  switch(c) {
  case AudioCodec::MPEG1: // Doesn't differentiate between MPEG1 or 2
  case AudioCodec::MPEG2: return "A_MPEG/L3";
  case AudioCodec::AC3: return "A_AC3";
  case AudioCodec::TRUHD: return "A_TRUEHD";
  case AudioCodec::AC3PLUS: return "A_EAC3";
  case AudioCodec::LPCM: return "A_PCM/INT/LIT";
  case AudioCodec::DTS: // Apparently agreed to be detected at runtime. DTSHD does encode a full DTS stream too...
  case AudioCodec::DTSHD: return "A_DTS"; 
  case AudioCodec::Opus: return "A_OPUS";
  default: throw std::runtime_error("Unsupported codec");
  }
};

std::string SubtitleCodecToMkv(SubtitleCodec c) {
  switch(c) {
  case SubtitleCodec::Vobsub: return "S_VOBSUB";
  case SubtitleCodec::PG: return "S_HDMV/PGS";
  case SubtitleCodec::IG: return "S_HDMV/PGS";
  case SubtitleCodec::BlurayText: return "S_HDMV/TEXTST";
  default: throw std::runtime_error("Unsupported codec");
  }
}

struct MatroskaMuxOutStream : MuxOutStream {
  std::function<void(size_t, std::span<const uint8_t>)> write;
  std::vector<uint8_t> packetBuffer;
  size_t packetBufferStream = 0;
  std::map<int, std::vector<Tag>> tags;
  std::vector<ChapterMarking> chapters;
  std::vector<std::shared_ptr<VideoStreamDescription>> videostreams;
  std::vector<std::shared_ptr<AudioStreamDescription>> audiostreams;
  std::vector<std::shared_ptr<SubtitleStreamDescription>> subtitlestreams;
  std::map<StreamDescription*, int> streamids;
  size_t currentstream = 1;
  size_t offset = 0;
  EbmlWriter cluster;
  size_t clusterTimestamp = 0;
  size_t highestTimestamp = 0;
  MatroskaMuxOutStream(std::function<void(size_t, std::span<const uint8_t>)> write) 
  : write(write)
  {}
  ~MatroskaMuxOutStream() {
  }
  void AddTag(int level, Tag tag) override {
    tags[level].push_back(tag);
  }
  void AddChapterMarking(ChapterMarking marking) override {
    chapters.push_back(std::move(marking));
  }
  void AddStream(std::shared_ptr<VideoStreamDescription> desc) override {
    streamids[desc.get()] = currentstream++;
    videostreams.push_back(std::move(desc));
  }
  void AddStream(std::shared_ptr<AudioStreamDescription> desc) override {
    streamids[desc.get()] = currentstream++;
    audiostreams.push_back(std::move(desc));
  }
  void AddStream(std::shared_ptr<SubtitleStreamDescription> desc) override {
    streamids[desc.get()] = currentstream++;
    subtitlestreams.push_back(std::move(desc));
  }
  void SetDuration(uint64_t duration) override {
    highestTimestamp = duration;
  }
  void WriteHeader(size_t totalFileSize = 0) {
    EbmlWriter w;

    EbmlWriter header;
    header.add(mkv_ids::EbmlVersion, 1);
    header.add(mkv_ids::EbmlReadVersion, 1);
    header.add(mkv_ids::EbmlMaxIdLength, 4);
    header.add(mkv_ids::EbmlMaxSizeLength, 8);
    header.add(mkv_ids::DocType, "matroska");
    header.add(mkv_ids::DocTypeVersion, 4);
    header.add(mkv_ids::DocTypeReadVersion, 2);
    w.add(mkv_ids::EbmlHeader, header);

    // This is to be replaced with the actual file remaining size when known
    w.writevint((size_t)mkv_ids::Segment);
    if (totalFileSize == 0) {
      w.add8(0xFF);
    } else {
      w.writevint(totalFileSize - w.size() - 8, 8);
    }
    EbmlWriter info;
    if (clusterTimestamp > 0)
      info.addFloat(mkv_ids::Duration, highestTimestamp / 1000000.0f);
    info.add(mkv_ids::TimestampScale, 1000000);
    info.add(mkv_ids::MuxingApp, "libvelox-0.0.1");
    info.add(mkv_ids::WritingApp, "libvelox-0.0.1");
    w.addcrc(mkv_ids::Info, info);

    EbmlWriter tracks;
    for (auto& stream : videostreams) {
      EbmlWriter track;
      track.add(mkv_ids::TrackNumber, streamids.find(stream.get())->second);
      track.add(mkv_ids::TrackType, 1);
      track.add(mkv_ids::FlagLacing, 0);
      track.add(mkv_ids::Language, "und");
      track.add(mkv_ids::CodecId, VideoCodecToMkv(stream->codec));
      track.add(mkv_ids::DefaultDuration, size_t(1000000000 / stream->framerate));
      EbmlWriter video;
      video.add(mkv_ids::FlagInterlaced, 1); // TODO
      video.add(mkv_ids::FieldOrder, 9); // TODO
      video.add(mkv_ids::PixelWidth, stream->imagesize.first);
      video.add(mkv_ids::PixelHeight, stream->imagesize.second);
      video.add(mkv_ids::DisplayUnit, 3); // Aspect ratio
      video.add(mkv_ids::DisplayWidth, stream->aspectratio.first);
      video.add(mkv_ids::DisplayHeight, stream->aspectratio.second);
      track.add(mkv_ids::Video, video);
      tracks.add(mkv_ids::TrackEntry, track);
    }
    for (auto& stream : audiostreams) {
      EbmlWriter track;
      track.add(mkv_ids::TrackNumber, streamids.find(stream.get())->second);
      track.add(mkv_ids::TrackType, 2);
      track.add(mkv_ids::FlagLacing, 0);
      track.add(mkv_ids::Language, stream->language);
      track.add(mkv_ids::CodecId, AudioCodecToMkv(stream->codec));
      EbmlWriter audio;
      audio.add(mkv_ids::Channels, stream->channels.size());
      audio.addFloat(mkv_ids::SamplingFrequency, (float)stream->samplingFrequency);
      if (stream->bitdepth > 0) {
        audio.add(mkv_ids::BitDepth, stream->bitdepth);
      }
      track.add(mkv_ids::Audio, audio);
      tracks.add(mkv_ids::TrackEntry, track);
      switch(stream->codec) {
        case AudioCodec::Opus:
          audio.add(mkv_ids::SeekPreRoll, 80000000);
          break;
        default:
          break;
      }
    }
    for (auto& stream : subtitlestreams) {
      EbmlWriter track;
      track.add(mkv_ids::TrackNumber, streamids.find(stream.get())->second);
      track.add(mkv_ids::TrackType, 17);
      track.add(mkv_ids::FlagLacing, 0);
      track.add(mkv_ids::Language, stream->language);
      track.add(mkv_ids::CodecId, SubtitleCodecToMkv(stream->format));
      tracks.add(mkv_ids::TrackEntry, track);
    }
    w.addcrc(mkv_ids::Tracks, tracks);

    if (not chapters.empty()) {
      EbmlWriter entry;
      size_t chapterId = 0;
      for (auto& chapter : chapters) {
        ++chapterId;
        EbmlWriter atom;
        atom.add(mkv_ids::ChapterUID, chapterId);
        EbmlWriter chapstring;
        chapstring.add(mkv_ids::ChapString, chapter.name);
        atom.add(mkv_ids::ChapterDisplay, chapstring);
        atom.add(mkv_ids::ChapterTimeStart, chapter.timestamp);
        if (chapter.duration) {
          atom.add(mkv_ids::ChapterTimeEnd, chapter.timestamp + chapter.duration);
        }
        entry.add(mkv_ids::ChapterAtom, atom);
      }
      EbmlWriter chaps;
      chaps.add(mkv_ids::EditionEntry, entry);
      w.addcrc(mkv_ids::Chapters, chaps);
    }

    if (not tags.empty()) {
      EbmlWriter tsw;
      for (auto& [level, ts] : tags) {
        EbmlWriter tag;
        {
          EbmlWriter targets;
          targets.add(mkv_ids::TargetTypeValue, level);
          if (level == 50) targets.add(mkv_ids::TargetType, "ALBUM");
          else if (level == 30) targets.add(mkv_ids::TargetType, "TRACK");
          tag.add(mkv_ids::Targets, targets);
        }
        for (auto& [key, value, startTime, endTime] : ts) {
          EbmlWriter simpletag;

          simpletag.add(mkv_ids::TagName, key);
          simpletag.add(mkv_ids::TagString, value);

          tag.add(mkv_ids::SimpleTag, simpletag);
        }
        tsw.add(mkv_ids::Tag, tag);
      }
      w.addcrc(mkv_ids::Tags, tsw);
    }

    std::vector<uint8_t> zeroes;
    zeroes.resize(4096 - 3 - w.size());
    w.writevint((int)mkv_ids::Void);
    w.writevint(zeroes.size(), 2);
    w.Bini::writer::add(zeroes);
    std::vector<uint8_t> out = w;
    assert(out.size() == 4096);
    write(0, out);
    offset = out.size();
  }
  void Run() override {
    WriteHeader(0);
    uint64_t currentTime = 0;
    std::map<int, Origo::channel_reader<Packet>> readers;
    for (auto& [stream, id] : streamids) {
      readers.emplace(id, stream->packets);
    }
    clusterTimestamp = 0;
    cluster.add(mkv_ids::Timestamp, 0);

    while (true) {
      if (currentTime - clusterTimestamp >= 10000000000) {
        EbmlWriter w;
        w.add(mkv_ids::Cluster, cluster);
        write(offset, w);
        offset += w.size();
        cluster = {};
        clusterTimestamp = currentTime;
        cluster.add(mkv_ids::Timestamp, clusterTimestamp / 1000000);
      }
      currentTime += 10000000;

      size_t emptyReaders = 0;
      for (auto& [id, r] : readers) {
        auto p = r.peek();
        if (not p) {
          emptyReaders++;
        } else {
          while (p && (*p)->timestamp < currentTime) {
            auto p2 = r.get();
            if (not p2->data.empty()) {
              if (p2->isKeyframe && currentTime != clusterTimestamp - 10000000) {
                EbmlWriter w;
                w.add(mkv_ids::Cluster, cluster);
                write(offset, w);
                offset += w.size();
                cluster = {};
                clusterTimestamp = currentTime - 10000000;
                cluster.add(mkv_ids::Timestamp, clusterTimestamp / 1000000);
              }

              EbmlWriter block;
              block.writevint(id);
              block.add16be(int16_t(((int64_t)p2->timestamp - (int64_t)clusterTimestamp) / 1000000));
              if (p2->timestamp > highestTimestamp) {
                highestTimestamp = p2->timestamp;
              }
              block.add8(0);
              block.Bini::writer::add(p2->data);
              cluster.add(mkv_ids::SimpleBlock, block);
            }
            p = r.peek();
          }
        }
      }
      if (emptyReaders == readers.size()) break;
    }
    EbmlWriter w;
    w.add(mkv_ids::Cluster, cluster);
    write(offset, w);
    offset += cluster.size();

    WriteHeader(offset);
  }
};

std::shared_ptr<MuxOutStream> OpenMatroskaOut(Graph& g, std::function<void(size_t, std::span<const uint8_t>)> write) {
  auto p = std::make_shared<MatroskaMuxOutStream>(std::move(write));
  g.Add(p);
  return p;
}

}

