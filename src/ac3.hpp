#pragma once

namespace Velox {

std::tuple<std::vector<AudioChannelPosition>, int, int> ac3_packet_to_channels(std::span<const uint8_t> firstPacket) {
  Bini::bitreader<> r(firstPacket);
  uint16_t syncval = r.get(16); // sync
  if (syncval != 0x0B77) {
    return {};
  }
  uint8_t bitstream_id = firstPacket[5] >> 3;
  uint8_t samplingrate_id = firstPacket[4] >> 6;
  int samplingrate;
  switch(samplingrate_id) {
    case 0: samplingrate = 48000; break;
    case 1: samplingrate = 44100; break;
    case 2: samplingrate = 32000; break;
    default:
    case 3: samplingrate = 0; break;
  }

  uint8_t channelValue;
  if (bitstream_id < 10) {
    r.get(32);
    channelValue = r.get(3);
    switch(channelValue) {
    case 0:
    case 1: break;
    case 2: 
    case 3: 
    case 4: 
    case 6: r.get(2); break;
    case 5: 
    case 7: r.get(4); break;
    }
    bool lfe = r.get(1);
    if (lfe) channelValue += 8;
  } else if (bitstream_id < 17) {
    r.get(20);
    channelValue = r.get(3);
    bool lfe = r.get(1);
    if (lfe) channelValue += 8;
  } else {
    return {{}, 0, 0};
  }

  std::vector<AudioChannelPosition> channels;
  switch(channelValue) {
  case 0: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }; break;
  case 1: channels = { AudioChannelPosition::Center }; break;
  case 2: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }; break;
  case 3: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight }; break;
  case 4: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter }; break;
  case 5: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter }; break;
  case 6: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight }; break;
  case 7: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight }; break;
  case 8: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight , AudioChannelPosition::LFE }; break;
  case 9: channels = { AudioChannelPosition::Center , AudioChannelPosition::LFE }; break;
  case 10: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight , AudioChannelPosition::LFE }; break;
  case 11: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight , AudioChannelPosition::LFE }; break;
  case 12: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter , AudioChannelPosition::LFE }; break;
  case 13: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter , AudioChannelPosition::LFE }; break;
  case 14: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight , AudioChannelPosition::LFE }; break;
  case 15: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight , AudioChannelPosition::LFE }; break;
  default: channels = {}; break;
  }
  return { std::move(channels), 24, samplingrate };
}

}


