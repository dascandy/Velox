#pragma once

#include <cstdint>
#include <string>

namespace Velox {

const constexpr uint64_t PNG_MAGIC = 0x89504E470D0A1A0A;

struct ChunkType {
  enum Type {
    IHDR = 0x49484452,
    PLTE = 0x504c5445,
    IDAT = 0x49444154,
    IEND = 0x49454e44,
    tRNS = 0x74524e53,
    cHRM = 0x6348524d,
    gAMA = 0x67414d41,
    iCCP = 0x69434350,
    sBIT = 0x73424954,
    sRGB = 0x73524742,
    iTXt = 0x69545874,
    tEXt = 0x74455874,
    zTXt = 0x7a545874,
    bKGD = 0x624b4744,
    hIST = 0x68495354,
    pHYs = 0x70485973,
    sPLT = 0x73504c54,
    tIME = 0x74494d45,

    acTL = 0x6163544c,
    fcTL = 0x6663544c,
    fdAT = 0x66644154,
  };

  uint32_t value;
  ChunkType(Type t) : value((uint32_t)t) {}
  bool isCritical() { return (value & 0x20000000) == 0x20000000; }
  bool isPrivate() { return (value & 0x200000) == 0x200000; }
  bool isReserved() { return (value & 0x2000) == 0x2000; }
  bool isSafeToCopy() { return (value & 0x20) == 0x20; }
};

enum class ColorType {
  Greyscale = 0,
  TrueColor = 2,
  IndexedColor = 3,
  GreyscaleAlpha = 4,
  TruecolorAlpha = 6,
};

enum class Filter {
  None = 0,
  Sub = 1,
  Up = 2,
  Average = 3,
  Paeth = 4,
};

}

