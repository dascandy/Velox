#pragma once

#include <array>
#include <cstdint>
#include <utility>
#include <span>
#include <vector>
#include <scsi/sg.h>

std::array<uint8_t, 10> ScsiCommand(uint8_t command, uint8_t mode, uint8_t buffer, uint32_t offset, uint32_t arglength, uint8_t controlbyte);
sg_io_hdr_t create_command(std::span<const uint8_t> command, bool read, bool write, std::span<const uint8_t> buffer, std::span<uint8_t> sense);
std::pair<bool, std::vector<uint8_t>> readbuffer(int fd, uint8_t buffer, uint32_t offset, uint32_t size);
std::pair<bool, std::vector<uint8_t>> writebuffer(int fd, uint8_t buffer, uint32_t offset, std::span<const uint8_t> data);
