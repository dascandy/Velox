#include <cstring>
#include <bini/reader.hpp>
#include <cstdio>
#include "velox/Png.hpp"
#include "PngStructs.hpp"
#include <decoco/decoco.hpp>
#include <fstream>

namespace Velox {

struct PngReader : private Bini::reader {
  PngReader(std::span<const uint8_t> data)
  : Bini::reader(data)
  {
    if (read64be() != PNG_MAGIC) {
      // Invalid file
    }
  }
  std::pair<uint32_t, std::span<const uint8_t>> getChunk() {
    if (sizeleft() < 12) {
      return { (uint32_t)0, std::span<const uint8_t>{} };
    }
    uint32_t len = read32be();
    uint32_t type = read32be();
    std::span<const uint8_t> data = get(len);
    uint32_t crc = read32be();
    (void)crc;
    return { (uint32_t)type, data };
  }
  using Bini::reader::sizeleft;
};

struct PngImageInfo {
  std::vector<Color> palette;
  std::span<const uint8_t> trns;
  ColorType colortype;
  uint8_t bitdepth;
  uint8_t channels;
  uint8_t compression;
  uint8_t filter;
  uint8_t interlace;
};

Color readpixel(Bini::bitreader<>& reader, const PngImageInfo& info) {
  const float maxvalue = (1 << info.bitdepth) - 1;
  switch(info.colortype) {
  case ColorType::Greyscale:
  {
    float y = reader.get(info.bitdepth) / maxvalue;
    return Color{y, y, y};
  }
  case ColorType::GreyscaleAlpha:
  {
    float y = reader.get(info.bitdepth) / maxvalue;
    float a = reader.get(info.bitdepth) / maxvalue;
    return Color{y, y, y, a};
  }
  case ColorType::TrueColor:
  {
    float r = reader.get(info.bitdepth) / maxvalue;
    float g = reader.get(info.bitdepth) / maxvalue;
    float b = reader.get(info.bitdepth) / maxvalue;
    return Color{r, g, b};
  } 
  case ColorType::TruecolorAlpha:
  {
    float r = reader.get(info.bitdepth) / maxvalue;
    float g = reader.get(info.bitdepth) / maxvalue;
    float b = reader.get(info.bitdepth) / maxvalue;
    float a = reader.get(info.bitdepth) / maxvalue;
    return Color{r, g, b, a};
  } 
  case ColorType::IndexedColor:
  {
    uint8_t val = reader.get(info.bitdepth);
    if (val >= info.palette.size()) return {};
    return info.palette[val];
  }
  }
  return {};
}

struct PngStream : VideoStream {
  struct ImageDataReference {
    std::vector<std::span<const uint8_t>> imageData;
    ImageDataReference* overlaidOn = nullptr;
    uint32_t width;
    uint32_t height;
    uint32_t xoff;
    uint32_t yoff;
    // In units of 1/300ths of a second
    size_t delay = 0;
    size_t displayTime = 0;
    uint8_t blendStyle = 0;
  };
  std::span<const uint8_t> palette;
  std::span<const uint8_t> trns;
  std::vector<ImageDataReference> frames;
  Image currentImage;
  ImageDataReference* currentReference = nullptr;
  PngImageInfo info;
  size_t totalDuration = 0;
  size_t loopCount = 1;
  PngStream(std::span<const uint8_t> data) {
    rate = Framerate::F300;
    frames.resize(1);
    PngReader r(data);
    ImageDataReference* overlaidOn = nullptr;
    
    while (r.sizeleft() > 0) {
      auto [type, data] = r.getChunk();
      switch(type) {
        case ChunkType::IHDR:
        {
          printf("ihdr\n");
          Bini::reader r(data);
          uint32_t width = r.read32be();
          uint32_t height = r.read32be();
          info.bitdepth = r.read8();
          info.colortype = (ColorType)r.read8();
          info.compression = r.read8();
          info.filter = r.read8();
          info.interlace = r.read8();
          PixelFormat pf = PixelFormat::RGBA16;
          switch(info.colortype) {
          case ColorType::Greyscale:
            info.channels = 1;
            pf = (info.bitdepth == 16) ? PixelFormat::Grayscale16 : PixelFormat::Grayscale8;
            break;
          case ColorType::TrueColor:
          case ColorType::IndexedColor:
            info.channels = 3;
            pf = (info.bitdepth == 16) ? PixelFormat::RGB16 : PixelFormat::RGB8;
            break;
          case ColorType::GreyscaleAlpha:
            info.channels = 2;
            pf = (info.bitdepth == 16) ? PixelFormat::RGBA16 : PixelFormat::RGBA8;
            break;
          case ColorType::TruecolorAlpha:
            info.channels = 4;
            pf = (info.bitdepth == 16) ? PixelFormat::RGBA16 : PixelFormat::RGBA8;
            break;
          default:
            throw 42;
          }
          currentImage = Image(width, height, pf);
        }
          break;

        case ChunkType::PLTE:
        {
          printf("plte\n");
          Bini::reader plt(data);
          while (plt.sizeleft()) {
            info.palette.push_back(Color::from888(plt.read24be()));
          }
        }
          break;
        case ChunkType::tRNS: // After PLTE; before IDAT
        {
          printf("trns\n");
          Bini::reader trns(data);
          size_t index = 0;
          while (trns.sizeleft() && index < info.palette.size()) {
            info.palette[index++].alpha = (trns.read8() / 255.0f);
          }
        }
          break;

        case ChunkType::IDAT:
          printf("idat\n");
          frames.back().imageData.push_back(data);
          break;
          
        case ChunkType::IEND:
          printf("iend\n");
          break;

        case ChunkType::acTL:
        {
          printf("actl\n");
          Bini::reader actl(data);
          frames.clear();
          frames.reserve(actl.read32be());
          loopCount = actl.read32be();
        }
          break;
        case ChunkType::fcTL:
        {
          printf("fctl\n");
          Bini::reader fctl(data);
          fctl.read32be();
          frames.push_back({});
          frames.back().width = fctl.read32be();
          frames.back().height = fctl.read32be();
          frames.back().xoff = fctl.read32be();
          frames.back().yoff = fctl.read32be();
          uint16_t delay_num = fctl.read16be();
          uint16_t delay_den = fctl.read16be();
          frames.back().delay = (150 + 300 * delay_num) / delay_den;
          uint8_t dispose_op = fctl.read8();
          uint8_t blend_op = fctl.read8();
          frames.back().overlaidOn = overlaidOn;
          
          switch(dispose_op) {
          case 0:
            overlaidOn = &frames.back();
            break;
          case 1:
            overlaidOn = nullptr;
            break;
          case 2:
            overlaidOn = frames[frames.size() - 2].overlaidOn;
            break;
          }
          frames.back().blendStyle = blend_op;
        }
          break;
        case ChunkType::fdAT:
        {
          printf("fdat\n");
          Bini::reader fdat(data);
          fdat.read32be();
          frames.back().imageData.push_back(fdat.get(fdat.sizeleft()));
        }
          break;
        case ChunkType::gAMA:
        {
          Bini::reader gama(data);
          currentImage.gamma = 100000.0f / gama.read32be();
        }
          break;
        case ChunkType::bKGD: // background color, ignore
        case ChunkType::sPLT: // suggested palette, ignore
        case ChunkType::hIST: // palette use histogram, ignore
        case ChunkType::pHYs: // physical image size info, ignore
          break;
        case ChunkType::tIME: // Last modification time, ignore
          break;
        // Color information that we ignore (for now)
        case ChunkType::cHRM:
        case ChunkType::sBIT:
        case ChunkType::iCCP:
        case ChunkType::sRGB:
          break;
        case ChunkType::iTXt:
        case ChunkType::tEXt:
        case ChunkType::zTXt:
          // ignore text
          break;

        default:
          printf("Unknown png chunk type %08x\n", type);
          break;
      }
    }
    for (auto& frame : frames) {
      frame.displayTime = totalDuration;
      totalDuration += frame.delay;
    }
    Decode(frames[0]);
  }
  const Image& get() override { 
    return currentImage; 
  }
  bool seek(size_t frameno) override {
    size_t current = 0;
    while (frames[current].displayTime + frames[current].delay < frameno) current++;
    printf("switching to frame %zu, displaytime %zu delay %zu\n", current, frames[current].displayTime, frames[current].delay);
    return Decode(frames[current]);
  }
  bool Decode(ImageDataReference& ref) {
    if (&ref == currentReference) return false;
    if (ref.overlaidOn == nullptr) {
      currentImage.Clear();
    } else if (ref.overlaidOn != currentReference) {
      Decode(*ref.overlaidOn);
    }

    std::vector<uint8_t> output;
    size_t stride = (ref.width * info.bitdepth * info.channels + 7) / 8;
    output.resize((1 + stride) * currentImage.height);
    std::unique_ptr<Decoco::Decompressor> decomp = Decoco::ZlibDecompressor();
    size_t offset = 0;
    for (auto& chunk : ref.imageData) {
      auto result = decomp->decompress(chunk, std::span<uint8_t>(output).subspan(offset));
      offset += result.size();
    }
    uint8_t filterstride = (info.bitdepth * info.channels + 7) / 8;
    Bini::reader r(output);
    std::vector<uint8_t> linebuffer;
    std::vector<uint8_t> lastlinebuffer;
    linebuffer.resize(stride);
    lastlinebuffer.resize(stride);
    for (size_t n = 0; n < ref.height; n++) {
      uint8_t filter = r.read8();
      std::span<const uint8_t> line = r.get(stride);
      Decoco::PngUnfilter(filter, filterstride, linebuffer, lastlinebuffer, line);
      Bini::bitreader r2(linebuffer);
      if (ref.blendStyle) {
        for (size_t h = 0; h < currentImage.width; h++) {
          Color c = readpixel(r2, info);
          Color in = currentImage.getPixel(ref.xoff + h, ref.yoff + n);
          currentImage.setPixel(ref.xoff + h, ref.yoff + n, Color::Blend(in.GammaCorrect(currentImage.gamma), c.GammaCorrect(currentImage.gamma), c.alpha).GammaInvCorrect(currentImage.gamma));
        }
      } else {
        for (size_t h = 0; h < currentImage.width; h++) {
          currentImage.setPixel(ref.xoff + h, ref.yoff + n, readpixel(r2, info));
        }
      }
      std::swap(linebuffer, lastlinebuffer);
    }
    currentReference = &ref;
    return true;
  }
};

std::unique_ptr<VideoStream> LoadPngVideo(std::span<const uint8_t> data) {
  return std::make_unique<PngStream>(data);
}

Image LoadPng(std::span<const uint8_t> data) {
  return PngStream(data).get();
}

}


