#pragma once

#include "Image.hpp"

Image TgaLoad(std::span<const uint8_t> data);
std::vector<uint8_t> TgaSave(const Image&);


