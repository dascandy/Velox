#pragma once

#include "bini/writer.hpp"
#include "mkv_ids.hpp"
#include "caligo/crc.hpp"

inline uint64_t maxValueFor(uint8_t byteCount) {
  return (1 << (byteCount * 7)) - 1;
}

inline int64_t subValueFor(uint8_t byteCount) {
  return (1 << (byteCount * 7 - 1)) - 1;
}

size_t minsizefor(size_t value) {
  if (value < 0xFFFFFFF) {
    if (value < 0x3FFF) {
      return (value < 0x7F) ? 1 : 2;
    } else {
      return (value < 0x1FFFFF) ? 3 : 4;
    }
  } else {
    if (value < 0x3FFFFFFFFFF) {
      return (value < 0x7FFFFFFFF) ? 5 : 6;
    } else {
      return (value < 0x1FFFFFFFFFFFF) ? 7 : 8;
    }
  } // Numbers of more than 56 bits not accounted for
}

struct EbmlWriter : public Bini::writer {
  using writer::writer;
  void writevint(size_t value, size_t minsize = 1) {
    minsize = std::max(minsize, minsizefor(value));
    value |= (1ULL << (minsize * 7));
    for (int n = minsize; n --> 0;) {
      uint8_t v = (value >> (8*n));
      add8(v);
    }
  }
  void add(mkv_ids id, Bini::writer w) {
    writevint((size_t)id);
    writevint(w.size());
    Bini::writer::add((std::vector<uint8_t>)w);
  }
  void addcrc(mkv_ids id, Bini::writer w) {
    writevint((size_t)id);
    writevint(w.size() + 6);
    add8(0xbf);
    add8(0x84);
    Bini::writer::add(Caligo::CRC32(w).data());
    Bini::writer::add((std::vector<uint8_t>)w);
  }
  void add(mkv_ids id, size_t value) {
    writevint((size_t)id);
    size_t bytes = 1;
    size_t c = value;
    while (c > 0xFF) { c >>= 8; bytes++; }
    writevint(bytes);
    addSizedbe(value, bytes);
  }
  void addFloat(mkv_ids id, float value) {
    writevint((size_t)id);
    add8(0x84);
    addfloatbe(value);
  }
  void add(mkv_ids id, std::string_view value) {
    writevint((size_t)id);
    writevint(value.size());
    Bini::writer::add(value.data());
  }
};

