#include "velox/MuxStream.hpp"
#include <dvdnavpp/dvdnavpp.hpp>
#include <bini/reader.hpp>
#include <map>
#include <list>
#include <caligo/base64.hpp>

namespace Velox {

AudioCodec DvdAudioToAudioCodec(std::string str) {
  if (str == "ac3") return AudioCodec::AC3;
  if (str == "dts") return AudioCodec::DTS;
  if (str == "lpcm") return AudioCodec::LPCM;
  if (str == "mpeg2") return AudioCodec::MPEG2;
  throw std::runtime_error("Invalid audio codec: " + str);
}

std::vector<AudioChannelPosition> DvdChannelsToLayout(AudioCodec codec, int value) {
  switch(codec) {
  case AudioCodec::AC3:
    switch(value) {
    case 0: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight };
    case 1: return { AudioChannelPosition::Center };
    case 2: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight };
    case 3: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight };
    case 4: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter };
    case 5: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter };
    case 6: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight };
    case 7: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight };
    case 8: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight , AudioChannelPosition::LFE };
    case 9: return { AudioChannelPosition::Center , AudioChannelPosition::LFE };
    case 10: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight , AudioChannelPosition::LFE };
    case 11: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight , AudioChannelPosition::LFE };
    case 12: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter , AudioChannelPosition::LFE };
    case 13: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter , AudioChannelPosition::LFE };
    case 14: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight , AudioChannelPosition::LFE };
    case 15: return { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight , AudioChannelPosition::LFE };
    }
  default:
  case AudioCodec::DTS:
    break;
  }
  return {};
}

struct OutputChannel {
  OutputChannel(Origo::channel<Packet>& channel)
  : writer(channel)
  , last_timestamp(0)
  {}
  Origo::channel_writer<Packet> writer;
  std::vector<uint8_t> buffer;
  uint64_t last_timestamp;
  void AddToChannel(uint8_t channel_id, std::vector<uint8_t> newdata, uint64_t timestamp);
  size_t mpegreclock = 0;
  size_t framesSinceKeyframe = 0;
};

struct DvdMuxInStream : public MuxInStream {
public:
  dvdnav nav;
  std::array<uint8_t, 2048> buffer;
  size_t buttonsToSkip = 10;
  Bini::reader buffer_reader;
  bool streamDone = false;
  uint64_t timestamp = 0;
  uint64_t pes_timestamp = 0;
  size_t lastVobuEndPtm = 0;
  size_t timestampOffset = 0;
  std::map<uint8_t, OutputChannel> channels;
  std::thread thr;
  ~DvdMuxInStream() {
    thr.join();
  }
  DvdMuxInStream(std::string device)
  : nav(device)
  , buffer_reader(buffer)
  {
    nav.set_readahead_flag(false);
    nav.set_pgc_positioning_flag(true);
    nav.menu_language("en");
    nav.audio_language("en");
    nav.spu_language("en");
    title = nav.get_title();
    bool atStartOfMainFeature = false;
    while (not atStartOfMainFeature) {
      auto [block, type] = nav.get_next_block(buffer);
      switch(type) {
      case DVDNAV_STILL_FRAME:
        nav.still_skip();
        break;
      case DVDNAV_WAIT:
        nav.wait_skip();
        break;
      case DVDNAV_CELL_CHANGE:
        try {
          auto [pos, len] = nav.get_position();
          if (len > 1000000 && not atStartOfMainFeature) {
            atStartOfMainFeature = true;
            try {
              auto video_aspect = nav.get_video_aspect() == 0 ? std::pair<int, int>{4,3} : std::pair<int, int>{16,9};
              auto [w, h] = nav.get_video_resolution();
              float framerate = nav.get_frame_rate();
              videostreams.push_back(std::make_shared<VideoStreamDescription>(VideoCodec::Mpeg2, framerate, std::pair<int, int>{w, h}, video_aspect));
              channels.emplace(0, videostreams.back()->packets);

              for (size_t n = 0; n < 255; n++) {
                int8_t aud_stream = nav.get_audio_logical_stream(n);
                if (aud_stream == -1) break;
                auto lang = nav.audio_stream_to_lang(n);
                uint32_t chan = nav.audio_stream_channels(n);
                printf("%zu %x %s %u\n", n, aud_stream + 0x80, lang.c_str(), chan);
                AudioCodec codec = DvdAudioToAudioCodec(nav.audio_stream_format(n));
                audiostreams.push_back(std::make_shared<AudioStreamDescription>(DvdAudioToAudioCodec(nav.audio_stream_format(n)), 48000, DvdChannelsToLayout(codec, chan)));
                audiostreams.back()->language = lang;
                channels.emplace(0x80 + aud_stream, audiostreams.back()->packets);
              }
              for (size_t n = 0; n < 255; n++) {
                int8_t spu_stream = nav.get_spu_logical_stream(n);
                if (spu_stream == -1) break;
                auto lang = nav.spu_stream_to_lang(n);
                printf("%zu %x %s\n", n, spu_stream + 0x20, lang.c_str());
                subtitlestreams.push_back(std::make_shared<SubtitleStreamDescription>(SubtitleCodec::Vobsub, lang));
                channels.emplace(spu_stream + 0x20, subtitlestreams.back()->packets);
              }
              auto [title_no, current_chapter] = nav.current_title_info();
              auto [chaps, duration] = nav.describe_title_chapters(title_no);
              size_t number = 1;
              for (size_t n = 0; n < chaps.size(); n++) {
                size_t end = (n == chaps.size() - 1) ? duration : chaps[n+1];
                chapters.push_back({ 100000 * chaps[n] / 9, std::to_string(number++), 100000 * (end - chaps[n]) / 9 });
              }
              thr = std::thread([this]{ Run(); });
            }
            catch (std::exception& e) {
              printf("error %s\n", e.what());
            }
          }
        } catch (...) {}
        break;
      case DVDNAV_NAV_PACKET:
        {
          pci_t* pci = nav.get_current_nav_pci();
          [[maybe_unused]] dsi_t* dsi = nav.get_current_nav_dsi();
          if (pci->hli.hl_gi.btn_ns > 0) {
            if (buttonsToSkip) {
              buttonsToSkip--;
            } else {
              nav.button_select_and_activate(pci, nav.get_current_highlight());
            }
          }
        }
        break;
      case DVDNAV_BLOCK_OK:
      case DVDNAV_NOP:
      case DVDNAV_SPU_CLUT_CHANGE:
      case DVDNAV_SPU_STREAM_CHANGE:
      case DVDNAV_AUDIO_STREAM_CHANGE:
      case DVDNAV_HIGHLIGHT:
      case DVDNAV_VTS_CHANGE:
      case DVDNAV_HOP_CHANNEL:
      case DVDNAV_STOP:
        break;
      }
    }
  }
  std::map<uint8_t, uint8_t> streamOutputMap;
  void LoadNextBlock() {
    if (streamDone) return;
    while (true) {
      auto [block, type] = nav.get_next_block(buffer);
      switch(type) {
      case DVDNAV_BLOCK_OK:
      {
        buffer_reader = Bini::reader(buffer);
        // In theory this contains timestamp info etc. In practice, it's used to confuse computers. We skip.
        buffer_reader.skip(4);
        uint64_t in = buffer_reader.readSizedbe(6);
        timestamp = 1000 * (300 * (((in & 0x380000000000) >> 13) | ((in & 0x03fff8000000) >> 12) | ((in & 0x3fff800) >> 11)) + ((in & 0x3FE) >> 1)) / 27 + timestampOffset;
        buffer_reader.skip(3);
        
        uint8_t stuffing = buffer_reader.read8() & 0x7;
        buffer_reader.skip(stuffing);
        return;
      }
      case DVDNAV_STILL_FRAME:
        nav.still_skip();
        break;
      case DVDNAV_WAIT:
        nav.wait_skip();
        break;
      case DVDNAV_VTS_CHANGE:
        return;
      case DVDNAV_CELL_CHANGE:
      {
        auto [pos, len] = nav.get_position();
        if (len < 1000000) 
          streamDone = true;
      }
        break;
      case DVDNAV_NAV_PACKET:
      {
        streamOutputMap.clear();
        pgc_t pgc = nav.get_current_pgc();
        for (size_t n = 0; n < audiostreams.size(); n++) {
          streamOutputMap[0x80 + ((pgc.audio_control[n] >> 8) & 0x7)] = 0x80 + n;
        }
        for (size_t n = 0; n < subtitlestreams.size(); n++) {
          streamOutputMap[0x20 + ((pgc.subp_control[n] >> 16) & 0x1F)] = 0x20 + n;
        }
        auto pci = nav.get_current_nav_pci();
        auto s = (100000ULL * pci->pci_gi.vobu_s_ptm) / 9;
        auto e = (100000ULL * pci->pci_gi.vobu_e_ptm) / 9;
        if (lastVobuEndPtm != s) {
          printf("PTS discontinuity at %zu.%zu\n", timestampOffset / 1000000000, timestampOffset % 1000000000);
          timestampOffset += (lastVobuEndPtm - s);
        }
        lastVobuEndPtm = e;
      }
        break;
      case DVDNAV_HOP_CHANNEL:
      case DVDNAV_STOP:
      case DVDNAV_NOP:
      case DVDNAV_SPU_CLUT_CHANGE:
      case DVDNAV_SPU_STREAM_CHANGE:
      case DVDNAV_AUDIO_STREAM_CHANGE:
      case DVDNAV_HIGHLIGHT:
        break;
      }
    }
  }
  void Run() {
//    size_t packetsSent = 0;
    while (true) {
      if (buffer_reader.fail() || buffer_reader.sizeleft() <= 0) {
        LoadNextBlock();
        if (buffer_reader.sizeleft() == 0) {
          channels.clear();
          return;
        }
      }
      buffer_reader.skip(3);
      uint8_t type = buffer_reader.read8();
      uint16_t length = buffer_reader.read16be();
      std::span<const uint8_t> block = buffer_reader.get(length);
      uint8_t streamid = 0;
      if (type != 0xbd && type != 0xe0) {
        continue;
      }
      // skip PES header
      Bini::reader r(block);
      uint16_t flags = r.read16be();
      uint8_t pes_hl = r.read8();
      Bini::reader pes(r.get(pes_hl));
      if (flags & 0x80) {
        uint64_t raw_ptm = pes.readSizedbe(5);
        pes_timestamp = timestampOffset + 100000 * (((raw_ptm & 0xE00000000) >> 3) | ((raw_ptm & 0xFFFE0000) >> 2) | ((raw_ptm & 0xFFFE) >> 1)) / 9;
      }

      block = r.get(r.sizeleft());
      if (block.empty()) continue;

      if (type == 0xbd) {
        streamid = block[0];
        if (streamid < 0x80) {
          block = block.subspan(1);
          streamid = streamid / 2 + 0x10;
        } else {
          block = block.subspan(4);
        }
      }
      auto it = channels.find(streamid);
      if (it != channels.end()) {
        it->second.AddToChannel(streamid, {block.begin(), block.end()}, pes_timestamp);
      }
    }
    printf("closing writers\n");
    channels.clear();
    printf("closed writers\n");
  }
};

std::vector<std::shared_ptr<MuxInStream>> OpenDvd(Graph& g, std::string device) {
  auto p = std::make_shared<DvdMuxInStream>(device);
  g.Add(p);
  return {p};
}

}

void Velox::OutputChannel::AddToChannel(uint8_t channel_id, std::vector<uint8_t> newdata, uint64_t timestamp) {
  if (channel_id == 0x0) {
    buffer.insert(buffer.end(), newdata.begin(), newdata.end());
    static const uint8_t pic_start[4] = { 0x00, 0x00, 0x01, 0x00 };
    static const uint8_t seq_header[4] = { 0x00, 0x00, 0x01, 0xb3 };
    while (true) {
      uint8_t* picture_start = (uint8_t*)memmem(buffer.data(), buffer.size(), pic_start, 4);
      if (not picture_start) return;
      size_t presize = picture_start - buffer.data();
      if (presize + 10 > buffer.size()) return; 

      uint8_t* picture_start_next = (uint8_t*)memmem(picture_start + 4, buffer.size() - presize - 4, pic_start, 4);
      uint8_t* seq_header_next = (uint8_t*)memmem(picture_start + 4, buffer.size() - presize - 4, seq_header, 4);
      if (picture_start_next || seq_header_next) {
        uint8_t* touse = picture_start_next;
        if (not picture_start_next or
            (seq_header_next && picture_start_next > seq_header_next)) {
          touse = seq_header_next;
        }
        size_t size = touse - buffer.data();
        bool isKeyframe = buffer.data()[3] == 0xb3;
        framesSinceKeyframe++;
        if (isKeyframe) {
          mpegreclock += framesSinceKeyframe * 40000000;
          framesSinceKeyframe = 0;
        }
        uint16_t fid = (picture_start[4] << 2) | (picture_start[5] >> 6);
        writer.add({ std::vector(buffer.data(), buffer.data() + size), mpegreclock + 40000000 * fid, isKeyframe });
        memmove(buffer.data(), touse, buffer.size() - size);
        buffer.resize(buffer.size() - size);
      } else {
        return;
      }
    }
  } else if (channel_id < 0x40) {
    if (timestamp != last_timestamp) buffer.clear();
    buffer.insert(buffer.end(), newdata.begin(), newdata.end());
    last_timestamp = timestamp;
    Bini::reader r(buffer);
    uint16_t size = r.read16be();
    if (r.fail() || size < buffer.size()) {
      return;
    }

    writer.add(Packet{std::move(buffer), timestamp, false});
  } else {
    buffer.insert(buffer.end(), newdata.begin(), newdata.end());
    static const uint8_t ac3_header[2] = { 0x0B, 0x77 };
    while (true) {
      uint8_t* next = (uint8_t*)memmem(buffer.data() + 1, buffer.size() - 1, ac3_header, 2);
      if (not next) return;
      size_t size = (next - buffer.data());
      timestamp += 32000000;
      writer.add({ std::vector(buffer.data(), buffer.data() + size), timestamp, false });
      memmove(buffer.data(), next, buffer.size() - size);
      buffer.resize(buffer.size() - size);
    }
  }
}

