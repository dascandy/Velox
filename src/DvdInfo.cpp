#include "velox/MuxStream.hpp"
#include <dvdnavpp/dvdnavpp.hpp>
#include <bini/reader.hpp>
#include <map>
#include <set>
#include <list>
#include <dvdread/dvd_reader.h>
#include <caligo/base64.hpp>
#include "velox/DvdInfo.hpp"
#include <fcntl.h>
#include <unistd.h>

namespace Velox {

std::string DvdId(std::string device) {
  std::array<uint8_t, 16> id;
  dvd_reader_t *reader = DVDOpen(device.c_str());
  DVDDiscID(reader, id.data());
  DVDClose(reader);
  return Caligo::base64(id, false, true);
}

static std::string ConvertDvdAspect(int aspect) {
  switch(aspect) {
  case 0: return "4:3";
  case 3: return "16:9";
  default: return "unknown";
  }
}

static int dvd_read_name(std::string& name, std::string& serial, std::string& alt, const std::string& device) {
  /* Because we are compiling with _FILE_OFFSET_BITS=64
   * all off_t are 64bit.
   */
  off_t off;
  ssize_t read_size = 0;
  int fd = -1;
  uint8_t data[2048];

  /* Read DVD name */
  if ((fd = open(device.c_str(), O_RDONLY)) == -1) {
    goto fail;
  }

  if ((off = lseek( fd, 32 * 2048, SEEK_SET )) == (off_t) - 1) {
    goto fail;
  }

  if( off != ( 32 * 2048 ) ) {
    goto fail;
  }

  if ((read_size = read( fd, data, 2048 )) == -1) {
    goto fail;
  }

  close(fd);
  fd = -1;
  if (read_size != 2048) {
    goto fail;
  }

  for(size_t i=25; i < 73; i++ ) {
    if(data[i] == 0) break;
    if((data[i] > 32) && (data[i] < 127)) {
      name.push_back(data[i]);
    } else {
      name.push_back(' ');
    }
  }
  for(size_t i=73; i < 89; i++ ) {
    if(data[i] == 0) break;
    if((data[i] > 32) && (data[i] < 127)) {
      serial.push_back(data[i]);
    } else {
      serial.push_back(' ');
    }
  }
  for(size_t i=89; i < 128; i++ ) {
    if(data[i] == 0) break;
    if((data[i] > 32) && (data[i] < 127)) {
      alt.push_back(data[i]);
    } else {
      alt.push_back(' ');
    }
  }
  return 1;

fail:
  if (fd >= 0)
    close(fd);

  return 0;
}

enum class TitleUsage {
  Unused,
  Preroll,
  MainFeature,
  Postroll,
  Offtrack,
};

std::string to_string(TitleUsage usage) {
  switch(usage) {
  case TitleUsage::Unused:
    return "unused";
  case TitleUsage::Preroll:
    return "preroll";
  case TitleUsage::MainFeature:
    return "feature";
  case TitleUsage::Postroll:
    return "postroll";
  case TitleUsage::Offtrack:
    return "offtrack";
  }
}

std::map<int32_t, TitleUsage> GetTitleUsages(dvdnav& nav) {
  std::vector<dvdnav_t*> backlog;
  // Trace down main path first
  TitleUsage current = TitleUsage::Preroll;
  std::map<int32_t, TitleUsage> usages;
  std::set<dvdnav::current_position> seen;
  int32_t current_title = 0;
  bool main_loop_done = false;
  std::array<uint8_t, 2048> buffer;
  size_t skipButtons = 10;
  while (not main_loop_done) {
    auto [block, type] = nav.get_next_block(buffer);
    switch(type) {
    case DVDNAV_STILL_FRAME:
      nav.still_skip();
      break;
    case DVDNAV_WAIT:
      nav.wait_skip();
      break;
    case DVDNAV_CELL_CHANGE:
      try {
        auto pos = nav.get_current_position();
        if (current_title != pos.title) {
          current_title = pos.title;
          if (current == TitleUsage::MainFeature) {
            current = TitleUsage::Postroll;
          } else if (pos.len > 1000000 && current == TitleUsage::Preroll) {
            current = TitleUsage::MainFeature;
          }
          if (pos.title != 0) {
            printf("found %u %u %u %u %u\n", pos.title, pos.pgcn, pos.pgn, pos.pos, pos.len);
            if (seen.contains(pos)) {
              current = TitleUsage::Offtrack;
              main_loop_done = true;
            } else {
              seen.insert(pos);
            }
            if (usages[pos.title] == TitleUsage::Unused) {
              printf("Marking title %u as %d\n", pos.title, (int)current);
              usages[pos.title] = current;
            }
            if (pos.len > 100 && 
                pos.len - pos.pos > 10000) {
              nav.sector_search(50, SEEK_END);
            }
          }
        }
      } catch (...) {}
      break;
    case DVDNAV_NAV_PACKET:
      {
        pci_t* pci = nav.get_current_nav_pci();
        [[maybe_unused]] dsi_t* dsi = nav.get_current_nav_dsi();
        if (pci->hli.hl_gi.btn_ns > 0) {
          if (skipButtons > 0) {
            skipButtons--;
          } else {
            // Some corrupt DVDs provide a menu before having a valid VM to inquire
            try {
              auto pos = nav.get_current_position();
              seen.insert(pos);
              printf("found %u %u %u %u %u choice of %u options; picking %u\n", pos.title, pos.pgcn, pos.pgn, pos.pos, pos.len, pci->hli.hl_gi.btn_ns, nav.get_current_highlight());
              for (int n = 1; n <= pci->hli.hl_gi.btn_ns; n++) {
                dvdnav_highlight_area_t area;
                dvdnav_get_highlight_area(pci, n, 0, &area);
                printf("touch area %d/0 from %d/%d to %d/%d size %d\n", n, area.sx, area.sy, area.ex, area.ey, (area.ey - area.sy) * (area.ex - area.sx));
                dvdnav_get_highlight_area(pci, n, 1, &area);
                printf("touch area %d/1 from %d/%d to %d/%d size %d\n", n, area.sx, area.sy, area.ex, area.ey, (area.ey - area.sy) * (area.ex - area.sx));
                if (n == nav.get_current_highlight()) continue;
                dvdnav_t* cnav;
                try {
                  dvdnav_dup(&cnav, nav.nav);
                  dvdnav_button_select_and_activate(cnav, pci, n);
                  backlog.push_back(cnav);
                  printf("queueing %p for option %d\n", (void*)cnav, n);
                } catch (...) {
                  dvdnav_free_dup(cnav);
                }
              }
              if (nav.get_current_highlight() <= pci->hli.hl_gi.btn_ns) {
                nav.button_select_and_activate(pci, nav.get_current_highlight());
              } else {
                printf("errr\n");
              }
            }
            catch (...) {
            }
          }
        }
      }
      break;
    case DVDNAV_BLOCK_OK:
    case DVDNAV_NOP:
    case DVDNAV_SPU_CLUT_CHANGE:
    case DVDNAV_SPU_STREAM_CHANGE:
    case DVDNAV_AUDIO_STREAM_CHANGE:
    case DVDNAV_HIGHLIGHT:
    case DVDNAV_VTS_CHANGE:
    case DVDNAV_HOP_CHANNEL:
    case DVDNAV_STOP:
      break;
    }
  }
  while (not backlog.empty()) {
    auto cnav = backlog.back();
    backlog.pop_back();
    printf("looking at %p\n", (void*)cnav);
    bool chunk_done = false;
    while (not chunk_done) {
      int32_t type;
      int32_t len = buffer.size();
      dvdnav_get_next_block(cnav, buffer.data(), &type, &len);
      switch(type) {
      case DVDNAV_STILL_FRAME:
        dvdnav_still_skip(cnav);
        break;
      case DVDNAV_WAIT:
        dvdnav_wait_skip(cnav);
        break;
      case DVDNAV_CELL_CHANGE:
        try {
          dvdnav::current_position pos;
          dvdnav_current_title_program(cnav, &pos.title, &pos.pgcn, &pos.pgn);
          dvdnav_get_position_in_title(cnav, &pos.pos, &pos.len);
          if (seen.contains(pos)) {
            printf("already saw %u %u %u %u %u\n", pos.title, pos.pgcn, pos.pgn, pos.pos, pos.len);
            chunk_done = true;
          } else {
            seen.insert(pos);
            if (current_title != pos.title) {
              current_title = pos.title;
              if (pos.title != 0) {
                if (usages[pos.title] == TitleUsage::Unused) {
                  printf("Marking title %u as %d\n", pos.title, 4);
                  usages[pos.title] = TitleUsage::Offtrack;
                }
              }
            }
            if (pos.len > 100 && 
                pos.len - pos.pos > 10000) {
              nav.sector_search(50, SEEK_END);
            }
          }
        } catch (...) {}
        break;
      case DVDNAV_NAV_PACKET:
        {
          pci_t* pci = dvdnav_get_current_nav_pci(cnav);
          if (pci->hli.hl_gi.btn_ns > 0) {
            auto pos = nav.get_current_position();
            seen.insert(pos);
            chunk_done = true;
            printf("found %u %u %u %u %u choice of %u options\n", pos.title, pos.pgcn, pos.pgn, pos.pos, pos.len, pci->hli.hl_gi.btn_ns);
            for (int n = 1; n <= pci->hli.hl_gi.btn_ns; n++) {
              dvdnav_t* copy;
              dvdnav_dup(&copy, cnav);
              dvdnav_button_select_and_activate(cnav, pci, n);
              backlog.push_back(copy);
              printf("queueing %p for option %d\n", (void*)copy, n);
              dvdnav_highlight_area_t area;
              dvdnav_get_highlight_area(pci, n, 0, &area);
              printf("touch area %d/0 from %d/%d to %d/%d size %d\n", n, area.sx, area.sy, area.ex, area.ey, (area.ey - area.sy) * (area.ex - area.sx));
              dvdnav_get_highlight_area(pci, n, 1, &area);
              printf("touch area %d/1 from %d/%d to %d/%d size %d\n", n, area.sx, area.sy, area.ex, area.ey, (area.ey - area.sy) * (area.ex - area.sx));
            }
          }
        }
        break;
      case DVDNAV_BLOCK_OK:
      case DVDNAV_NOP:
      case DVDNAV_SPU_CLUT_CHANGE:
      case DVDNAV_SPU_STREAM_CHANGE:
      case DVDNAV_AUDIO_STREAM_CHANGE:
      case DVDNAV_HIGHLIGHT:
      case DVDNAV_VTS_CHANGE:
      case DVDNAV_HOP_CHANNEL:
      case DVDNAV_STOP:
        break;
      }
    }
    dvdnav_free_dup(cnav);
  }
  return usages;
}

DvdDisc DiscData(std::string device) {
  DvdDisc disc;
  {
    (void)dvd_read_name;
    dvd_read_name(disc.disctitle, disc.serial, disc.disctitle2, device);

    dvdnav nav(device);
    nav.set_readahead_flag(false);
    nav.set_pgc_positioning_flag(true);
    nav.menu_language("en");
    nav.audio_language("en");
    nav.spu_language("en");

    auto usages = GetTitleUsages(nav);

    for (int32_t n = 1; n <= nav.number_of_titles(); n++) {
      nav.play(n);
      std::array<uint8_t, 2048> buffer;
      nav.get_next_block(buffer);
      disc.titles.emplace_back();
      Title& title = disc.titles.back();
      title.id = n;
      auto [chapters, total] = nav.describe_title_chapters(n);
      title.duration = total / 90000.0;
      chapters.insert(chapters.begin(), 0);
      for (size_t n = 0; n < chapters.size() - 1; n++) {
        title.parts.push_back({chapters[n] / 90000.0, (chapters[n+1] - chapters[n]) / 90000.0});
      }
      title.usage = to_string(usages[n]);
      
      title.aspect_ratio = ConvertDvdAspect(nav.get_video_aspect());
      auto [width, height] = nav.get_video_resolution();
      title.resolution = std::to_string(width) + "x" + std::to_string(height);

      for (size_t n = 0; n < 32; n++) {
        int8_t aud_stream = nav.get_audio_logical_stream(n);
        if (aud_stream == -1) break;
        title.tracks.push_back({"audio", aud_stream, nav.audio_stream_to_lang(n), nav.audio_stream_format(n), (uint8_t)nav.audio_stream_channels(n)});
      }
      for (size_t n = 0; n < 32; n++) {
        int8_t spu_stream = nav.get_spu_logical_stream(n);
        if (spu_stream == -1) break;
        title.tracks.push_back({"subtitle", spu_stream, nav.spu_stream_to_lang(n), "vobsub", 0});
      }
    }
  }
  disc.dvdid = DvdId(device);
  return disc;
}

}


