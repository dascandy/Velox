#include "velox/MuxStream.hpp"
#include <bini/reader.hpp>
#include <list>
#include <fcntl.h>
#include <sys/types.h>
#include <linux/cdrom.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <caligo/sha1.hpp>
#include <caligo/base64.hpp>
#include <velox/cddb.hpp>
#include <thread>

namespace Velox {

struct CdAudioMuxInStream : public GraphNode {
public:
  int fd;
  uint32_t finalsectorno = 0;
  std::thread thr; 
  std::vector<uint64_t> splitpoints;

  std::vector<std::shared_ptr<MuxInStream>> streams;
  CdAudioMuxInStream(std::string device, bool forceMerge)
  {
    // O_NONBLOCK indicates that we are reading audio, not data. Don't ask.
    fd = open(device.c_str(), O_RDONLY | O_NONBLOCK);

    try {
      cdrom_tochdr hdr;
      int rv = ioctl(fd, CDROMREADTOCHDR, &hdr);
      if (rv == -1) throw std::runtime_error("Not CD");
      char discid_in[805];
      memset(discid_in, '0', 804);
      discid_in[804] = 0;

      std::vector<uint64_t> timestamps;
      std::vector<uint8_t> audiotracks;
      uint8_t finaltrack = hdr.cdth_trk1;
      sprintf(discid_in, "%02X%02X", hdr.cdth_trk0, finaltrack);

      // It's a kind of magic. 0xAA gets you the lead-out start.
      cdrom_tocentry entry{};
      entry.cdte_track = 0xAA;
      entry.cdte_format = CDROM_LBA;
      rv = ioctl(fd, CDROMREADTOCENTRY, &entry);
      if (rv == -1) throw std::runtime_error("Not CD");
      finalsectorno = entry.cdte_addr.lba;
      sprintf(discid_in + 4, "%08X", finalsectorno + 150);
      std::vector<int> noise;
      for (size_t n = hdr.cdth_trk0; n <= hdr.cdth_trk1; n++) {
        cdrom_tocentry entry{};
        entry.cdte_track = n;
        entry.cdte_format = CDROM_LBA;
        rv = ioctl(fd, CDROMREADTOCENTRY, &entry);
        if (rv == -1) throw std::runtime_error("Not CD");
        if ((entry.cdte_ctrl & CDROM_DATA_TRACK) == 0) {
          uint64_t time = (1000000000ULL * entry.cdte_addr.lba) / 75;
          timestamps.push_back(time);

          if (n != hdr.cdth_trk0) {
            // Check to see if the volume in the last 2/3rds of a second before the new track is "silent"
            // we sum 58800 samples, so any value below about 588000 is "silent".
            int sum = 0;
            for (size_t i = 0; i < 50; i++) {
              cdrom_read_audio ra;
              ra.addr.lba = entry.cdte_addr.lba - 50 + i;
              ra.addr_format = CDROM_LBA;
              ra.nframes = 1;
              std::array<uint8_t,2352> buffer; 
              ra.buf = buffer.data();
     
              ioctl(fd, CDROMREADAUDIO, &ra);
              Bini::reader r(buffer);
              while (r.sizeleft()) {
                sum += std::abs(int16_t(r.read16le()));
              }
            }
            noise.push_back(sum);
          }

          audiotracks.push_back(n);
          sprintf(discid_in + strlen(discid_in), "%08X", entry.cdte_addr.lba + 150);
        } else {
          finaltrack = n - 1;
          finalsectorno = entry.cdte_addr.lba - 11400;
          char newbuf[13];
          sprintf(newbuf, "%02X%02X%08X", hdr.cdth_trk0, finaltrack, finalsectorno + 150);
          memcpy(discid_in, newbuf, 12);
          break;
        }
      }

      timestamps.push_back((1000000000ULL * finalsectorno) / 75);
      if (audiotracks.empty()) {
        throw std::runtime_error("Disc has no audio tracks");
      }
      bool allConnected = true;
      for (size_t n = 0; n < timestamps.size() - 1; n++) {
        bool isConnected = true;
        if (n < timestamps.size() - 2 && noise[n] < 588000) {
          isConnected = false;
          allConnected = false;
        }
        streams.push_back(std::make_shared<MuxInStream>());
        streams.back()->audiostreams.push_back(std::make_shared<AudioStreamDescription>(AudioCodec::LPCM, 44100, std::vector<AudioChannelPosition>{ AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }));
        streams.back()->audiostreams.back()->bitdepth = 16;
        streams.back()->chapters.push_back({0, "Track " + std::to_string(n+1), timestamps[n+1] - timestamps[n], isConnected});
        splitpoints.push_back(timestamps[n]);
      }
      splitpoints.push_back(timestamps.back());
      rv = ioctl(fd, CDROM_SELECT_SPEED, 4);
      if (rv == -1) throw std::runtime_error("Not CD");
      while (strlen(discid_in) < 804) strcat(discid_in, "00000000");
      std::string discid = Caligo::base64(Caligo::SHA1(std::span<const uint8_t>((const uint8_t*)discid_in, (const uint8_t*)discid_in + 804)).data(), false, true, Caligo::DotUnderscore);
      discid.push_back('-');
      try {
        CddbLookupResult result = CddbLookup(discid);

        if (result.tracks.size() != streams.size()) {
          throw std::runtime_error("Unusable result, got " + std::to_string(result.tracks.size()) + " result for " + std::to_string(streams.size()) + " track disc");
        }
        size_t n = 0;
        for (auto& t : result.tracks) {
          auto& stream = *streams[n++];
          stream.tags.push_back({ "DISCID", discid });
          stream.tags.push_back({ "MBID", result.mbid });
          stream.tags.push_back({ "ARTIST", result.artist });
          stream.tags.push_back({ "TITLE", result.title });
          stream.tags.push_back({ "DATE_RELEASED", result.releaseDate });
          stream.tags.push_back({ "TOTAL_PARTS", std::to_string(result.tracks.size()) });
          stream.title = result.artist + " - " + result.title;
          auto& chap = stream.chapters[0];
          chap.name = t.artist + " - " + t.title;
          chap.tags.push_back({ "ARTIST", t.artist });
          chap.tags.push_back({ "TITLE", t.title });
          chap.tags.push_back({ "PART_NUMBER", std::to_string(n) });
        }
      } catch (std::exception& e) {
        printf("Error: %s\n", e.what());
        printf("Could not lookup discid %s\n", discid.c_str());
        std::string submit_url = "https://musicbrainz.org/bare/cdlookup.html?id=" + discid + 
                                 "&tracks=" + std::to_string(finaltrack - hdr.cdth_trk0 + 1) + 
                                 "&toc=" + std::to_string(hdr.cdth_trk0) + "+" + std::to_string(finaltrack) + 
                                 "+" + std::to_string(finalsectorno + 150);

        for (size_t n = hdr.cdth_trk0; n <= hdr.cdth_trk1; n++) {
          cdrom_tocentry entry{};
          entry.cdte_track = n;
          entry.cdte_format = CDROM_LBA;
          rv = ioctl(fd, CDROMREADTOCENTRY, &entry);
          if ((entry.cdte_ctrl & CDROM_DATA_TRACK) == 0) {
            submit_url += "+" + std::to_string(entry.cdte_addr.lba + 150);
          }
        }

        printf("Disc ID submit link: %s\n", submit_url.c_str());
        printf("Track info:\n");
        for (auto& s : streams) {
          auto& chapter = s->chapters[0];
          int duration = chapter.duration / 1000000000;
          printf("  %s. ?? - ?? (%d:%02d)\n", chapter.name.c_str(), duration / 60, duration % 60);
        }
        throw e;
      }
      if (allConnected || forceMerge) {
        std::shared_ptr<MuxInStream> newStream = std::make_shared<MuxInStream>();
        newStream->audiostreams.push_back(std::make_shared<AudioStreamDescription>(AudioCodec::LPCM, 44100, std::vector<AudioChannelPosition>{ AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }));
        newStream->audiostreams.back()->bitdepth = 16;

        size_t timestampSoFar = 0;
        for (auto& s : streams) {
          newStream->chapters.push_back(s->chapters[0]);
          newStream->chapters.back().timestamp = timestampSoFar;
          timestampSoFar += newStream->chapters.back().duration;
        }
        streams = { std::move(newStream) };
        splitpoints[1] = splitpoints.back();
        splitpoints.resize(2);
      }
    } catch (std::exception& e) {
      close(fd);
      throw;
    }
    thr = std::thread([this]{ Run(); });
  }
  ~CdAudioMuxInStream() override {
    ioctl(fd, CDROM_SELECT_SPEED, 0);
    close(fd);
    thr.join();
  }
  void RunUntil(Origo::channel_writer<Packet> out, size_t startSectorNo, size_t endSectorNo) {
    for (size_t sectorno = startSectorNo; sectorno < endSectorNo; sectorno++) {
      Packet p;
      p.timestamp = (1000000000ULL * (sectorno - startSectorNo)) / 75;
      p.data.resize(2352);

      cdrom_read_audio ra;
      ra.addr.lba = sectorno;
      ra.addr_format = CDROM_LBA;
      ra.nframes = 1;
      ra.buf = p.data.data();

      ioctl(fd, CDROMREADAUDIO, &ra);
      out.add(std::move(p));
    }
  }
  void Run() {
    size_t n = 0;
    for (auto& s : streams) {
      size_t startSector = (75 * splitpoints[n] + 500) / 1000000000ULL,
             endSector = (75 * splitpoints[n+1] + 500) / 1000000000ULL;
      RunUntil(s->audiostreams.back()->packets, startSector, endSector);
      n++;
    }
  }
};

std::vector<std::shared_ptr<MuxInStream>> OpenCdAudio(Graph& g, std::string device, bool forceMerge) {
  auto p = std::make_shared<CdAudioMuxInStream>(device, forceMerge);
  if (p)
    g.Add(p);
  return p->streams;
}

}


