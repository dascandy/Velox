#include "velox/MuxStream.hpp"
#include <bluraypp/bluraypp.hpp>
#include <bini/reader.hpp>
#include <libbluray/meta_data.h>
#include <map>
#include <list>
#include <bitset>
#include "velox/Mpeg2TS.hpp"
#include "velox/Mpeg2PES.hpp"
#include "ac3.hpp"
#include "lpcm_bd.hpp"
#include "dts.hpp"
#include <future>

namespace Velox {

std::shared_ptr<VideoStreamDescription> BlurayToVideo(bd_stream_info& s) {
  VideoCodec codec;
  switch(s.coding_type) {
    case BLURAY_STREAM_TYPE_VIDEO_MPEG1: codec = VideoCodec::Mpeg1; break;
    case BLURAY_STREAM_TYPE_VIDEO_MPEG2: codec = VideoCodec::Mpeg2; break;
    case BLURAY_STREAM_TYPE_VIDEO_VC1: codec = VideoCodec::VC1; break;
    case BLURAY_STREAM_TYPE_VIDEO_H264: codec = VideoCodec::H264; break;
    case BLURAY_STREAM_TYPE_VIDEO_HEVC: codec = VideoCodec::H265; break;
    default: codec = VideoCodec::Unknown; break;
  }
  float framerate;
  switch(s.rate) {
    case BLURAY_VIDEO_RATE_24000_1001: framerate = 23.976; break;
    case BLURAY_VIDEO_RATE_24: framerate = 24; break;
    case BLURAY_VIDEO_RATE_25: framerate = 25; break;
    case BLURAY_VIDEO_RATE_30000_1001: framerate = 29.97; break;
    case BLURAY_VIDEO_RATE_50: framerate = 50; break;
    case BLURAY_VIDEO_RATE_60000_1001: framerate = 59.94; break;
    default: framerate = 0; break;
  }
  std::pair<uint32_t, uint32_t> imagesize;
  bool interlaced;
  switch(s.format) {
    case BLURAY_VIDEO_FORMAT_480I: imagesize = { 720, 480 }; interlaced = true; break;
    case BLURAY_VIDEO_FORMAT_480P: imagesize = { 720, 480 }; interlaced = false; break;
    case BLURAY_VIDEO_FORMAT_576I: imagesize = { 720, 576 }; interlaced = true; break;
    case BLURAY_VIDEO_FORMAT_576P: imagesize = { 720, 576 }; interlaced = false; break;
    case BLURAY_VIDEO_FORMAT_720P: imagesize = { 1280, 720 }; interlaced = false; break;
    case BLURAY_VIDEO_FORMAT_1080I: imagesize = { 1920, 1080 }; interlaced = true; break;
    case BLURAY_VIDEO_FORMAT_1080P: imagesize = { 1920, 1080 }; interlaced = false; break;
    case BLURAY_VIDEO_FORMAT_2160P: imagesize = { 3840, 2160 }; interlaced = false; break;
  }
  std::pair<uint32_t, uint32_t> aspectratio;
  switch(s.aspect) {
    case BLURAY_ASPECT_RATIO_4_3: aspectratio = { 4, 3 }; break;
    case BLURAY_ASPECT_RATIO_16_9: aspectratio = { 16, 9 }; break;
  }
  std::shared_ptr<VideoStreamDescription> desc = std::make_shared<VideoStreamDescription>(codec, framerate, imagesize, aspectratio);
  desc->interlaced = interlaced;
  return desc;
}

std::shared_ptr<AudioStreamDescription> BlurayToAudio(bd_stream_info& s) {
  bool commentaryTrack = false;
  AudioCodec codec;
  switch(s.coding_type) {
    case BLURAY_STREAM_TYPE_AUDIO_MPEG1: codec = AudioCodec::MPEG1; break;
    case BLURAY_STREAM_TYPE_AUDIO_MPEG2: codec = AudioCodec::MPEG2; break;
    case BLURAY_STREAM_TYPE_AUDIO_LPCM: codec = AudioCodec::LPCM; break;
    case BLURAY_STREAM_TYPE_AUDIO_AC3: codec = AudioCodec::AC3; break;
    case BLURAY_STREAM_TYPE_AUDIO_DTS: codec = AudioCodec::DTS; break;
    case BLURAY_STREAM_TYPE_AUDIO_TRUHD: codec = AudioCodec::TRUHD; break;
    case BLURAY_STREAM_TYPE_AUDIO_AC3PLUS: codec = AudioCodec::AC3PLUS; break;
    case BLURAY_STREAM_TYPE_AUDIO_AC3PLUS_SECONDARY: codec = AudioCodec::AC3PLUS; commentaryTrack = true; break;
    case BLURAY_STREAM_TYPE_AUDIO_DTSHD: codec = AudioCodec::DTSHD; break;
    case BLURAY_STREAM_TYPE_AUDIO_DTSHD_MASTER: codec = AudioCodec::DTSHD; break;
    case BLURAY_STREAM_TYPE_AUDIO_DTSHD_SECONDARY: codec = AudioCodec::DTSHD; commentaryTrack = true; break;
  }
  std::vector<AudioChannelPosition> channels;
  switch(s.format) {
    case BLURAY_AUDIO_FORMAT_MONO: channels = { AudioChannelPosition::Center }; break;
    case BLURAY_AUDIO_FORMAT_STEREO: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }; break;
    // These two cases are handled below by snooping the first packet & reading it from that.
    case BLURAY_AUDIO_FORMAT_MULTI_CHAN: channels = {}; break;
    case BLURAY_AUDIO_FORMAT_COMBO: channels = {}; break;
    default: channels = {}; break;
  }
  uint32_t samplingFrequency;
  switch(s.rate) {
    case BLURAY_AUDIO_RATE_48: samplingFrequency = 48000; break;
    case BLURAY_AUDIO_RATE_96: samplingFrequency = 96000; break;
    case BLURAY_AUDIO_RATE_192: samplingFrequency = 192000; break;
    case BLURAY_AUDIO_RATE_192_COMBO: samplingFrequency = 192000; break;
    case BLURAY_AUDIO_RATE_96_COMBO: samplingFrequency = 96000; break;
  }

  std::shared_ptr<AudioStreamDescription> desc = std::make_shared<AudioStreamDescription>(codec, samplingFrequency, std::move(channels));
  desc->language = std::string{ (char)s.lang[0], (char)s.lang[1], (char)s.lang[2] };
  desc->commentaryTrack = commentaryTrack;
  return desc;
}

std::shared_ptr<SubtitleStreamDescription> BlurayToSubtitle(bd_stream_info& s) {
  SubtitleCodec format;
  switch(s.coding_type) {
    case BLURAY_STREAM_TYPE_SUB_PG: format = SubtitleCodec::PG; break;
    case BLURAY_STREAM_TYPE_SUB_IG: format = SubtitleCodec::IG; break;
    case BLURAY_STREAM_TYPE_SUB_TEXT: format = SubtitleCodec::BlurayText; break;
  }

  std::string language{ (char)s.lang[0], (char)s.lang[1], (char)s.lang[2] };
  std::shared_ptr<SubtitleStreamDescription> desc = std::make_shared<SubtitleStreamDescription>(format, language);

  switch(s.char_code) {
  case BLURAY_TEXT_CHAR_CODE_UTF8: desc->encoding = TextEncoding::Utf8; break;
  case BLURAY_TEXT_CHAR_CODE_UTF16BE: desc->encoding = TextEncoding::Utf16be; break;
  case BLURAY_TEXT_CHAR_CODE_SHIFT_JIS: desc->encoding = TextEncoding::ShiftJis; break;
  case BLURAY_TEXT_CHAR_CODE_EUC_KR: desc->encoding = TextEncoding::EucKr; break;
  case BLURAY_TEXT_CHAR_CODE_GB18030_20001: desc->encoding = TextEncoding::Gb18030; break;
  case BLURAY_TEXT_CHAR_CODE_CN_GB: desc->encoding = TextEncoding::CnGb; break;
  case BLURAY_TEXT_CHAR_CODE_BIG5: desc->encoding = TextEncoding::Big5; break;
  default: desc->encoding = TextEncoding::NotApplicable; break;
  }
  return desc;
}

struct BlurayMuxInStream : public MuxInStream {
public:
  bluray nav;
  std::map<uint16_t, Mpeg2PES> pes;
  Mpeg2TS ts;
  std::atomic<bool> cancel{false};
  size_t channelsSnooping = 0;
  std::promise<void> doneSnooping;
  std::thread thr;
 
  BlurayMuxInStream(std::string device)
  : nav(device)
  , ts([&](uint16_t id, std::span<const uint8_t> sp) {
    auto it = pes.find(id);
    // Only send out streams that have content, not the metadata ones
    if (it == pes.end()) return;
    it->second.Add(sp);
    })
  {
    [[maybe_unused]] auto discinfo = nav.get_disc_info();
    [[maybe_unused]] auto titles = nav.get_titles();
    uint32_t titleId = nav.get_main_title();
    auto info = nav.get_title_info(titleId);
    for (auto& c : info.chapters) {
      chapters.push_back({ c.start, std::to_string(c.idx + 1), c.duration });
    }
    if (not info.clips.empty()) {
      auto& c = info.clips[0];
      for (auto& stream : c.video_streams) {
        videostreams.push_back(BlurayToVideo(stream));
        pes.emplace(stream.pid, Mpeg2PES{ videostreams.back()->packets });
      }
      for (auto& stream : c.audio_streams) {
        audiostreams.push_back(BlurayToAudio(stream));
        if (audiostreams.back()->channels.empty()) {
          channelsSnooping++;
        }
        pes.emplace(stream.pid, Mpeg2PES{ audiostreams.back()->packets });
      }
      for (auto& stream : c.pg_streams) {
        subtitlestreams.push_back(BlurayToSubtitle(stream));
        pes.emplace(stream.pid, Mpeg2PES{ subtitlestreams.back()->packets });
      }
//      for (auto& stream : c.ig_streams) subtitlestreams.push_back(BlurayToSubtitle(stream));
    }

    auto* meta = nav.get_meta();
    if (meta) {
      title = meta->di_name;
    }
    nav.select_title(titleId);

    thr = std::thread([this]{ Run(); });
    // Block until we are done snooping
    doneSnooping.get_future().get();
  }
  void Run() {
    while (true) {
      auto buffer = nav.read(12288);
      if (buffer.empty()) {
        pes.clear();
        if (channelsSnooping > 0) {
          // Emergency exit; a channel has zero data and the disc is done
          doneSnooping.set_value();
        }
        return;
      } else {
        ts.Add(buffer);
      }
      if (channelsSnooping > 0) {
        for (auto& stream : audiostreams) {
          if (stream->channels.empty()) {
            channelsSnooping++;
          }
        }
        if (channelsSnooping == 0) {
          doneSnooping.set_value();
        }
      }
    }
  }
};

std::vector<std::shared_ptr<MuxInStream>> OpenBluray(Graph& g, std::string device) {
  auto p = std::make_shared<BlurayMuxInStream>(device);
  g.Add(p);
  return {p};
}

}


