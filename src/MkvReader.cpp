#include "velox/MuxStream.hpp"
#include <bini/reader.hpp>
#include <list>
#include <map>
#include <fcntl.h>
#include <sys/types.h>
#include <linux/cdrom.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <caligo/sha1.hpp>
#include <caligo/base64.hpp>
#include <velox/cddb.hpp>
#include <thread>
#include <manto/mapping.hpp>
#include "EbmlReader.hpp"

namespace Velox {

VideoCodec VideoCodecFromMkv(std::string_view c) {
  static std::map<std::string_view, VideoCodec> fromMkv = {
    { "V_AV1", VideoCodec::AV1 },
    { "V_MS/VFW/FOURCC", VideoCodec::VC1 },
    { "V_MPEG1", VideoCodec::Mpeg1 },
    { "V_MPEG2", VideoCodec::Mpeg2 },
    { "V_MPEG4/ISO/AVC", VideoCodec::H264 },
    { "V_MPEGH/ISO/HEVC", VideoCodec::H265 },
    { "V_VP8", VideoCodec::VP8 },
    { "V_VP9", VideoCodec::VP9 },
    { "V_THEORA", VideoCodec::Theora },
  };
  auto it = fromMkv.find(c);
  if (it != fromMkv.end()) return it->second;
  return VideoCodec::Unknown;
}

AudioCodec AudioCodecFromMkv(std::string_view c) {
  static std::map<std::string_view, AudioCodec> fromMkv = {
    { "A_MPEG/L3", AudioCodec::MPEG2 },
    { "A_AC3", AudioCodec::AC3 },
    { "A_TRUEHD", AudioCodec::TRUHD },
    { "A_EAC3", AudioCodec::AC3PLUS },
    { "A_PCM/INT/LIT", AudioCodec::LPCM },
    { "A_DTS", AudioCodec::DTS },
    { "A_OPUS", AudioCodec::Opus },
  };
  auto it = fromMkv.find(c);
  if (it != fromMkv.end()) return it->second;
  return AudioCodec::Unknown;
}

SubtitleCodec SubtitleCodecFromMkv(std::string_view c) {
  static std::map<std::string_view, SubtitleCodec> fromMkv = {
    { "S_VOBSUB", SubtitleCodec::Vobsub },
    { "S_HDMV/PGS", SubtitleCodec::PG },
    { "S_HDMV/PGS", SubtitleCodec::IG },
    { "S_HDMV/TEXTST", SubtitleCodec::BlurayText },
  };
  auto it = fromMkv.find(c);
  if (it != fromMkv.end()) return it->second;
  return SubtitleCodec::Unknown;
}

struct MatroskaMuxInStream : public MuxInStream {
public:
  int fd;
  std::thread thr; 
  uint32_t totalsamples;
  uint32_t samplerate;
  uint16_t channels;
  uint16_t bytespersample;
  mapping mapped;
  std::vector<EbmlReader> clusters;
  std::map<size_t, Origo::channel_writer<Packet>> writers;

  void ParseInfo(EbmlReader r) {
    double durationF = 0.0;
    uint64_t timestampScale = 1000000;
    for (auto [id, data] : r.chunks()) {
      switch(id) {
      case mkv_ids::Title: title = r.GetString(); break;
      case mkv_ids::Duration: duration = r.GetFloat(); break;
      case mkv_ids::TimestampScale: timestampScale = r.readvint(); break;
      default: break;
      }
    }
    this->duration = (uint64_t)(durationF * timestampScale);
  }
  void ParseVideoTrack(EbmlReader r) {
    size_t tracknumber;
    VideoCodec codec;
    float framerate;
    std::pair<uint32_t, uint32_t> imagesize;
    std::pair<uint32_t, uint32_t> aspectratio;
    std::vector<uint8_t> codecPrivate;
    bool interlaced = false;
    for (auto [id, data] : r.chunks()) {
      switch(id) {
        case mkv_ids::TrackNumber: tracknumber = data.GetIntValue(); break;
        case mkv_ids::CodecId: codec = VideoCodecFromMkv(data.GetString()); break;
        case mkv_ids::CodecPrivate: {
          auto sp = data.get(data.sizeleft()); 
          codecPrivate = std::vector<uint8_t>(sp.begin(), sp.end());
        }
          break;
        case mkv_ids::Video: {
          for (auto [id2, data2] : data.chunks()) {
            switch(id2) {
            case mkv_ids::DefaultDuration: framerate = 1000000000.0 / data.GetFloat(); break;
            case mkv_ids::PixelWidth: imagesize.first = data.GetIntValue(); break;
            case mkv_ids::PixelHeight: imagesize.second = data.GetIntValue(); break;
            // There is a display unit too, but we can use any of the units as aspect ratio input
            case mkv_ids::DisplayWidth: aspectratio.first = data.GetIntValue(); break;
            case mkv_ids::DisplayHeight: aspectratio.second = data.GetIntValue(); break;
            case mkv_ids::FlagInterlaced: interlaced = data.GetIntValue() == 1; break;
            default: break;
            }
          }
        }
          break;
        default: break;
      }
    }
    videostreams.push_back(std::make_shared<VideoStreamDescription>(codec, framerate, imagesize, aspectratio));
    videostreams.back()->codecPrivate = std::move(codecPrivate);
    videostreams.back()->interlaced = interlaced;
    writers.emplace(tracknumber, videostreams.back()->packets);
  }
  void ParseAudioTrack(EbmlReader r) {
    static std::map<size_t, std::vector<AudioChannelPosition>> channelMap;
    size_t tracknumber;
    AudioCodec codec;
    uint32_t samplingFrequency;
    std::vector<AudioChannelPosition> channels;
    std::string language = "und";
    uint8_t bitdepth = 0; // unknown or irrelevant by default
    bool commentaryTrack = false;
    for (auto [id, data] : r.chunks()) {
      switch(id) {
        case mkv_ids::TrackNumber: tracknumber = data.GetIntValue(); break; 
        case mkv_ids::CodecId: codec = AudioCodecFromMkv(data.GetString()); break;
        case mkv_ids::Language: language = data.GetString(); break;
        case mkv_ids::FlagCommentary: commentaryTrack = true; break;
        case mkv_ids::Audio: {
          for (auto [id2, data2] : data.chunks()) {
            switch(id2) {
            case mkv_ids::SamplingFrequency: samplingFrequency = data.GetFloat(); break;
            case mkv_ids::Channels: { // We prefer channel positions, but will just guess.
              size_t channelCount = data.GetIntValue();
              if (channelMap.contains(channelCount)) {
                channels = channelMap[channelCount];
              }
            }
              break;
            // There is a display unit too, but we can use any of the units as aspect ratio input
            case mkv_ids::BitDepth: bitdepth = data.GetIntValue(); break;
            default: break;
            }
          }
        } 
          break;
        default: break;
      }
    }
    audiostreams.push_back(std::make_shared<AudioStreamDescription>(codec, samplingFrequency, channels));
    audiostreams.back()->language = language;
    audiostreams.back()->bitdepth = bitdepth;
    audiostreams.back()->commentaryTrack = commentaryTrack;
    writers.emplace(tracknumber, audiostreams.back()->packets);
  }
  void ParseSubtitleTrack(EbmlReader r) {
    size_t tracknumber;
    SubtitleCodec codec;
    std::string language = "und";
    bool commentaryTrack = false;
    bool hardOfHearing = false;
    
    for (auto [id, data] : r.chunks()) {
      switch(id) {
        case mkv_ids::TrackNumber: tracknumber = data.GetIntValue(); break;
        case mkv_ids::CodecId: codec = SubtitleCodecFromMkv(data.GetString()); break;
        case mkv_ids::Language: language = data.GetString(); break;
        case mkv_ids::FlagCommentary: commentaryTrack = data.GetIntValue() ? true : false; break;
        case mkv_ids::FlagHearingImpaired: hardOfHearing = data.GetIntValue() ? true : false; break;
        default: break;      
      }
    }
    subtitlestreams.push_back(std::make_shared<SubtitleStreamDescription>(codec, language));
    subtitlestreams.back()->commentaryTrack = commentaryTrack;
    subtitlestreams.back()->hardOfHearing = hardOfHearing;
    writers.emplace(tracknumber, subtitlestreams.back()->packets);
  }
  void ParseTracks(EbmlReader r) {
    for (auto [id, data] : r.chunks()) {
      EbmlReader c = data;
      for (auto [id2, data2] : c.chunks()) {
        if (id2 != mkv_ids::TrackType) continue;
        uint8_t tracktype = data2.read8();
        switch(tracktype) {
          case 0x01:
            ParseVideoTrack(data);
            break;
          case 0x02:
            ParseAudioTrack(data);
            break;
          case 0x11:
            ParseSubtitleTrack(data);
            break;
        }
      }
    }
  }
  void ParseChapters(EbmlReader r) {
    for (auto [id, data] : r.chunks()) {
      if (id != mkv_ids::EditionEntry) continue;
      for (auto [id2, data2] : data.chunks()) {
        if (id2 == mkv_ids::ChapterAtom) {
          std::string name;
          uint64_t start = 0;
          uint64_t end = 0;
          for (auto [id3, data3] : data2.chunks()) {
            switch(id3) {
            case mkv_ids::ChapterDisplay: {
              for (auto [id4, data4] : data3.chunks()) {
                switch(id4) {
                case mkv_ids::ChapString: name = data4.GetString(); break;
                default: break;
                }
              }
            }
              break;
            case mkv_ids::ChapterTimeStart: start = data3.GetIntValue(); break;
            case mkv_ids::ChapterTimeEnd: end = data3.GetIntValue(); break;
            default: break;
            }
          }
          chapters.push_back({start, name});
          if (end != 0) {
            chapters.back().duration = end - start;
          }
        }
      }
    }
  }
  void ParseTags(EbmlReader r) {
    for (auto [id, data] : r.chunks()) {
      if (id != mkv_ids::Tag) continue;
      std::vector<Tag> tags;
      for (auto [id2, data2] : data.chunks()) {
        switch(id2) {
          case mkv_ids::SimpleTag:
          {
            std::string key, value;
            for (auto [id3, data3] : data2.chunks()) {
              if (id3 == mkv_ids::TagName) {
                key = data3.GetString();
              } else if (id3 == mkv_ids::TagString) {
                value = data3.GetString();
              }
            }
            tags.push_back({key, value});
          }
            break;
          default: break;
        }
      }
    }
  }
  MatroskaMuxInStream(mapping map)
  : mapped(std::move(map))
  {
    EbmlReader r(mapped.data());
    for (auto [id, data] : r.chunks()) {
      if (id != mkv_ids::Segment) continue;
      for (auto [id2, data2] : data.chunks()) {
        switch(id2) {
        case mkv_ids::Info: // basic info like duration & title
          ParseInfo(data2);
          break;
        case mkv_ids::Tracks: // Info about the streams of data
          ParseTracks(data2);
          break;
        case mkv_ids::Chapters: // chapters
          ParseChapters(data2);
          break;
        case mkv_ids::Tags: // tags
          ParseTags(data2);
          break;
        case mkv_ids::Cluster: // the actual data
          clusters.push_back(data2);
          break;
        default:
          break;
        }
      }
    }
    thr = std::thread([this]{ Run(); });
  }
  ~MatroskaMuxInStream() override {
    thr.join();
  }
  void Run() {
    for (auto& c : clusters) {
      uint64_t baseTimestamp = 0;
      for (auto [id, block] : c.chunks()) {
        switch(id) {
        case mkv_ids::SimpleBlock:
        {
          size_t channel = block.GetIntValue();
          auto it = writers.find(channel);
          if (it == writers.end()) break;
          uint64_t timestamp = baseTimestamp + block.read16be();
          block.read8();
          auto data = block.get(block.sizeleft());
          Packet p;
          p.timestamp = timestamp;
          p.data = std::vector<uint8_t>(data.begin(), data.end());
          it->second.add(std::move(p));
        }
          break;
          
        case mkv_ids::Timestamp:
          baseTimestamp = block.GetIntValue();
          break;
        default: break;
        }
      }
    }
  }
};

std::shared_ptr<MuxInStream> OpenMatroskaIn(Graph& graph, mapping map) {
  auto p = std::make_shared<MatroskaMuxInStream>(std::move(map));
  if (p)
    graph.Add(p);
  return p;
}

}


