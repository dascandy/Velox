#pragma once

#include <vector>
#include <cstddef>
#include <cstdint>
#include <span>

enum class PixelFormat {
  Bitmask,
  Scale8,
  R5G6B5,
  RGB8,
  BGR8,
  RGBA8,
  BGRA8,
  RGB10A2,
  RGB16,
  RGBA16,
  RGB32F,
  RGBA32F,
};

enum class Framerate {
  F15,
  F24,
  F25,
  F30,
  F50,
  F60,
  F100,
};

enum class StillImageFormat {
  Unknown,
  Tga,
  Bmp,
  Png,
  Jpeg, 
};

enum class VideoStreamFormat {
  Unknown,
  Gif89a,
};

enum class MuxStreamFormat {
  Unknown,
  AVI,
  Mpeg,
  Ogg,
};

struct Color {
  float red = 0.0f, green = 0.0f, blue = 0.0f, alpha = 1.0f;
};

struct Image {
  Image() {}
  Image(size_t width, size_t height, PixelFormat format);
  Image ConvertTo(PixelFormat format) const;
  void setPixel(uint32_t x, uint32_t y, Color color);
  Color getPixel(uint32_t x, uint32_t y) const;
  std::vector<uint8_t> buffer;
  uint32_t width, height;
  PixelFormat format;
};

struct VideoStream {
  uint32_t width, height;
  PixelFormat format;
  Framerate rate;
  virtual ~VideoStream() {}
  virtual const Image& get() = 0;
  // returns true if the seek resulted in a different image to display
  virtual bool seek(size_t frameno) = 0;
  virtual VideoStream* clone() = 0;
  int64_t framecount;
};

Image LoadStillImage(std::span<const uint8_t> data, StillImageFormat format = StillImageFormat::Unknown);
VideoStream* LoadVideoStream(std::span<const uint8_t> data, VideoStreamFormat format = VideoStreamFormat::Unknown);
std::vector<uint8_t> SaveStillImage(const Image&, StillImageFormat format);

Image VerticalFlip(const Image& img);
Image HorizontalFlip(const Image& img);

