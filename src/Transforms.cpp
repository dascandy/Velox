#include "velox/Transforms.hpp"

namespace Velox {

Image FlipVertical(const Image& img) {
  Image target;
  target.width = img.width;
  target.height = img.height;
  target.format = img.format;
  target.buffer.resize(img.buffer.size());
  for (size_t y = 0; y < img.height; y++) {
    for (size_t x = 0; x < img.width; x++) {
      target.setPixel(x, y, img.getPixel(x, img.height-y-1));
    }
  }
  return target;
}

Image FlipHorizontal(const Image& img) {
  Image target;
  target.width = img.width;
  target.height = img.height;
  target.format = img.format;
  target.buffer.resize(img.buffer.size());
  for (size_t y = 0; y < img.height; y++) {
    for (size_t x = 0; x < img.width; x++) {
      target.setPixel(x, y, img.getPixel(img.width-x-1, y));
    }
  }
  return target;
}

}


