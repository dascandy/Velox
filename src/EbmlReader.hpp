#pragma once

#include "bini/reader.hpp"
#include "mkv_ids.hpp"
#include "caligo/crc.hpp"

struct EbmlReader : public Bini::reader {
  using reader::reader;
  uint64_t readvint() {
    size_t bytes = 0;
    uint64_t value = 0;
    do {
      value = (value << 8) | read8();
    } while (bytes < 10 && (value & (1 << (bytes * 7))) == 0);
    value ^= value & (1 << (bytes * 7));
    return value;
  }
  std::pair<mkv_ids, EbmlReader> getebml() {
    size_t id = readvint();
    size_t size = readvint();
    return { (mkv_ids)id, get(size == 0 ? sizeleft() : size) };
  }
  std::vector<std::pair<mkv_ids, EbmlReader>> chunks() {
    std::vector<std::pair<mkv_ids, EbmlReader>> rv;
    while (sizeleft()) rv.push_back(getebml());
    if (fail()) return {};
    return rv;
  }
  double GetFloat() {
    if (sizeleft() == 4) {
      return readfloatbe();
    } else if (sizeleft() == 8) {
      return readdoublebe();
    } else {
      setFail();
      return 0.0;
    }
  }
  std::string_view GetString() {
    return getString(sizeleft());
  }
  size_t GetIntValue() {
    return readSizedbe(sizeleft());
  }
};

