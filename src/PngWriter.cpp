#include "velox/Png.hpp"
#include <cstring>
#include <bini/writer.hpp>
#include <cstdio>
#include "PngStructs.hpp"
#include <caligo/crc.hpp>
#include <cstring>

namespace Velox {

template <typename T>
std::span<const uint8_t> AsSpan(const T&t) {
  return std::span<const uint8_t>((const uint8_t*)&t, (const uint8_t*)(&t + 1));
}

struct PngWriter : public Bini::writer {
  PngWriter() {
    add64be(0x89504E470D0A1A0A);
  }
  void addChunk(ChunkType type, std::span<const uint8_t> data) {
    Bini::writer header;
    header.add32be(data.size());
    header.add32be(type.value);
    add(header);
    add(data); 
    // endian swap?
    add(Caligo::CRC32(header, data).data());
  }
  using Bini::writer::data;
};

PixelFormat getPngFormat(PixelFormat format) {
  switch(format) {
  case PixelFormat::RGB32F: 
  case PixelFormat::RGB16: 
    return PixelFormat::RGB16;
  case PixelFormat::BGR8: 
  case PixelFormat::RGB8: 
    return PixelFormat::RGB8;
  case PixelFormat::RGB10A2: 
  case PixelFormat::RGBA32F: 
  case PixelFormat::RGBA16: 
    return PixelFormat::RGBA16;
  case PixelFormat::BGRA8: 
  case PixelFormat::RGBA8: 
    return PixelFormat::RGBA8;
  case PixelFormat::Bitmask:
    return PixelFormat::Bitmask;
  case PixelFormat::Grayscale8: 
    return PixelFormat::Grayscale8;
  case PixelFormat::Grayscale16: 
    return PixelFormat::Grayscale16;
  }
}

std::vector<uint8_t> PngImageData(const Image& image) {
  Bini::writer w;
  for (size_t n = 0; n < image.height; n++) {
    w.add8(0);
    w.add(image.GetLine(n));
  }
  return std::move(w);
}

std::vector<uint8_t> PngSave(const Image& image) {
  PixelFormat targetFormat = getPngFormat(image.format);
  if (targetFormat != image.format) {
    return PngSave(image.ConvertTo(targetFormat));
  }

  uint8_t bitdepth = 0;
  uint8_t colortype = 0;
  switch(targetFormat) {
  case PixelFormat::RGB16: 
  case PixelFormat::RGBA16: 
    bitdepth = 16; 
    colortype = 2;
    break;
  case PixelFormat::RGB8: 
  case PixelFormat::RGBA8: 
    bitdepth = 8; 
    colortype = 2;
    break;
  case PixelFormat::Bitmask:
    bitdepth = 1; 
    colortype = 0;
    break;
  case PixelFormat::Grayscale8: 
    bitdepth = 8; 
    colortype = 0;
    break;
  case PixelFormat::Grayscale16: 
    bitdepth = 16; 
    colortype = 0;
    break;
  default:
    std::abort();
  }

  Bini::writer header;
  header.add32be(image.width);
  header.add32be(image.height);
  header.add8(bitdepth);
  header.add8(colortype);
  header.add8(0); // compression, can only be 0
  header.add8(0); // filter, can only be 0
  header.add8(0); // interlace, we don't

  PngWriter writer;
  writer.addChunk(ChunkType::IHDR, header);
  writer.addChunk(ChunkType::IDAT, PngImageData(image));
  writer.addChunk(ChunkType::IEND, {});
  return std::move(writer);
}

struct PngSubframeData {
  uint32_t x;
  uint32_t y;
  uint8_t dispose;
  uint8_t blend;
  Image image;
};

bool RowMatches(const Image& first, const Image& second, size_t row) {
  if (first.width != second.width) return false;

  // Written to allow SIMD optimization
  bool rv = true;
  for (size_t n = 0; n < first.width; n++) {
    rv = rv & (first.getPixel(n, row) == second.getPixel(n, row));
  }
  return rv;
}

bool ColumnMatches(const Image& first, const Image& second, size_t column) {
  if (first.height != second.height) return false;

  // Written to allow SIMD optimization
  bool rv = true;
  for (size_t n = 0; n < first.height; n++) {
    rv = rv & (first.getPixel(column, n) == second.getPixel(column, n));
  }
  return rv;
}

PngSubframeData CreateSubframe(const Image& newImage, const Image& currentImage) {
  PngSubframeData sub;
  sub.dispose = 0;
  sub.blend = 0;

  sub.x = 0;
  sub.y = 0;
  while (sub.x < newImage.width && ColumnMatches(newImage, currentImage, sub.x)) sub.x++;
  while (sub.y < newImage.height && RowMatches(newImage, currentImage, sub.y)) sub.y++;

  uint32_t subw = newImage.width - sub.x;
  uint32_t subh = newImage.height - sub.y;
  while (subw > 0 && ColumnMatches(newImage, currentImage, sub.x + subw)) subw--;
  while (subh > 0 && RowMatches(newImage, currentImage, sub.y + subh)) subh--;

  sub.image = Image(subw, subh, newImage.format);
  for (size_t row = 0; row < subh; row++) {
    auto inLine = newImage.GetLine(sub.y + row);
    auto outLine = sub.image.GetLine(row);
    size_t offset = PixelFormatToBPP(newImage.format) * sub.x;
    memcpy((void*)outLine.data(), inLine.data() + offset, outLine.size());
  }

  return sub;
}

std::vector<uint8_t> SavePng(const Image& image) {
  ImageVideoStream video(image);
  return SavePngVideo(video);
}

std::vector<uint8_t> SavePngVideo(VideoStream& video) {
  PixelFormat targetFormat = getPngFormat(video.format);

  uint8_t bitdepth = 0;
  uint8_t colortype = 0;
  switch(targetFormat) {
  case PixelFormat::RGB16: 
  case PixelFormat::RGBA16: 
    bitdepth = 16; 
    colortype = 2;
    break;
  case PixelFormat::RGB8: 
  case PixelFormat::RGBA8: 
    bitdepth = 8; 
    colortype = 2;
    break;
  case PixelFormat::Bitmask:
    bitdepth = 1; 
    colortype = 0;
    break;
  case PixelFormat::Grayscale8: 
    bitdepth = 8; 
    colortype = 0;
    break;
  case PixelFormat::Grayscale16: 
    bitdepth = 16; 
    colortype = 0;
    break;
  default:
    std::abort();
  }

  Bini::writer header;
  header.add32be(video.width);
  header.add32be(video.height);
  header.add8(bitdepth);
  header.add8(colortype);
  header.add8(0); // compression, can only be 0
  header.add8(0); // filter, can only be 0
  header.add8(0); // interlace, we don't

  PngWriter writer;
  writer.addChunk(ChunkType::IHDR, header);

  video.seek(0);
  uint32_t frameCount = 1;
  uint32_t loopCount = 0;
  for (size_t n = 0; n < (size_t)video.framecount; n++) {
    if (video.seek(n)) frameCount++;
  }
  Bini::writer animControl;
  animControl.add32be(frameCount);
  animControl.add32be(loopCount);
  writer.addChunk(ChunkType::acTL, animControl);

  uint32_t framesSinceLast = 0;
  video.seek(0);
  Image currentImage = video.get();
  uint32_t seqNumber = 0;
  for (size_t n = 0; n < (size_t)video.framecount; n++) {
    bool newFrame = video.seek(n);
    if (newFrame || n == 0) {
      const Image& newImage = video.get();
      PngSubframeData subframe = CreateSubframe(newImage, currentImage);
      currentImage = newImage;

      Bini::writer frameControl;
      frameControl.add32be(seqNumber++);
      frameControl.add32be(subframe.image.width);
      frameControl.add32be(subframe.image.height);
      frameControl.add32be(subframe.x);
      frameControl.add32be(subframe.y);
      frameControl.add16be(framesSinceLast);
      frameControl.add16be((uint16_t)video.rate);
      frameControl.add8((uint8_t)subframe.dispose);
      frameControl.add8((uint8_t)subframe.blend);
      writer.addChunk(ChunkType::fcTL, frameControl);
      writer.addChunk(n == 0 ? ChunkType::IDAT : ChunkType::fdAT, PngImageData(subframe.image));
      framesSinceLast = 0;
    } else {
      framesSinceLast++;
    }
  }
  writer.addChunk(ChunkType::IEND, {});
  return std::move(writer);
}

}


