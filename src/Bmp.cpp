#include "velox/Bmp.hpp"
#include <cstring>
#include "velox/Transforms.hpp"
#include "bini/reader.hpp"
#include "bini/writer.hpp"
#include "velox/Color.hpp"
#include <cassert>

namespace Velox {

Image LoadBmp(std::span<const uint8_t> data) {
  Bini::reader r(data);
  uint16_t magic = r.read16le();
  if (magic != 0x4C42) 
    return {};

  uint32_t filesize = r.read32le();
  if (filesize < data.size())
    return {};

  r.skip(4);
  uint32_t pixeloffset = r.read32le();
  uint32_t dibheadersize = r.read32le();
  Bini::reader dib(r.get(dibheadersize - 2));
  uint32_t width = r.read32le();
  uint32_t height = r.read32le();
  r.skip(2);
  uint16_t bpp = r.read16le();
  uint32_t compression = r.read32le();
  r.skip(12);
  uint32_t colors = r.read32le();
  std::vector<Color> palette;
  if (compression != 0) return {};
  if (colors == 0) colors = 1 << bpp;
  if (bpp <= 8) {
    for (size_t n = 0; n < colors; n++) {
      palette.push_back(Color::from8888(r.read32be())); // be or le?
    }
  }
  Bini::reader imagedata(data.subspan(pixeloffset));

  Image img;
  if (bpp == 1) {
    img = Image(width, height, PixelFormat::Grayscale8);
  } else if (bpp <= 24) {
    img = Image(width, height, PixelFormat::RGB8);
  } else {
    img = Image(width, height, PixelFormat::RGBA8);
  }

  for (size_t y = height; y --> 0;) {
    uint32_t stride = (width * bpp + 7) / 8;
    while (stride & 3) stride++;
    Bini::bitreader<> line(r.get(stride));
    for (size_t x = 0; x < width; x++) {
      uint32_t value = line.get(bpp);
      switch(bpp) {
        case 1:
        case 4:
        case 8:
          if (value >= colors) return {};
          img.setPixel(x, y, palette[value]);
          break;
        case 16:
          img.setPixel(x, y, Color::from565(value));
          break;
        case 24:
          img.setPixel(x, y, Color::from888(value));
          break;
        case 32:
          img.setPixel(x, y, Color::from8888(value));
          break;
      }
    }
  }
  return img;
}

}

