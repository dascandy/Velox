#include "Scsi.hpp"
#include <array>
#include <cstdint>
#include <scsi/sg.h>
#include <span>
#include <vector>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>

std::array<uint8_t, 10> ScsiCommand(uint8_t command, uint8_t mode, uint8_t buffer, uint32_t offset, uint32_t arglength, uint8_t controlbyte) {
  return std::array<uint8_t, 10>{ 
    command, mode, buffer, 
    static_cast<uint8_t>(offset >> 16), static_cast<uint8_t>(offset >> 8), static_cast<uint8_t>(offset),
    static_cast<uint8_t>(arglength >> 16), static_cast<uint8_t>(arglength >> 8), static_cast<uint8_t>(arglength),
    controlbyte 
  };
}

sg_io_hdr_t create_command(std::span<const uint8_t> command, bool read, bool write, std::span<const uint8_t> buffer, std::span<uint8_t> sense) {
  sg_io_hdr_t cmd;
  cmd.interface_id = 'S';
  cmd.dxfer_direction = read ? (write ? SG_DXFER_TO_FROM_DEV : SG_DXFER_FROM_DEV) : (write ? SG_DXFER_TO_DEV : SG_DXFER_NONE);
  cmd.cmd_len = command.size();
  cmd.mx_sb_len = sense.size();
  cmd.iovec_count = 0;
  cmd.dxfer_len = buffer.size();
  cmd.dxferp = const_cast<uint8_t*>(buffer.data());
  cmd.cmdp = const_cast<uint8_t*>(command.data());
  cmd.sbp = sense.data();
  cmd.timeout = 1000;
  cmd.flags = 0;
  cmd.pack_id = 0;
  cmd.usr_ptr = nullptr;
  cmd.status = 0;
  cmd.masked_status = 0;
  cmd.msg_status = 0;
  cmd.sb_len_wr = 0;
  cmd.host_status = 0;
  cmd.driver_status = 0;
  cmd.resid = 0;
  cmd.duration = 0;
  cmd.info = 0;
  return cmd;
}

std::pair<bool, std::vector<uint8_t>> readbuffer(int fd, uint8_t buffer, uint32_t offset, uint32_t size) {
  std::vector<uint8_t> data;
  data.resize(size);
  std::vector<uint8_t> sense;
  sense.resize(64);
  auto commandblock = ScsiCommand(0x3c, 1, buffer, offset, size, 0);
  sg_io_hdr_t command = create_command(commandblock, true, false, data, sense);
  ioctl(fd, SG_IO, &command);
  if (command.masked_status == 0) {
    return { true, data };
  } else {
    return { false, sense };
  }
}

std::pair<bool, std::vector<uint8_t>> writebuffer(int fd, uint8_t buffer, uint32_t offset, std::span<const uint8_t> data) {
  std::vector<uint8_t> sense;
  auto commandblock = ScsiCommand(0x3b, 6, buffer, offset, data.size(), 0);
  sg_io_hdr_t command = create_command(commandblock, false, true, data, sense);
  ioctl(fd, SG_IO, &command);
  if (command.masked_status == 0) {
    return { true, {} };
  } else {
    return { false, sense };
  }
}

