#include "velox/Gif.hpp"
#include <cstring>
#include <cstdio>

namespace Velox {

struct GifHeader {
  char sig[6];
  uint16_t width, height;
  uint8_t pack;
  uint8_t bgColor;
  uint8_t aspectRatio;
} __attribute__((packed));

struct GifImage {
  uint16_t x, y, width, height;
  uint8_t flags;
} __attribute__((packed));

enum class Type {
  Image = 0x2C,
  Extension = 0x21,
  GCE = 0xF9,
  Application = 0xFF,
  Trailer = 0x3B,
};

struct Gce {
  uint8_t blocksize;
  uint8_t flags;
  uint16_t delay;
  uint8_t transparentColor;
  uint8_t terminator;
} __attribute__((packed));

GifStream::GifStream(std::span<const uint8_t> data) 
: data(data)
{
  rate = Framerate::F100;
  GifHeader* hdr = (GifHeader*)data.data();
  width = hdr->width;
  height = hdr->height;
  currentImage = Image(width, height, PixelFormat::RGBA8);
  size_t offset = sizeof(GifHeader);
  if (hdr->pack & 0x80) {
    size_t tableSize = 6 << (hdr->pack & 0x7);
    globalColorTable = data.subspan(offset, tableSize);
    offset += tableSize;
  }
  uint32_t currentFrameNo = 0;
  ImageDataReference* lastImage = nullptr;
  Gce lastgce = {};
  const uint8_t* firstImage = nullptr;
  while (offset < data.size()) {
    switch((Type)data[offset++]) {
    case Type::Extension:
      switch((Type)data[offset++]) {
      case Type::GCE:
        if (firstImage == nullptr) firstImage = data.data() + offset - 2;
        lastgce = *(Gce*)(data.data() + offset);
        offset += sizeof(Gce);
        break;
      case Type::Application:
      {
        // Check for Netscape loop thing
        uint8_t tocheck[] = { 0x0b, 'N', 'E', 'T', 'S', 'C', 'A', 'P', 'E', '2', '.', '0', 0x03, 0x01 };
        if (memcmp(data.data() + offset, tocheck, sizeof(tocheck)) == 0) {
          loopCount = data[offset+14] + data[offset+15] * 256;
        }
        offset += 12;
        while (data[offset] != 0) {
          offset += data[offset] + 1;
        }
        offset++;
      }
        break;
      default:
  fprintf(stderr, "%s:%d Unknown extension %d\n", __FILE__, __LINE__, (int)data[offset-1]);
        offset += data[offset] + 2;
//        framecount = 0;
//        return;
      }
      break;
    case Type::Image:
      if (firstImage == nullptr) firstImage = data.data() + offset - 1;

      // skip over image block
      if (data[offset + 8] & 0x80) {
        size_t tableSize = 6 << (data[offset+8] & 0x7);
        offset += tableSize;
      }
      offset += 10; // image block + lzw start
      while (data[offset] != 0) {
        offset += data[offset] + 1;
      }
      offset++;

      if (lastgce.delay != 0 || (lastgce.flags & 0x1C) > 0x04) {
        ImageDataReference& currentImage = offsets[currentFrameNo];
        // end of image chunk
        currentImage.imageData = std::span<const uint8_t>(firstImage, data.data()+offset);
        currentImage.overlaidOn = lastImage;
        switch(lastgce.flags & 0x1C) {
          default:
          case 0x00:
          case 0x04:
            currentImage.clearStyle = ImageDataReference::ClearStyle::LeaveAsIs;
            lastImage = &currentImage;
            break;
          case 0x08:
            currentImage.clearStyle = ImageDataReference::ClearStyle::ClearToBackground;
            lastImage = nullptr;
            break;
          case 0x0C:
            currentImage.clearStyle = ImageDataReference::ClearStyle::LeaveAsIs;
            break;
        }
        firstImage = nullptr;
        currentFrameNo += lastgce.delay;
      }
      break;
    case Type::Trailer:
    {
      if (firstImage) {
        ImageDataReference& currentImage = offsets[currentFrameNo];
        // end of image chunk
        currentImage.imageData = std::span<const uint8_t>(firstImage, data.data()+offset);
        currentImage.overlaidOn = lastImage;
        switch(lastgce.flags & 0x1C) {
          default:
          case 0x00:
          case 0x04:
            currentImage.clearStyle = ImageDataReference::ClearStyle::LeaveAsIs;
            lastImage = &currentImage;
            break;
          case 0x08:
            currentImage.clearStyle = ImageDataReference::ClearStyle::ClearToBackground;
            lastImage = nullptr;
            break;
          case 0x0C:
            currentImage.clearStyle = ImageDataReference::ClearStyle::LeaveAsIs;
            break;
        }
        firstImage = nullptr;
        currentFrameNo += lastgce.delay;
      }
      framecount = currentFrameNo;
    }
      return;
    default:
      fprintf(stderr, "unknown ext at %d offset %d\n", data[offset-1], (int)offset-1);
      framecount = 0;
      return;
    }
  }
  // Log error about file being incomplete
  framecount = currentFrameNo;
}

struct LzwDecoder {
  LzwDecoder(Image& image, std::span<const uint8_t> colorTable, uint8_t mcs, GifImage* id, uint16_t transColorId)
  : image(image)
  , mcs(mcs+1)
  , id(id)
  , transColorId(transColorId)
  {
    curX = id->x;
    curY = id->y;
    for (size_t n = 0; n < colorTable.size(); n += 3) {
      colorMap.push_back(Color{colorTable[n] / 255.0f, colorTable[n+1] / 255.0f, colorTable[n+2] / 255.0f});
    }
    while (colorMap.size() < (1 << mcs)) {
      colorMap.push_back({});
    }
    currentIndex = colorMap.size() + 1;
    buffer = 0;
    bits = 0;
  }
  uint8_t cs() {
    uint8_t cs = 0;
    if (currentIndex == 4096) return 12;
    uint16_t cur = currentIndex;
    while (cur) { cs++; cur /= 2; }
//    return mcs > cs ? mcs : cs;
    return cs;
  }
  void addByte(uint8_t byte) {
    buffer |= (byte << bits);
    bits += 8;
    while (bits >= cs()) {
      uint32_t value = buffer & ((1 << cs()) - 1);
      buffer >>= cs();
      bits -= cs();
      addIndex(value);
    }
  }
  void chaseChain(uint16_t index) {
    if (index > colorMap.size()) {
      auto [color, target] = lzwEntries[index - colorMap.size() - 2];
      chaseChain(target);
      index = color;
    }
    addColor(index);
  }
  void addIndex(uint16_t index) {
    if (index == colorMap.size()) { // Clear code
      lzwEntries.clear();
      currentIndex = colorMap.size() + 1;
      printf("clear code\n");
    } else if (index == colorMap.size() + 1) { // End of image code
      printf("EOI code\n");
      // and then what?
    } else {
      if (currentIndex < 4096 && not lzwEntries.empty()) {
        uint16_t seekIndex = index;
        while (seekIndex >= colorMap.size()) seekIndex = lzwEntries[seekIndex - colorMap.size() - 2].second;
        lzwEntries[currentIndex - colorMap.size() - 2].first = (uint8_t)seekIndex;
      }

      if (index < colorMap.size()) {
        addColor(index);
      } else {
        auto [color, target] = lzwEntries[index - colorMap.size() - 2];
        chaseChain(target);
        addColor(color);
      }
      if (currentIndex < 4096)
        currentIndex++;
      lzwEntries.push_back({0, index});
    }
  }
  void addColor(uint8_t colorId) {
    if (colorId != transColorId) {
      image.setPixel(curX, curY, colorMap[colorId]);
    }
    curX++;
    if (curX == id->x + id->width) {
      curY++;
      curX = id->x;
    }
  }
  uint8_t bits = 0;
  uint32_t buffer = 0;
  Image& image;
  uint8_t mcs = 0;
  GifImage* id;
  uint16_t transColorId = 256;
  uint16_t currentIndex = 0;
  uint32_t curX = 0, curY = 0;
  std::vector<Color> colorMap;
  std::vector<std::pair<uint8_t, uint16_t>> lzwEntries;
};

void GifStream::Decode(ImageDataReference* ref) {
  if (ref->overlaidOn != currentReference) {
    if (ref->overlaidOn == nullptr) {
//      currentImage.clear(); // to what?
    } else {
      Decode(ref->overlaidOn);
    }
  }
  Gce* gce = nullptr;
  GifImage* image = nullptr;
  std::span<const uint8_t> colorTable = globalColorTable;
  size_t offset = 0;
  uint16_t transColorId = 256;
  while (ref->imageData[offset] == (uint8_t)Type::Extension) {
    if (ref->imageData[offset+1] == (uint8_t)Type::GCE) {
      gce = (Gce*)(&ref->imageData[offset+2]);
      offset += 8;
      if (gce->flags & 1) {
        transColorId = gce->transparentColor;
      }
    } else {
      offset += 2;
      while (ref->imageData[offset] != 0) {
        offset += ref->imageData[offset] + 1;
      }
      offset++;
    }
  }
  image = (GifImage*)(&ref->imageData[offset+1]);
  offset += 10;
  if (image->flags & 0x80) {
    size_t tableSize = 6 << (image->flags & 0x7);
    colorTable = ref->imageData.subspan(offset, tableSize);
    offset += tableSize;
  }
  uint8_t mcs = ref->imageData[offset++];

  LzwDecoder decoder(currentImage, colorTable, mcs, image, transColorId);
  while (ref->imageData[offset]) {
    uint8_t bytes = ref->imageData[offset++];
    for (size_t n = 0; n < bytes; n++) {
      decoder.addByte(ref->imageData[offset++]);
    }
  }

  currentReference = ref;
}

const Image& GifStream::get() {
  return currentImage;
}

bool GifStream::seek(size_t frameno) {
  ImageDataReference* best = nullptr;
  for (auto& [time, image] : offsets) {
    if (time <= frameno) best = &image;
  }
  if (best == currentReference) return false;

  if (best != nullptr)
    Decode(best);
  return true;
}

Image LoadGif(std::span<const uint8_t> data) {
  return GifStream(data).get();
}

std::unique_ptr<VideoStream> LoadGifVideo(std::span<const uint8_t> data) {
  return std::make_unique<GifStream>(data);
}

}

