#include "velox/MuxStream.hpp"

namespace Velox {

/*
struct ChapterSplitter : GraphNode {
  std::vector<std::shared_ptr<MuxInStream>> streams;
  std::shared_ptr<MuxInStream> inputStream;
  std::vector<std::thread> threads;
  std::vector<uint64_t> splitpoints;
  ChapterSplitter(std::shared_ptr<MuxInStream> stream) 
  : inputStream(stream) {
    for (size_t n = 0; n < stream->chapters.size(); n++) {
      streams.push_back(std::make_shared<MuxInStream>());
      auto& m = streams.back();
      m->chapters.push_back(stream->chapters[n]);
      m->tags = stream->tags;
      m->title = stream->title;
      splitpoints.push_back(stream->chapters[n].timestamp);
    }
    for (auto& v : stream->videostreams) {
      std::vector<Origo::channel<Packet>*> out;
      for (size_t n = 0; n < stream->chapters.size(); n++) {
        auto& m = streams[n];
        m->videostreams.push_back(std::make_shared<VideoStreamDescription>(v->codec, v->framerate, v->imagesize, v->aspectratio));
        m->videostreams.back()->description = v->description;
        m->videostreams.back()->codecPrivate = v->codecPrivate;
        m->videostreams.back()->interlaced = v->interlaced;
        out.push_back(&m->videostreams.back()->packets);
      }
      threads.push_back(std::thread([in = &v->packets, out = std::move(out), this]{ CopyThread(*in, out); }));
    }
    for (auto& a : stream->audiostreams) {
      std::vector<Origo::channel<Packet>*> out;
      for (size_t n = 0; n < stream->chapters.size(); n++) {
        auto& m = streams[n];
        m->audiostreams.push_back(std::make_shared<AudioStreamDescription>(a->codec, a->samplingFrequency, a->channels));
        m->audiostreams.back()->language = a->language;
        m->audiostreams.back()->description = a->description;
        m->audiostreams.back()->bitdepth = a->bitdepth;
        m->audiostreams.back()->bitrate = a->bitrate;
        m->audiostreams.back()->commentaryTrack = a->commentaryTrack;
        out.push_back(&m->audiostreams.back()->packets);
      }
      threads.push_back(std::thread([in = &a->packets, out = std::move(out), this]{ CopyThread(*in, out); }));
    }
    for (auto& s : stream->subtitlestreams) {
      std::vector<Origo::channel<Packet>*> out;
      for (size_t n = 0; n < stream->chapters.size(); n++) {
        auto& m = streams[n];
        m->subtitlestreams.push_back(std::make_shared<SubtitleStreamDescription>(s->format, s->language));
        m->subtitlestreams.back()->description = s->description;
        m->subtitlestreams.back()->encoding = s->encoding;
        m->subtitlestreams.back()->commentaryTrack = s->commentaryTrack;
        m->subtitlestreams.back()->hardOfHearing = s->hardOfHearing;
        out.push_back(&m->audiostreams.back()->packets);
      }
      threads.push_back(std::thread([in = &s->packets, out = std::move(out), this]{ CopyThread(*in, out); }));
    }
  }
  void CopyThread(Origo::channel<Packet>& in, std::vector<Origo::channel<Packet>*> out) {
    size_t track = 0;
    try {
      while (true) {
        Packet p = in.get();
        if (track + 1 < splitpoints.size() && p.timestamp >= splitpoints[track+1]) {
          out[track]->close_write();
          track++;
        }
        out[track]->add_value(std::move(p));
      }
    } catch (ChannelDone&) {
      out.back()->close_write();
    }
  }
  ~ChapterSplitter() {
    for (auto& t : threads) t.join();
  }
};

std::vector<std::shared_ptr<MuxInStream>> ChapterSplit(std::shared_ptr<MuxInStream> stream) {
  std::shared_ptr<ChapterSplitter> cs = std::make_shared<ChapterSplitter>(stream);
  // How to keep this dang thing alive?
  return cs->streams;
}
*/

}

