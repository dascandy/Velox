#pragma once

namespace Velox {

enum DTS_channels {
  DTS_CHANNELS_MONO = 0,
  DTS_CHANNELS_DUAL_MONO = 1,
  DTS_CHANNELS_STEREO = 2,
  DTS_CHANNELS_STEREO_SUMDIFF = 3,
  DTS_CHANNELS_STEREO_TOTAL = 4,
  DTS_CHANNELS_3FRONT = 5,
  DTS_CHANNELS_2FRONT_1REAR = 6,
  DTS_CHANNELS_3FRONT_1REAR = 7,
  DTS_CHANNELS_2FRONT_2REAR = 8,
  DTS_CHANNELS_3FRONT_2REAR = 9,
  DTS_CHANNELS_4FRONT_2REAR = 10,
  DTS_CHANNELS_3FRONT_2REAR_1OV = 11,
  DTS_CHANNELS_3FRONT_3REAR = 12,
  DTS_CHANNELS_5FRONT_2REAR = 13,
  DTS_CHANNELS_4FRONT_4REAR = 14,
  DTS_CHANNELS_5FRONT_3REAR = 15,
  DTS_CHANNELS_LFE = 16,
};

std::tuple<std::vector<AudioChannelPosition>, int, int> dts_packet_to_channels(std::span<const uint8_t> firstPacket) {
  Bini::bitreader<> r(firstPacket);
  uint32_t syncval = r.get(32); // sync
  if (syncval != 0x7ffe8001) return {};
  r.get(28); // 5 fields we ignore
  uint8_t channelValue = r.get(6);
  uint8_t sfreq = r.get(4);
  r.get(15);
  bool lfe = r.get(1);
  if (lfe) channelValue += 16;
  
  std::vector<AudioChannelPosition> channels;
  switch(channelValue) {
  case DTS_CHANNELS_MONO: channels = { AudioChannelPosition::Center }; break;
  case DTS_CHANNELS_DUAL_MONO: channels = { AudioChannelPosition::Center }; break;
  case DTS_CHANNELS_STEREO: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }; break;
  case DTS_CHANNELS_STEREO_SUMDIFF: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }; break;
  case DTS_CHANNELS_STEREO_TOTAL: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }; break;
  case DTS_CHANNELS_3FRONT: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight }; break;
  case DTS_CHANNELS_2FRONT_1REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter }; break;
  case DTS_CHANNELS_3FRONT_1REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter }; break;
  case DTS_CHANNELS_2FRONT_2REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearRight }; break;
  case DTS_CHANNELS_3FRONT_2REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearRight }; break;
  case DTS_CHANNELS_4FRONT_2REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearRight }; break;
  case DTS_CHANNELS_3FRONT_2REAR_1OV: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearRight, AudioChannelPosition((int)AudioChannelPosition::Center | (int)AudioChannelPosition::Overhead) }; break;
  case DTS_CHANNELS_3FRONT_3REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearCenter, AudioChannelPosition::RearRight }; break;
  case DTS_CHANNELS_5FRONT_2REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearRight }; break;
  // This makes no sense to me.
  case DTS_CHANNELS_4FRONT_4REAR: channels = { AudioChannelPosition(19), AudioChannelPosition(22), AudioChannelPosition(2), AudioChannelPosition(5), AudioChannelPosition(8), AudioChannelPosition(11), AudioChannelPosition(13), AudioChannelPosition(16) }; break;
  case DTS_CHANNELS_5FRONT_3REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearCenter, AudioChannelPosition::RearRight }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_MONO: channels = { AudioChannelPosition::Center }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_DUAL_MONO: channels = { AudioChannelPosition::Center }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_STEREO: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_STEREO_SUMDIFF: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_STEREO_TOTAL: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_3FRONT: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_2FRONT_1REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_3FRONT_1REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::RearCenter }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_2FRONT_2REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearRight }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_3FRONT_2REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearRight }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_4FRONT_2REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearRight }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_3FRONT_2REAR_1OV: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearRight, AudioChannelPosition((int)AudioChannelPosition::Center | (int)AudioChannelPosition::Overhead) }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_3FRONT_3REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearCenter, AudioChannelPosition::RearRight }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_5FRONT_2REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearRight }; break;
  // This makes no sense to me.
  case DTS_CHANNELS_LFE | DTS_CHANNELS_4FRONT_4REAR: channels = { AudioChannelPosition(19), AudioChannelPosition(22), AudioChannelPosition(2), AudioChannelPosition(5), AudioChannelPosition(8), AudioChannelPosition(11), AudioChannelPosition(13), AudioChannelPosition(16) }; break;
  case DTS_CHANNELS_LFE | DTS_CHANNELS_5FRONT_3REAR: channels = { AudioChannelPosition::FrontLeft, AudioChannelPosition::Center, AudioChannelPosition::FrontRight, AudioChannelPosition::SideLeft, AudioChannelPosition::SideRight, AudioChannelPosition::RearLeft, AudioChannelPosition::RearCenter, AudioChannelPosition::RearRight }; break;
  default: channels = {}; break;
  }

  int bitdepth, samples;

  const unsigned int samplerates[16] = {
    0, 8000, 16000, 32000, 0, 0, 11025, 22050, 44100, 0, 0,
    12000, 24000, 48000, 96000, 192000
  };
  if (sfreq < sizeof(samplerates) / 4) { 
    samples = samplerates[sfreq];
  }
  bitdepth = 24;

  return { std::move(channels), bitdepth, samples };
}

}


