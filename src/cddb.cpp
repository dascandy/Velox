#include "velox/cddb.hpp"
#include "fetch.hpp"

CddbLookupResult CddbLookup(std::string_view discid) {
  CddbLookupResult result;
  auto disc = fetch_json("https://musicbrainz.org/ws/2/discid/" + std::string(discid) + "?fmt=json");
  result.mbid = std::string(disc["releases"][0]["id"]);
  auto release = fetch_json("https://musicbrainz.org/ws/2/release/" + result.mbid + "?fmt=json&inc=artist-credits+discids+recordings");
  std::vector<std::string> tracktitles;
  result.artist = std::string(release["artist-credit"][0]["name"]);
  result.title = std::string(release["title"]);
  result.releaseDate = std::string(release["date"]);
  
  for (auto& disc : release["media"]) {
    for (auto& disc_v : disc["discs"]) {
      if (disc_v["id"] != discid) continue;
      for (auto track : disc["tracks"]) {
        result.tracks.push_back({std::string(track["artist-credit"][0]["name"]), std::string(track["title"]), 1000000ULL * long(track["length"])});
      }
      if (result.tracks.size() > 0) break;
    }
    if (result.tracks.size() > 0) break;
  }

  return result;
}


