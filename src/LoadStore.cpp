#include "velox/Image.hpp"
#include "velox/Transforms.hpp"
#include "velox/Gif.hpp"
#include "velox/Tga.hpp"
#include "velox/Png.hpp"
#include "velox/Bmp.hpp"
#include "velox/Jpeg.hpp"
#include <cmath>
#include <cstring>
#include <cstdio>
#include <cassert>

namespace Velox {

FileFormat GuessFormat(std::span<const uint8_t> data) {
  if (data.size() < 8) return FileFormat::Unknown;

  switch(data[0]) {
    case 0x1A:
      return FileFormat::Mkv;
    case 0x42:
      return FileFormat::Bmp;
    case 0x89:
      return FileFormat::Png;
    case 0xFF:
      return FileFormat::Jpeg;
    case 0x47:
      return FileFormat::Gif89a;
    case 0x52:
      return FileFormat::Wav;
  }

  if (data[1] < 2 &&
      ((data[2] >= 1 && data[2] <= 3) || (data[2] >= 9 && data[2] <= 11)))
    return FileFormat::Tga;

  if (data[4] == 'f' && data[5] == 't' && data[6] == 'y' && data[7] == 'p')
    return FileFormat::Quicktime;

  return FileFormat::Unknown;
}

Image LoadStillImage(std::span<const uint8_t> data, FileFormat format) {
  if (format == FileFormat::Unknown) {
    format = GuessFormat(data);
  }

  switch(format) {
    case FileFormat::Tga:
      return LoadTga(data);
    case FileFormat::Png:
      return LoadPng(data);
    case FileFormat::Gif89a:
      return LoadGif(data);
    case FileFormat::Bmp:
      return LoadBmp(data);
    case FileFormat::Jpeg:
      return LoadJpeg(data);
    default:
      return {};
  } 
}

std::vector<uint8_t> SaveStillImage(const Image& image, FileFormat format) {
  switch(format) {
    case FileFormat::Tga:
      return SaveTga(image);
    case FileFormat::Png:
      return SavePng(image);
//    case FileFormat::Jpeg:
//      return SaveJpeg(image);
    default:
      return {};
  }
}

std::unique_ptr<VideoStream> LoadVideoStream(std::span<const uint8_t> data, FileFormat format) {
  if (format == FileFormat::Unknown) {
    format = GuessFormat(data);
  }
  switch(format) { 
    case FileFormat::Gif89a:
      return LoadGifVideo(data);
    case FileFormat::Png:
      return LoadPngVideo(data);
    default:
    {
      auto image = LoadStillImage(data, format);
      if (image.width) return std::make_unique<ImageVideoStream>(std::move(image));
      return nullptr;
    }
  } 
}

}


