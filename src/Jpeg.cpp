#include "velox/Jpeg.hpp"
#include <cstring>
#include "velox/Transforms.hpp"
#include "bini/reader.hpp"
#include "bini/writer.hpp"
#include "velox/Color.hpp"
#include <cassert>
#include <exception>

namespace Velox {

enum JpegFrame : uint16_t {
  SOI = 0xFFD8,
  SOF0 = 0xFFC0,
  SOF2 = 0xFFC2,
  DHT = 0xFFC4,
  DQT = 0xFFDB,
  DRI = 0xFFDD,
  SOS = 0xFFDA,
  RST0 = 0xFFD0,
  APP0 = 0xFFE0,
  APP1 = 0xFFE1,
  COM = 0xFFFE,
  EOI = 0xFFD9,
};

struct HuffmanTable {
  std::span<const uint8_t> counts;
  std::span<const uint8_t> values;
  HuffmanTable() {}
  HuffmanTable(std::span<const uint8_t> counts, std::span<const uint8_t> values) 
  : counts(counts)
  , values(values)
  {
    size_t val = 1;
    size_t total = 0;
    for (auto& c : counts) {
      val <<= 1;
      if (val <= c) throw std::runtime_error("Invalid huffman table");
      val -= c;
      total += c;
    }
    if (total != values.size()) throw std::runtime_error("Invalid huffman table");
  }
  uint8_t get(Bini::bitreader<>& r) {
    uint32_t value = 0;
    uint32_t counter = 0;
    uint8_t shift = 0;
    uint32_t had = 0;
    while (true) {
      shift++;
      value = (value << 1) | r.get(1);
      counter <<= 1;
      uint32_t maxval = counter + counts[shift-1];
      if (value < maxval) {
        had += (value - counter);
        return values[had];
      }
      counter += counts[shift-1];
      had += counts[shift-1];
      if (had == values.size()) std::terminate(); // out of table
    }
  }
};

struct JpegReader : public Bini::reader {
  using reader::reader;
  std::vector<uint8_t> getSosBlock() {
    std::vector<uint8_t> data;
    while (sizeleft()) {
      const uint8_t* backup_p = p;
      uint8_t v = read8();
      if (v == 0xFF) {
        uint8_t v2 = read8();
        if (v2 > 0x00) {
          p = backup_p;
          return data;
        }
        data.push_back(v);
      } else {
        data.push_back(v);
      }
    }
    return data;
  }
};

struct Component {
  uint8_t h_ss;
  uint8_t v_ss;
  uint8_t tq;
  uint8_t hd;
  uint8_t ha;
};

std::array<std::pair<uint8_t, uint8_t>, 64> indexFromZZ = {
  std::pair<uint8_t, uint8_t>{ 0, 0 },
  std::pair<uint8_t, uint8_t>{ 1, 0 },
  std::pair<uint8_t, uint8_t>{ 0, 1 },
  std::pair<uint8_t, uint8_t>{ 0, 2 },
  std::pair<uint8_t, uint8_t>{ 1, 1 },
  std::pair<uint8_t, uint8_t>{ 2, 0 },
  std::pair<uint8_t, uint8_t>{ 3, 0 },
  std::pair<uint8_t, uint8_t>{ 2, 1 },
  std::pair<uint8_t, uint8_t>{ 1, 2 },
  std::pair<uint8_t, uint8_t>{ 0, 3 },
  std::pair<uint8_t, uint8_t>{ 0, 4 },
  std::pair<uint8_t, uint8_t>{ 1, 3 },
  std::pair<uint8_t, uint8_t>{ 2, 2 },
  std::pair<uint8_t, uint8_t>{ 3, 1 },
  std::pair<uint8_t, uint8_t>{ 4, 0 },
  std::pair<uint8_t, uint8_t>{ 5, 0 },
  std::pair<uint8_t, uint8_t>{ 4, 1 },
  std::pair<uint8_t, uint8_t>{ 3, 2 },
  std::pair<uint8_t, uint8_t>{ 2, 3 },
  std::pair<uint8_t, uint8_t>{ 1, 4 },
  std::pair<uint8_t, uint8_t>{ 0, 5 },
  std::pair<uint8_t, uint8_t>{ 0, 6 },
  std::pair<uint8_t, uint8_t>{ 1, 5 },
  std::pair<uint8_t, uint8_t>{ 2, 4 },
  std::pair<uint8_t, uint8_t>{ 3, 3 },
  std::pair<uint8_t, uint8_t>{ 4, 2 },
  std::pair<uint8_t, uint8_t>{ 5, 1 },
  std::pair<uint8_t, uint8_t>{ 6, 0 },
  std::pair<uint8_t, uint8_t>{ 7, 0 },
  std::pair<uint8_t, uint8_t>{ 6, 1 },
  std::pair<uint8_t, uint8_t>{ 5, 2 },
  std::pair<uint8_t, uint8_t>{ 4, 3 },
  std::pair<uint8_t, uint8_t>{ 3, 4 },
  std::pair<uint8_t, uint8_t>{ 2, 5 },
  std::pair<uint8_t, uint8_t>{ 1, 6 },
  std::pair<uint8_t, uint8_t>{ 0, 7 },
  std::pair<uint8_t, uint8_t>{ 1, 7 },
  std::pair<uint8_t, uint8_t>{ 2, 6 },
  std::pair<uint8_t, uint8_t>{ 3, 5 },
  std::pair<uint8_t, uint8_t>{ 4, 4 },
  std::pair<uint8_t, uint8_t>{ 5, 3 },
  std::pair<uint8_t, uint8_t>{ 6, 2 },
  std::pair<uint8_t, uint8_t>{ 7, 1 },
  std::pair<uint8_t, uint8_t>{ 7, 2 },
  std::pair<uint8_t, uint8_t>{ 6, 3 },
  std::pair<uint8_t, uint8_t>{ 5, 4 },
  std::pair<uint8_t, uint8_t>{ 4, 5 },
  std::pair<uint8_t, uint8_t>{ 3, 6 },
  std::pair<uint8_t, uint8_t>{ 2, 7 },
  std::pair<uint8_t, uint8_t>{ 3, 7 },
  std::pair<uint8_t, uint8_t>{ 4, 6 },
  std::pair<uint8_t, uint8_t>{ 5, 5 },
  std::pair<uint8_t, uint8_t>{ 6, 4 },
  std::pair<uint8_t, uint8_t>{ 7, 3 },
  std::pair<uint8_t, uint8_t>{ 7, 4 },
  std::pair<uint8_t, uint8_t>{ 6, 5 },
  std::pair<uint8_t, uint8_t>{ 5, 6 },
  std::pair<uint8_t, uint8_t>{ 4, 7 },
  std::pair<uint8_t, uint8_t>{ 5, 7 },
  std::pair<uint8_t, uint8_t>{ 6, 6 },
  std::pair<uint8_t, uint8_t>{ 7, 5 },
  std::pair<uint8_t, uint8_t>{ 7, 6 },
  std::pair<uint8_t, uint8_t>{ 6, 7 },
  std::pair<uint8_t, uint8_t>{ 7, 7 },
};

void applyComponent(std::array<float, 64>& i, size_t which, int16_t value) {
  const float pi = 3.141592653589793;
  auto [sx, sy] = indexFromZZ[which];
  printf("applying %d at %u/%u\n", value, sx, sy);
  for (size_t y = 0; y < 8; y++) {
    for (size_t x = 0; x < 8; x++) {
      i[y*8+x] += cos((pi * (2 * x + 1) * sx) / 16) * cos((pi * (2 * y + 1) * sy) / 16) * value;
    }
  }
}

int32_t numToDelta(uint8_t ssss, Bini::bitreader<>& br) {
  if (ssss == 16) return 32768;
  if (ssss == 0) return 0;
  int16_t val = br.get(ssss);
  if (val < (1 << (ssss - 1))) {
    return val - (1 << ssss) - 1;
  } else {
    return val;
  }
}

void decode_macroblock(Image& i, int dx, int dy, Bini::bitreader<>& br, int32_t& dc_val, HuffmanTable& dc, HuffmanTable& ac, std::array<uint16_t, 64>& quant) {
  std::array<float, 64> coefficients = {
    2, 
    1.4142, 1.4142, 
    1.4142, 1, 1.4142, 
    1.4142, 1, 1, 1.4142, 
    1.4142, 1, 1, 1, 1.4142, 
    1.4142, 1, 1, 1, 1, 1.4142, 
    1.4142, 1, 1, 1, 1, 1, 1.4142, 
    1.4142, 1, 1, 1, 1, 1, 1, 1.4142, 
    1, 1, 1, 1, 1, 1, 1, 
    1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1,
    1, 1, 1, 1,
    1, 1, 1,
    1, 1,
    1,
  };
  std::array<float, 64> raw = {};
  uint8_t val = dc.get(br);
  dc_val += numToDelta(val, br);
  applyComponent(raw, 0, dc_val * coefficients[0] * quant[0]);

  size_t index = 1;
  while (index < 64)
  {
    uint8_t code = ac.get(br);
    if (code == 0) break;
    index += code >> 4;
    uint8_t extra = code & 0xf;
    if (index < 64) {
      int16_t ac_val = numToDelta(extra, br);
      applyComponent(raw, index, ac_val * coefficients[index] * quant[index]);
      index++;
    }
  }
  for (size_t y = 0; y < 8; y++) {
    for (size_t x = 0; x < 8; x++) {
      float value = (128 + raw[y*8+x] / 8.0) / 255.0f;
      printf("value %f\n", value);
      i.setPixel(x + dx, y + dy, Color{ value, value, value });
      printf("%f\n", i.getPixel(x + dx, y + dy).red);
    }
  }
}

Image LoadJpeg(std::span<const uint8_t> data) {
  std::array<uint16_t, 64> quant[4] = {};
  JpegReader r(data);
  uint8_t bpp;
  uint16_t height;
  uint16_t width;
  uint8_t components;
  std::map<uint8_t, Component> comps;
  std::map<uint8_t, HuffmanTable> huffmanTables;
  std::map<uint8_t, Image> subimages;
  while (true) {
    uint16_t magic = r.read16be();
    if (magic == JpegFrame::SOI) continue;
    else if (magic == JpegFrame::APP0) {
      uint16_t size = r.read16be();
      Bini::reader app(r.get(size-2));
      uint32_t id = app.read32be();
      if (id == 0x4A464946) {
        // JFIF version, desnity, thumbnail size and thumbnail data
      } else if (id == 0x4A465858) {
        // JFXX block, with 3 other thumbnails
      }
    } else if (magic == JpegFrame::SOF0) {
      uint16_t size = r.read16be();
      Bini::reader sof(r.get(size-2));
      bpp = sof.read8();
      height = sof.read16be();
      width = sof.read16be();
      components = sof.read8();
      for (size_t n = 0; n < components; n++) {
        Component c;
        uint8_t id = sof.read8();
        uint8_t ss = sof.read8();
        c.v_ss = (ss & 0xF);
        c.h_ss = (ss >> 4);
        c.tq = sof.read8();
        comps[id] = c;
      }
    } else if (magic == JpegFrame::DQT) {
      r.read16be();
      uint8_t target = r.read8();
      for (size_t n = 0; n < 64; n++) {
        quant[target & 0x3][n] = (target & 0x10) ? r.read16be() : r.read8();
      }
    } else if (magic == JpegFrame::DHT) {
      uint16_t size = r.read16be();
      Bini::reader dht(r.get(size-2));
      uint8_t id = dht.read8();
      std::span<const uint8_t> counts = dht.get(16);
      std::span<const uint8_t> values = dht.get(dht.sizeleft());
      huffmanTables.emplace(id, HuffmanTable(counts, values));
    } else if (magic == JpegFrame::SOS) {
      uint16_t size = r.read16be();
      Bini::reader sos(r.get(size-2));
      uint8_t Ns = sos.read8();
      std::vector<uint8_t> Ts;
      for (size_t n = 0; n < Ns; n++) {
        uint8_t comp = sos.read8();
        uint8_t ts = sos.read8();
        comps[comp].hd = (ts >> 4);
        comps[comp].ha = (ts & 0xF) | 0x10;
      }
      std::vector<uint8_t> sosblock = r.getSosBlock();
      Bini::bitreader<> br(sosblock);
      int max_h_ss = 1;
      int max_v_ss = 1;
      for (auto& [_, c] : comps) {
        if (c.h_ss > max_h_ss) max_h_ss = c.h_ss;
        if (c.v_ss > max_v_ss) max_v_ss = c.v_ss;
      }
      for (auto& [id, c] : comps) {
        size_t mb_w = (((max_h_ss - 1 + width) / max_h_ss * c.h_ss + 7) / 8);
        size_t mb_h = (((max_v_ss - 1 + height) / max_v_ss * c.v_ss + 7) / 8);
        Image& image = subimages[id] = Image(8*mb_w, 8*mb_h, PixelFormat::Grayscale8);
        int32_t dc_val = 0;
        for (size_t y = 0; y < mb_h; y++) {
          for (size_t x = 0; x < mb_w; x++) {
            decode_macroblock(image, x*8, y*8, br, dc_val, huffmanTables[c.hd], huffmanTables[c.ha], quant[c.tq]);
          }
        }
      }
      // assemble image from components & subsampling
    } else if (magic == JpegFrame::COM) {
      // Ignore comments
      uint16_t size = r.read16be();
      r.skip(size-2);
    } else if (magic == JpegFrame::EOI) {
      fprintf(stderr, "EOI\n");
      // Assemble components into full image
      PixelFormat pf;
      if (bpp <= 8) {
        if (components == 1) {
          pf = PixelFormat::Grayscale8;
        } else {
          pf = PixelFormat::RGB8;
        }
      } else {
        if (components == 1) {
          pf = PixelFormat::Grayscale16;
        } else {
          pf = PixelFormat::RGB16;
        }
      }
      Image image(width, height, pf);
      image.LoadFrom(ColorSpace::YCbCr, subimages[1], subimages[2], subimages[3]);
      return image;
    } else {
      fprintf(stderr, "Unknown magic %04x\n", magic);
      exit(1);
    }
  }
  return {};
}

}

